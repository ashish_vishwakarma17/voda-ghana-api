package com.services.impl;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.Album;
import com.beans.AlbumData;
import com.beans.Albums;
import com.beans.ArtistAlbum;
import com.beans.ArtistData;
import com.beans.ArtistTrack;
import com.beans.Artists;
import com.beans.DeviceInformation;
import com.beans.FeaturedPlaylist;
import com.beans.FeaturedPlaylistBean;
import com.beans.FeaturedPlaylistTracks;
import com.beans.Genre;
import com.beans.GenreAlbums;
import com.beans.GenreArtists;
import com.beans.GenreBean;
import com.beans.GenreTracks;
import com.beans.HomeItems;
import com.beans.Radio;
import com.beans.RadioBean;
import com.beans.RadioTracks;
import com.beans.Root;
import com.beans.TrackData;
import com.beans.Tracks;
import com.database.DataBaseProcedures;
import com.model.HomeParameters;
import com.services.ContentServices;

@Service
public class ContentServiceImpl implements ContentServices {
	
	@Autowired
	DataBaseProcedures dbProcedures;
	@Autowired
	private MessageSource messageSource;
	private Root obj;
    @SuppressWarnings("rawtypes")
	private  List list = null;

 //SEARCH(ALBUM/ARTIST/SONGS)
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse search(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("tracks") || reqParam.getEventType().equalsIgnoreCase("track")) {
                list = dbProcedures.trackSearch(reqParam,deviceInformation);
                if (list.isEmpty()) {
                    obj = new Root(113, messageSource.getMessage("113", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("albums") || reqParam.getEventType().equalsIgnoreCase("album")) {
                list = dbProcedures.albumSearch(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(111, messageSource.getMessage("111", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("artists") || reqParam.getEventType().equalsIgnoreCase("artist")) {
                list = dbProcedures.artistSearch(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(112, messageSource.getMessage("112", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("playlist") || reqParam.getEventType().equalsIgnoreCase("playlists")) {
                list = dbProcedures.playlistSearch(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(112, messageSource.getMessage("112", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new FeaturedPlaylist(list);
                }
            } else {
            	obj = new Root(114, messageSource.getMessage("114", null,new Locale(deviceInformation.getLang())));
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 9: - " + e.getMessage());
        	obj = new Root(114, messageSource.getMessage("114", null,new Locale(deviceInformation.getLang())));

        }
		return new RootResponse(obj);
	}
	//GET_HOME_ITEMS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getHomeData(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.getHomeItem(reqParam, deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new HomeItems(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 10: - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_TRACK_INFO
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getTrackInfo(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.trackInfo(reqParam, deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(197, messageSource.getMessage("197", null,new Locale(deviceInformation.getLang())));
            } else {
                try {
                    TrackData ddata = (TrackData) list.get(0);                                    
                    if (ddata.getRcode().startsWith("-")) {                                        
                        obj = new Root(196, messageSource.getMessage("196", null,new Locale(deviceInformation.getLang())));
                    } else {
                        obj = new Tracks(list);
                    }
                } catch (Exception e) {
                    obj = new Tracks(list);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 11: - " + e.getMessage());
            obj = new Root(197, messageSource.getMessage("197", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_ALL_ALBUM
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getAllAlbum(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.allAlbums(reqParam,deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new Albums(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 12: - " + e.getMessage());
            obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	 //GET_ALBUM_TRACKS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getAlbumTrack(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            AlbumData alb = dbProcedures.album(reqParam,deviceInformation);
            list = dbProcedures.albumTracks(reqParam, deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new Album(alb, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 14: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_ALL_ARTIST
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getAllArtist(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.allArtists(reqParam,deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new Artists(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 15: - " + e.getMessage());
            obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_ARTIST_ALBUMS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getArtistAlbum(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            ArtistData artistData = dbProcedures.artist(reqParam,deviceInformation);
            list = dbProcedures.artistAlbums(reqParam, deviceInformation);
            if (list.isEmpty() || artistData == null) {
                obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new ArtistAlbum(artistData, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 16: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_ARTIST_TRACKS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getArtistTrack(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            ArtistData artistData = dbProcedures.artist(reqParam,deviceInformation);
            list =dbProcedures.artistTracks(reqParam, deviceInformation);
            if (list.isEmpty() || artistData == null) {
                obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new ArtistTrack(artistData, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 17: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//Get All Genre
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getAllGenre(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.allGenre(reqParam, deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new Genre(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 18: - " + e.getMessage());
            obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_GENRE_ALBUMS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getGenreAlbum(HomeParameters reqParam, DeviceInformation deviceInformation) {
		GenreBean genre;
        try {
            genre = dbProcedures.genre(reqParam,deviceInformation);
            list = dbProcedures.genreAlbums(reqParam,deviceInformation);
            if (list.isEmpty() || genre == null) {
                obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new GenreAlbums(genre, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 19: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_Genre_ARTIST
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getGenreArtist(HomeParameters reqParam,DeviceInformation deviceInformation) {
		GenreBean genre;
        try {
            genre =dbProcedures.genre(reqParam, deviceInformation);
            list = dbProcedures.genreArtists(reqParam, deviceInformation);
            if (list.isEmpty() || genre == null) {
                obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new GenreArtists(genre, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 20: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	//-----------After 20 ------------------------------------------------------------------
	
	
	//GENRE_TRACKS_LIST 
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getTrackList(HomeParameters reqParam,DeviceInformation deviceInformation) {
		GenreBean genre;
        try {
            genre = dbProcedures.genre(reqParam, deviceInformation);
            list = dbProcedures.genreTrackList(reqParam, deviceInformation);
            if (list.isEmpty() || genre == null) {
            	obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new GenreTracks(genre, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 21: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GENRE_ARTIST_ALBUM_LIST/ GENRE_ARTIST_TRACK_LIST 
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getGenreArtistAlbumTrackList(HomeParameters reqParam, DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("album") || reqParam.getEventType().equalsIgnoreCase("albums")) {
                list = dbProcedures.genreArtistAlbums(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else {
                list = dbProcedures.genreArtistTrackList(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 22: - " + e.getMessage());
            obj = new Root(195, messageSource.getMessage("195", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//RECOMMENDED (ALBUM/ARTIST/TRACK) 
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getRecommended(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("albums") || reqParam.getEventType().equalsIgnoreCase("album")) {
                list = dbProcedures.albumRecommendation(reqParam, deviceInformation);
                if (list.isEmpty()) {
                    obj = new Root(191, messageSource.getMessage("191", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("artists") || reqParam.getEventType().equalsIgnoreCase("artist")) {
                list = dbProcedures.artistRecommendation(reqParam, deviceInformation);
                if (list.isEmpty()) {
                    obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("tracks") || reqParam.getEventType().equalsIgnoreCase("track")) {
                list = dbProcedures.trackRecommendation(reqParam, deviceInformation);
                if (list.isEmpty()) {
                    obj = new Root(193, messageSource.getMessage("193", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else {
                obj = new Root(100, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 23: - " + e.getMessage());
            obj = new Root(100, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_FEATURED_PLAYLIST
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getPlayList(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.getFeaturedPlaylist(reqParam,deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new FeaturedPlaylist(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 24: - " + e.getMessage());
            obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_FEATURED_PLAYLIST_TRACKS
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getPlayListTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            FeaturedPlaylistBean playlist = dbProcedures.getFeaturedPlaylistInfo(reqParam,deviceInformation);
            list = dbProcedures.featuredPlaylistTrackList(reqParam,deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(132, messageSource.getMessage("132", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new FeaturedPlaylistTracks(playlist, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 25: - " + e.getMessage());
            obj = new Root(132, messageSource.getMessage("132", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_RADIO 
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getRadio(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.getRadio(reqParam,deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new Radio(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 26: - " + e.getMessage());
            obj = new Root(121, messageSource.getMessage("121", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//GET_RADIO_TRACKS 
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getRadioTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            RadioBean radio = dbProcedures.getRadioInfo(reqParam,deviceInformation);
            list = dbProcedures.radioTrackList(reqParam,deviceInformation);
            if (list.isEmpty()) {
            	obj = new Root(132, messageSource.getMessage("132", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new RadioTracks(radio, list);
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 27: - " + e.getMessage());
            obj = new Root(132, messageSource.getMessage("132", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	
	//----------------------------------------------------------------------------------
	
	//New/Popular/Featured Albums
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse newReleasedPopularFeaturedAlbum(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("1")) {
                list = dbProcedures.newAlbums(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("2")) {
                list = dbProcedures.popularAlbums(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("3")) {
                list = dbProcedures.featuredAlbums(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.(case 83): - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse newReleasedArtist(HomeParameters reqParam,DeviceInformation deviceInformation) {
        //New/Popular/Featured Artists
        try {
            if (reqParam.getEventType().equalsIgnoreCase("1")) {
                list = dbProcedures.newArtists(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("2")) {
                list = dbProcedures.popularAlbums(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("3")) {
                list = dbProcedures.featuredArtists(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.(case 84): - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse newReleasedTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("1")) {
                list = dbProcedures.newTracks(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("2")) {
                list = dbProcedures.popularTracks(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("3")) {
                list = dbProcedures.featuredTracks(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.(case 85): - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse filterByLetter(HomeParameters reqParam,DeviceInformation deviceInformation) {
        try {
            if (reqParam.getEventType().equalsIgnoreCase("1")) {
                list = dbProcedures.albumFilter(reqParam, deviceInformation);
                if (list.isEmpty()) {
                    obj = new Root(251, messageSource.getMessage("251", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Albums(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("2")) {
                list = dbProcedures.artistFilter(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(252, messageSource.getMessage("252", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("3")) {
                list = dbProcedures.playlistFilter(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(253, messageSource.getMessage("253", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new FeaturedPlaylist(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("4")) {
                list = dbProcedures.genreFilter(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(254, messageSource.getMessage("254", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("5")) {
                list = dbProcedures.trackFilter(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(255, messageSource.getMessage("255", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Tracks(list);
                }
            } else {
            	obj = new Root(256, messageSource.getMessage("256", null,new Locale(deviceInformation.getLang())));
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.(case 86): - " + e.getMessage());
            obj = new Root(256, messageSource.getMessage("256", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse recommendedSimilarArtist(HomeParameters reqParam, DeviceInformation deviceInformation) {
        //Recommended Artists
        try {
            if (reqParam.getEventType().equalsIgnoreCase("artist")) {
                list = dbProcedures.artistRecommendationByArtist(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("album")) {
                list = dbProcedures.artistRecommendationByAlbum(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("genre")) {
                list = dbProcedures.artistRecommendationByGenre(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("playlist")) {
                list = dbProcedures.artistRecommendationByPlaylist(reqParam, deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else if (reqParam.getEventType().equalsIgnoreCase("track")) {
                list = dbProcedures.artistRecommendationByTrack(reqParam,deviceInformation);
                if (list.isEmpty()) {
                	obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
                } else {
                    obj = new Artists(list);
                }
            } else {
            	obj = new Root(100, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
            }
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.(case 92): - " + e.getMessage());
            obj = new Root(192, messageSource.getMessage("192", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	
}
