package com.services.impl;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.FeedbackSubject;
import com.beans.Notification;
import com.beans.RequestParameter;
import com.beans.Root;
import com.database.DataBaseProcedures;
import com.model.UserParameters;
import com.services.CommunicationServices;

@Service
public class CommunicationServicesImpl implements CommunicationServices {
	private Root obj;
	@Autowired
	DataBaseProcedures dbProcedures;
	@Autowired
	private MessageSource messageSource;
	
	@SuppressWarnings("rawtypes")
	List list;
	
	//Evt-50 NOTIFICATION_READ
	@Override
	public RootResponse readNotification(UserParameters reqParam, DeviceInformation deviceInformation) {
        try {
            new Thread() {
                public void run() {
                	dbProcedures.readNotification(reqParam,deviceInformation);
                }
            }.start();
            //obj = getDownloadUrl(reqParam);
            obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
        } catch (Exception e) {
            System.out.println("Exception in .CASE 50: - " + e.getMessage());
            obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
            }
		return new RootResponse(obj);
	}

	//Evt-51 FEEDBACK
	@Override
	public RootResponse userFeedback(RequestParameter reqParam,  DeviceInformation deviceInformation) {
        try {
            obj = new Root(176, messageSource.getMessage("176", null,new Locale(deviceInformation.getLang())));
            new Thread() {
                public void run() {
                	dbProcedures.userFeedback(reqParam);
                }
            }.start();
        } catch (Exception e) {
            System.out.println("Exception in .CASE 51: - " + e.getMessage());
            obj = new Root(176, messageSource.getMessage("176", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}

	//Evt-52 //Social Sharing
	@Override
	public RootResponse socialSharingTrack(RequestParameter reqParam, DeviceInformation deviceInformation) {
        try {
            new Thread() {
                public void run() {
                	dbProcedures.socialSharingTrack(reqParam,deviceInformation);
                }
            }.start();
            obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
        } catch (Exception e) {
            System.out.println("Exception in .CASE 52: - " + e.getMessage());
            obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	//Evt-53 Feedback Subjects
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse feedbackSubjectList(RequestParameter reqParam , DeviceInformation deviceInformation) {
        try {
            list = dbProcedures.feedbackSubjectList(reqParam,deviceInformation);
            if (list.isEmpty()) {
            	obj = new Root(100, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
            } else {
                obj = new FeedbackSubject(list);
            }
        } catch (Exception e) {
            System.out.println("Exception in .CASE 53: - " + e.getMessage());
            obj = new Root(100, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	//Evt-57 NOTIFICATION
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getNotificationInfo(UserParameters reqParam, DeviceInformation deviceInformation) {
		try {
			list = dbProcedures.getNotificationInfo(reqParam,deviceInformation);
			if (list.isEmpty()) {
			obj = new Root(201, messageSource.getMessage("201", null,new Locale(deviceInformation.getLang())));
			} else {
			obj = new Notification(list);
			}
			} catch (Exception e) {
			System.out.println("Exception in .CASE 57: - " + e.getMessage());
			obj = new Root(201, messageSource.getMessage("201", null,new Locale(deviceInformation.getLang())));
			}
		return new RootResponse(obj);
	}
	
	
}
