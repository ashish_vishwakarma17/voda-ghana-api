package com.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.beans.CciPortalResponse;
import com.beans.DashBoardData;
import com.beans.OperatorData;
import com.database.DataBaseProcedures;
import com.services.CCIServices;

@Service
public class CCIServiceImpl implements CCIServices {
	
	@Autowired
	DataBaseProcedures dbProcedures;

	@Override
	public CciPortalResponse getUserSubDetails(String mobile_number) {
		return dbProcedures.getUserSubDetails(mobile_number);
	}
	
	@Override
	public List<CciPortalResponse> getUserSuccessDetails(String mobile_number) {
		return dbProcedures.getUserSuccessDetails(mobile_number);
	}
	@Override
	public List<CciPortalResponse> getUserUnsubDetails(String mobile_number) {
		return dbProcedures.getUserUnsubDetails(mobile_number);
	}
	
	@Override
	public List<OperatorData> getOperatorData(String fromDate, String toDate) {
		 
		return    dbProcedures.getOperatorData(fromDate, toDate);
	}
	@Override
	public List<DashBoardData> getDashBoardData() {		 
		return    dbProcedures.getDashBoardData();
	}
	
	

}
