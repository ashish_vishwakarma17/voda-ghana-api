package com.services.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.AppVersionInfoData;
import com.beans.CountryAppVersionInfo;
import com.beans.CountryInfoData;
import com.beans.DeviceInformation;
import com.beans.PaymentMethod;
import com.beans.PaymentMethods;
import com.beans.RequestParameter;
import com.beans.Root;
import com.beans.TokenResponse;
import com.beans.TokenResponseJSON;
import com.beans.UserInfoData;
import com.database.DataBaseProcedures;
import com.model.ApplicationInstallViaShareModel;
import com.model.Temp;
import com.services.AppConfigServices;

import beans.config.AudioQualityConfiguration;
import beans.config.Configuration;
import beans.config.Visibility;

@Service
public class AppConfigServiceImpl implements AppConfigServices {
	
	 @Value("${TOKEN_USER_NAME}")
	 private String tokenUserName;
	 
	 @Value("${TOKEN_PASSWORD}")
	 private String tokenPassword;
	 
	 @Value("${TOKEN_URL}")
	 private String tokenURL;
	 
	 @Value("${BASIC_AUTH}")
	 private String basicAuth;
	 
	 
	 
	 
	 
	
	@Autowired
	DataBaseProcedures dbProcedures;
	@Autowired
	UserInfoData userinfo;
	@Autowired
	private MessageSource messageSource;
	private Root obj;
  @SuppressWarnings("unused")
private   Object obj12 = null, obj1 = null, obj2 = null, obj3 = null, obj4 = null, obj5 = null, obj6 = null, obj7 = null, obj8 = null, obj9 = null , obj10=null;
   @SuppressWarnings("rawtypes")
private  List list = null;
  
	@Override
	public RootResponse applicationInstallViaShare(ApplicationInstallViaShareModel reqParam, DeviceInformation  deviceInformation) {
        int resultCode = dbProcedures.applicationInstallViaShare(reqParam,deviceInformation);
        obj = new Root(resultCode, messageSource.getMessage(String.valueOf(resultCode), null,new Locale(deviceInformation.getLang())));
		return new RootResponse(obj);
	}
	@Override
	public RootResponse applicationInstallViaShareClickTracking(ApplicationInstallViaShareModel reqParam  , DeviceInformation  deviceInformation) {
		int resultCode = 0;
	 
			resultCode = dbProcedures.appInstallViaShareClickTraking(reqParam,deviceInformation);
			obj = new Root(resultCode, messageSource.getMessage(String.valueOf(resultCode), null,new Locale(deviceInformation.getLang())));
		 	 
	
		return new RootResponse(obj);
	}	
	
	//Send Notification
	@Override
	public void sendNotification(String notificationID,String devPin,String lang,String type,String userId
			) {
				dbProcedures.sendNotification(notificationID,devPin,lang,type,userId);
	}	
	//Change Language
	@Override
	public RootResponse changeLanguage(RequestParameter reqParam , DeviceInformation  deviceInformation) {
		obj = new Root(302, messageSource.getMessage("302", null,new Locale(deviceInformation.getLang())));
		return new RootResponse(obj);
	} 

	@Override
	public RootResponse PromotionShareConfig(Temp temp,DeviceInformation  deviceInformation) {
			obj =dbProcedures.getPromotionShareConfig(temp.getUserId(),temp.getApiVersion());
			if(obj==null)
				obj = new Root(116, messageSource.getMessage("116", null,new Locale(deviceInformation.getLang())));
			return new RootResponse(obj);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse countryinfo(DeviceInformation  deviceInformation) {
		 try {
 
             obj1 = (CountryInfoData)dbProcedures.getCountryInfo(deviceInformation);
             obj2 = (AppVersionInfoData) dbProcedures.getAppVersionInfo(   deviceInformation);
             obj3 = (Visibility) dbProcedures.getAppFeaturesVisibility( deviceInformation);
             obj4 = (Configuration) dbProcedures.getApplicationConfiguration(deviceInformation);                            
             obj7 = (AudioQualityConfiguration) dbProcedures.getAudioQualityConfiguration(deviceInformation);
             list = (List<PaymentMethod>) dbProcedures.getPaymentMethods(deviceInformation);
           //  System.out.println("obj1 ::" + obj1  + "obj2 ::" + obj2 + "obj3 ::"+ obj3 + "obj4"+ obj4 +"obj7 : "+ obj7 + "list :: "+ list);
             if (list.isEmpty()) {
                 obj = new Root(271, messageSource.getMessage("271", null,new Locale(deviceInformation.getLang())));
             } else {
                 obj8 = new PaymentMethods(list);
             }
             //obj9 = dbProcedures.getLeftMenuTitle(reqParam);             
            // obj10=(OptScreenConfig)dbProcedures.getOptScreenConfig(reqParam);
             if (obj1 == null || obj2 == null || obj3 == null || obj4 == null ||  obj7 == null) {
                 obj = new Root(194, messageSource.getMessage("194", null,new Locale(deviceInformation.getLang())));
             } else {
                 obj = new CountryAppVersionInfo(obj1, obj2, obj3, obj4, obj7, obj8,obj9,obj10);
             }
            
         } catch (Exception e) {
             e.printStackTrace();
             obj = new Root(194, messageSource.getMessage("194", null,new Locale(deviceInformation.getLang())));

         }
		return new RootResponse(obj);
	}
	
	@Override
	public RootResponse getToken( DeviceInformation deviceInformation) {
		
	 
		 StringBuffer responseString = new StringBuffer();
		String Data;
	  	Data="client_id="+tokenUserName+"&client_secret="+tokenPassword;
		try {
			URL url = new URL(tokenURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization", basicAuth);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setDoOutput(true);
		    conn.setConnectTimeout(10000);
			
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		    wr.write(Data);
		    wr.flush();		      
		    wr.close();

		 //   System.out.println(conn.getResponseCode());
		    BufferedReader rd = null;
		    if (conn.getResponseCode() == 200)
		      {
		        rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		      }
		      else
		      {
		        rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		      }

		   
		      String line; 
		      while ((line = rd.readLine()) != null) 
		      {
		        responseString.append(line); 
		        }
		    rd.close();
			conn.disconnect();
			
		
			dbProcedures.tokenTracking(deviceInformation, responseString);
			
			if(responseString.toString().contains("error_description"))
			{
				JSONObject jsonErrObj = new JSONObject(responseString.toString());
//				TokenResponse[0]=jsonErrObj.getString("error_description");
//				TokenResponse[1]=jsonErrObj.getString("error");
				
				 obj = new Root(1, jsonErrObj.getString("error_description") );
			}
			else
			{
				
				
				JSONObject jsonObj = new JSONObject(responseString.toString());	 	
			  	
				
//			  	tokenResponseObj =	 new TokenResponse(jsonObj.getString("access_token"),  jsonObj.getString("token_type") , Integer.parseInt(jsonObj.getString("expires_in")));
				
	           //  obj = new Root(0, accressToken );
			  	obj=    new TokenResponseJSON(new TokenResponse(jsonObj.getString("access_token"),  jsonObj.getString("token_type") , jsonObj.getInt("expires_in")));
				
			}
		 
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		return new RootResponse(obj);
	}
	
}
