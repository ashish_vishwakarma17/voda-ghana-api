package com.services.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.Root;
import com.database.DataBaseProcedures;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.UserOfflineModel;
import com.services.DownloadServices;
import com.utility.DownloadUtility;

@Service
public class DownloadServiceImpl implements DownloadServices {

	@Value("${request-logs-path}")
	private String requestLogsPath;
	
	@Value("${offline-logs-path}")
	private String offLineLogPath;
	@Autowired
	DataBaseProcedures dbProcedures;
	
	@Autowired
	DownloadUtility downloadUtility;
	
	@Autowired
	private MessageSource messageSource;
	private Root obj;

	
	
   //58) SAVE_TRACK_OFFLINE_REQUEST 
	@Override
	public RootResponse saveTrackOfflineRequest(UserOfflineModel reqParam , DeviceInformation  deviceInformation) {
        try {
            obj = dbProcedures.getDownloadUrl(reqParam,messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())) , deviceInformation );
        } catch (Exception e) {
            System.out.println("Exception in Vodafone Ghana.CASE 58: - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}

	   //59) SAVE_TRACK_OFFLINE_REQUEST 
		@Override
		public RootResponse successfullySaveTrackOffline(UserOfflineModel reqParam , DeviceInformation  deviceInformation) {
            try {
                new Thread() {
                    public void run() {
                    	dbProcedures.successfulOfflineDownload(reqParam, deviceInformation);
                    }
                }.start();
                obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
            } catch (Exception e) {
                System.out.println("Exception in Vodafone Ghana.CASE 59: - " + e.getMessage());
                obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
            }
			return new RootResponse(obj);
		}
		
		//60) GET_USER_OFFLINE_INFORMATION  
		@Override
		public RootResponse getUserOfflineInformation(UserOfflineModel reqParam , DeviceInformation  deviceInformation) {
            try {
                obj = dbProcedures.getUserOfflineInformation(reqParam,  messageSource.getMessage("215", null,new Locale(deviceInformation.getLang())),messageSource.getMessage("216", null,new Locale(deviceInformation.getLang())) , deviceInformation);
                if (obj == null) {
                	obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
                }
            } catch (Exception e) {
                System.out.println("Exception in Vodafone Ghana.CASE 60: - " + e.getMessage());
                obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
            }
			return new RootResponse(obj);
		}
		
		//60) offLineStreamingLogs  
		@Override
		public RootResponse offLineStreamingLogs(UserOfflineModel reqParam , DeviceInformation  deviceInformation) {
            try {
                final String json = reqParam.getJson();
                new Thread() {
                    public void run() {
                        String fileName = UUID.randomUUID().toString();
                        //Log File
                        try {
                            FileWriter fw = new FileWriter(offLineLogPath + fileName + ".log");
                            fw.write(json);
                            fw.flush();
                            fw.close();
                        } catch (Exception e) {
                            System.out.println("Exception in Vodafone Ghana.OFFLINE_STREAMING LOGS: - " + e.getMessage());
                        }
                        //Lock File
                        try {
                            FileWriter fw = new FileWriter(offLineLogPath + fileName + ".lck");
                            fw.write("LOCK");
                            fw.flush();
                            fw.close();
                        } catch (Exception e) {
                            System.out.println("Exception in Vodafone Ghana.(OFFLINE_STREAMING LOGS): - " + e.getMessage());
                        }
                    }
                }.start();
                obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));

            } catch (Exception e) {
                System.out.println("Exception in Vodafone Ghana.case 77: - " + e.getMessage());
                obj = new Root(0, messageSource.getMessage("0", null,new Locale(deviceInformation.getLang())));
            }
			return new RootResponse(obj);
		}

		@Override
		public void downloadTrack(HttpServletRequest request, HttpServletResponse response , DeviceInformation  deviceInformation) {
			try {
				downloadUtility.processRequest(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Exception in Vodafone Ghana. Download Track: - " + e.getMessage());
			}
			
		}
		
		//101) Play Done  
		@Override
		public RootResponse playDone(HttpServletRequest request , DeviceInformation  deviceInformation) {
			try {
				//	doBeforeProcessing(request);
					obj = new Root(301, messageSource.getMessage("301", null,new Locale(deviceInformation.getLang())));
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
				obj = new Root(110, messageSource.getMessage("100", null,new Locale(deviceInformation.getLang())));
			}
			       
			
			
			return new RootResponse(obj);
		}
		
		
		 private void doBeforeProcessing(HttpServletRequest request)  throws IOException, ServletException {
		        final String finalParamList;
		        String paramList = "";
		        int n;
		        StringBuffer buf;
		        String name;
		        for (Enumeration en = request.getParameterNames(); en.hasMoreElements();) {
		            name = (String) en.nextElement();
		            String values[] = request.getParameterValues(name);
		            n = values.length;
		            buf = new StringBuffer();
		            buf.append(name);
		            buf.append("=");
		            for (int i = 0; i < n; i++) {
		                buf.append(values[i]);
		                if (i < n - 1) {
		                    buf.append(",");
		                }
		            }
		            if (paramList.isEmpty()) {
		                paramList += "dt=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "$local=" + request.getLocalAddr() + "$remote=" + request.getRemoteAddr() + "$" + buf.toString();
		            } else {                
		                if(!buf.toString().contains("L-USER-MSISDN:")&&!buf.toString().contains("json=")){
		                    paramList += "$" + buf.toString();    
		                }                
		            }
		            //log(buf.toString());
		            
		        }
		     //   System.out.println("----------"+request.getServletPath());
		        if ("POST".equalsIgnoreCase(request.getMethod())) {
		            Scanner s = null;
		            try {
		            //	System.out.println("Before--------------------------------");
		                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
		            //    System.out.println("After--------------------------------");

		            JsonFactory factory = new JsonFactory();
		            ObjectMapper mapper = new ObjectMapper(factory);
		            JsonNode rootNode = mapper.readTree(s.next() + "");  
		            Iterator<Map.Entry<String,JsonNode>> fieldsIterator = rootNode.fields();
		            while (fieldsIterator.hasNext()) {
		                Map.Entry<String,JsonNode> field = fieldsIterator.next();
		                //System.out.println("Key: " + field.getKey() + "\tValue:" + field.getValue());
		                String para=field.getKey()+"="+(field.getValue()+"").replaceAll("\"", "");
		                if (paramList.isEmpty()) {
		                    paramList += "dt=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "$local=" + request.getLocalAddr() + "$remote=" + request.getRemoteAddr() + "$" + para.toString();
		                } else {                
		                    if(!para.contains("L-USER-MSISDN:")&&!para.contains("json=")){
		                        paramList += "$" + para.toString();    
		                    }                
		                }
		            }
		            s.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		        
		     //  System.out.println("Filter LOG:::"+paramList.toString());
		        finalParamList = paramList;
		        //System.out.println(" Parameters LIST: " + paramList);
		        new Thread() {
		            public void run() {
		                String fileName = UUID.randomUUID().toString();
		                //Log File
		                try {
		                	//System.out.println("Log parametere list--------"+finalParamList);
		                    //FileWriter fw = new FileWriter(filterConfig.getServletContext().getInitParameter("request-logs-path") + fileName + ".log");
		                	FileWriter fw = new FileWriter(requestLogsPath + fileName + ".log");
		                   // System.out.println("Log parametere list--------"+finalParamList);
		                    fw.write(finalParamList);
		                    fw.flush();
		                    fw.close();
		                } catch (Exception e) {
		                    System.out.println("Exception in Mziiki LoggingFilter.LOG_FILE - " + e.getMessage());
		                    
		                }
		                //Lock File
		                try {
		                    //FileWriter fw = new FileWriter(filterConfig.getServletContext().getInitParameter("request-logs-path") + fileName + ".lck");
		                	FileWriter fw = new FileWriter(requestLogsPath + fileName + ".lck");
		                    fw.write("LOCK");
		                    fw.flush(); 
		                    fw.close();
		                } catch (Exception e) {
		                    System.out.println("Exception in Mziiki LoggingFilter.LOCK_FILE - " + e.getMessage());
		                }
		            }
		        }.start();
		    }
		    
		    
}
