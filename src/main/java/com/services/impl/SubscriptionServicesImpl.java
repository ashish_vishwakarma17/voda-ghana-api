package com.services.impl;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.app.beans.SubscriptionBenefits;
import com.app.beans.SubscriptionPackageList;
import com.beans.DeviceInformation;
import com.beans.Root;
import com.beans.UserInfo;
import com.beans.UserInfoData;
import com.database.DataBaseProcedures;
import com.model.Otp;
import com.model.Temp;
import com.services.SubscriptionServices;

@Service
public class SubscriptionServicesImpl implements SubscriptionServices {
	private Root obj;
	@Autowired
	DataBaseProcedures dbProcedures;
	@Autowired
	private MessageSource messageSource;
	@SuppressWarnings("rawtypes")
	private  List list = null;

	@Autowired
	UserInfoData userinfo;
	
	//CRBT_REQUEST 
	@Override
	public RootResponse crbtRequest(Otp reqParam , DeviceInformation deviceInformation) {
        try {
            int responseCode =dbProcedures.requestCRBT(reqParam, deviceInformation);
            obj = new Root(responseCode, messageSource.getMessage(""+responseCode, null,new Locale(deviceInformation.getLang())));
        } catch (Exception e) {
            System.out.println("Exception in .CASE 46: - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public RootResponse getSubPackage(Temp reqParam  , DeviceInformation deviceInformation) {
		
        try {
            list = dbProcedures.getSubscriptionPackageList(deviceInformation);
            if (list.isEmpty()) {
                obj = new Root(222, messageSource.getMessage("222", null,new Locale(deviceInformation.getLang())));
            } else {
            	 obj = new SubscriptionPackageList(list, dbProcedures.getSubscriptionTrialStatus(reqParam.getUserId()),dbProcedures.SubscriptionBenifits());
            }
        } catch (Exception e) {
            System.out.println("Exception in .case 74: - " + e.getMessage());
            obj = new Root(222, messageSource.getMessage("222", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@Override
	public RootResponse subscription(Temp reqParam , DeviceInformation deviceInformation) {
        try {
            	
                int responseCode = dbProcedures.requestSubscription(reqParam  ,  deviceInformation);
                obj = new Root(responseCode, messageSource.getMessage(String.valueOf(responseCode), null,new Locale(deviceInformation.getLang())));
            
        } catch (Exception e) {
            System.out.println("Exception in .case 75: - " + e.getMessage());
            obj = new Root(244, messageSource.getMessage("244", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@Override
	public RootResponse unSubscription(Temp reqParam , DeviceInformation deviceInformation) {
        //UN-SUBSCRIPTION_REQUEST
        try {
            int responseCode = dbProcedures.requestUnSubscription(reqParam,deviceInformation);
            obj = new Root(responseCode, messageSource.getMessage(String.valueOf(responseCode), null,new Locale(deviceInformation.getLang())));
        } catch (Exception e) {
            System.out.println("Exception in .case 76: - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	@Override
	public RootResponse activateSubscription(Temp reqParam , DeviceInformation deviceInformation) {
		
        try {
            int responseCode = dbProcedures.requestActivate(reqParam, deviceInformation);
            obj = new Root(responseCode, messageSource.getMessage(String.valueOf(responseCode), null,new Locale(deviceInformation.getLang())));
        } catch (Exception e) {
            System.out.println("Exception in .(case 82): - " + e.getMessage());
            obj = new Root(110, messageSource.getMessage("110", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@Override
	public RootResponse CampaignTracking(Temp reqParam , DeviceInformation deviceInformation) {
        try {
            	
                int responseCode = dbProcedures.CampaignTracking(reqParam , deviceInformation);
                obj = new Root(responseCode, messageSource.getMessage(String.valueOf(responseCode), null,new Locale(deviceInformation.getLang())));
            
        } catch (Exception e) {
            System.out.println("Exception in .case 75: - " + e.getMessage());
            obj = new Root(244, messageSource.getMessage("244", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}
	
	@Override
	public RootResponse SubscriptionViaCampaign(Temp reqParam , DeviceInformation deviceInformation) {
		SubscriptionBenefits subscriptionBenefits;
        try {
            userinfo = dbProcedures.SubscriptionViaCampaign(reqParam,deviceInformation);
            if (userinfo == null) {
                obj = new Root(150, messageSource.getMessage("150", null,new Locale(deviceInformation.getLang())));
            } else {
              //String outRes [] = dbProcedures.GetSubscriptionTrialStatus(reqParam.getUserId()).split("#");                               
                subscriptionBenefits=dbProcedures.SubscriptionBenifits();
                obj = new UserInfo(userinfo, subscriptionBenefits,dbProcedures.getOptScreenConfig());
            }
        } catch (Exception e) {
            System.out.println("Exception in . CASE 6: - " + e.getMessage());
            obj = new Root(150, messageSource.getMessage("150", null,new Locale(deviceInformation.getLang())));
        }
		return new RootResponse(obj);
	}

	@Override
	public void campaignThirdPartyCallBackSend(String ani, String campaingName, String tackingId) {
		  //ani = AESEncriptionDecription.decrypt(ani);
		 
			 dbProcedures.campaignThirdPartyCallBackSend(ani, campaingName, tackingId);
		 
		
	}

}
