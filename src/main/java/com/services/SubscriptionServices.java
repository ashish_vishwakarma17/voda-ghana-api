package com.services;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.model.Otp;
import com.model.Temp;


@Service
public interface SubscriptionServices {

	RootResponse crbtRequest(Otp reqParam , DeviceInformation deviceInformation );

	RootResponse getSubPackage(Temp reqParams ,  DeviceInformation deviceInformation);

	RootResponse subscription(Temp reqParam , DeviceInformation deviceInformation);

	RootResponse unSubscription(Temp reqParam , DeviceInformation deviceInformation);

	RootResponse activateSubscription(Temp reqParam , DeviceInformation deviceInformation);

	RootResponse CampaignTracking(Temp subscription, DeviceInformation headers);

	RootResponse SubscriptionViaCampaign(Temp subscription, DeviceInformation headers);
	
	 void  campaignThirdPartyCallBackSend(String ani , String campaingName , String tackingId);

}
