package com.services;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.RequestParameter;
import com.model.UserParameters;


@Service
public interface CommunicationServices {

	RootResponse readNotification(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse userFeedback(RequestParameter reqParam , DeviceInformation deviceInformation);

	RootResponse socialSharingTrack(RequestParameter reqParam , DeviceInformation deviceInformation);

	RootResponse feedbackSubjectList(RequestParameter reqParam , DeviceInformation deviceInformation);

	RootResponse getNotificationInfo(UserParameters reqParam, DeviceInformation deviceInformation);

	
	

}
