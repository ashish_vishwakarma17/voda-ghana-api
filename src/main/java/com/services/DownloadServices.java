package com.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.model.UserOfflineModel;


@Service
public interface DownloadServices {

	RootResponse saveTrackOfflineRequest(UserOfflineModel reqParam , DeviceInformation  deviceInformation );

	RootResponse successfullySaveTrackOffline(UserOfflineModel reqParam , DeviceInformation  deviceInformation);

	RootResponse getUserOfflineInformation(UserOfflineModel reqParam , DeviceInformation  deviceInformation);

	RootResponse offLineStreamingLogs(UserOfflineModel reqParam , DeviceInformation  deviceInformation);


	void downloadTrack(HttpServletRequest request, HttpServletResponse response, DeviceInformation  deviceInformation);

	RootResponse playDone(HttpServletRequest request, DeviceInformation  deviceInformation);

}
