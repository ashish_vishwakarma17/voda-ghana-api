package com.services;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.RequestParameter;
import com.model.ApplicationInstallViaShareModel;
import com.model.Temp;


@Service
public interface AppConfigServices {

	RootResponse countryinfo(DeviceInformation  deviceInformation );
	
	
	RootResponse applicationInstallViaShare(ApplicationInstallViaShareModel reqParam,  DeviceInformation  deviceInformation );

	RootResponse applicationInstallViaShareClickTracking(ApplicationInstallViaShareModel reqParam,  DeviceInformation  deviceInformation );

	void sendNotification(String notificationID, String devPin, String lang, String type, String userId);

	RootResponse changeLanguage(RequestParameter reqParam, DeviceInformation  deviceInformation );

	RootResponse PromotionShareConfig (Temp temp ,DeviceInformation  deviceInformation);
	
	RootResponse getToken( DeviceInformation deviceInformation);
	
}
