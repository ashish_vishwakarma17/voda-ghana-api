package com.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.DeviceInformationData;
import com.model.Otp;
import com.model.SignIn;
import com.model.UserParameters;


@Service
public interface UserDetailsServices {
	
	
	Object checkLogin(SignIn reqParam , DeviceInformation deviceInformation );

	Object SignOut(SignIn reqParam , DeviceInformation deviceInformation);

	Object userinfo(SignIn reqParam, DeviceInformation deviceInformation );

	Object updateuserinfo(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getFavouriteTrackList(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse addFavouriteTrack(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse removeFavouriteTrack(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getNotificationFavouriteCount(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getUserNotification(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getUserFavourite(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse changeUserLanguage(UserParameters reqParam, DeviceInformation deviceInformation);

	DeviceInformationData getUserDeviceInformation(UserParameters reqParam, DeviceInformation deviceInformation);
	
	RootResponse updateFCMToken(UserParameters reqParam, DeviceInformation deviceInformation);
	

	Object userDeviceLoginInformation(UserParameters reqParam, DeviceInformation deviceInformation);

	RootResponse uploadUserImage(HttpServletRequest request, HttpServletResponse respnse, MultipartFile file);

	RootResponse sendOtp(Otp reqParam, DeviceInformation  deviceInformation);

	RootResponse resendOtp(Otp Otp , DeviceInformation  deviceInformation);

	RootResponse varifyOtp(Otp reqParam, DeviceInformation  deviceInformation);

}
