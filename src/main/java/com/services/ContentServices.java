package com.services;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.RequestParameter;
import com.model.HomeParameters;


@Service
public interface ContentServices {

	RootResponse search(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getHomeData(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getGenreArtist(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getGenreAlbum(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getAllGenre(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getArtistTrack(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getArtistAlbum(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getAllArtist(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getAlbumTrack(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getAllAlbum(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getTrackInfo(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getTrackList(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getGenreArtistAlbumTrackList(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getRecommended(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getPlayList(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getPlayListTracks(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getRadio(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse getRadioTracks(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse newReleasedPopularFeaturedAlbum(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse newReleasedArtist(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse newReleasedTracks(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse filterByLetter(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse recommendedSimilarArtist(HomeParameters reqParam,DeviceInformation deviceInformation);
	
}
