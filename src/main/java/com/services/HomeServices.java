package com.services;

import org.springframework.stereotype.Service;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.RequestParameter;
import com.model.HomeParameters;


@Service
public interface HomeServices {

	RootResponse getContainer(HomeParameters reqParam, DeviceInformation deviceInformation);

	RootResponse ContainerItem(HomeParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getArtistInfo(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse followArtist(HomeParameters reqParam,DeviceInformation deviceInformation);

	RootResponse discoverAllItems(HomeParameters reqParam, DeviceInformation deviceInformation);

	RootResponse getArtistSeeAll(HomeParameters reqParam, DeviceInformation deviceInformation);


	
}
