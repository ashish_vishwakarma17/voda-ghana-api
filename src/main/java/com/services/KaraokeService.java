package com.services;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.app.beans.RootResponse;
import com.beans.DeviceInformation;
import com.beans.RequestParameter;
import com.beans.Root;


@Service
public interface KaraokeService {

	RootResponse getKaraokeInfo(RequestParameter reqParam, DeviceInformation deviceInformation);

	RootResponse getKaraokeComment(RequestParameter reqParam, DeviceInformation deviceInformation);

	RootResponse getKaraokeArtistFollow(RequestParameter reqParam, DeviceInformation deviceInformation);


	Root uploadKaraokeSong(HttpServletRequest request, HttpServletResponse response, MultipartFile uploadFile);


}
