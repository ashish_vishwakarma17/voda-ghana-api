package com.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.beans.RootResponse;
import com.beans.RequestParameter;
import com.model.ApplicationInstallViaShareModel;
import com.model.Temp;
import com.services.AppConfigServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(tags="AppConfigration - " ,description=" Configuration API")
//@Api(tags="AppConfigration - " ,description="EVT (2,90,95,238)")
@RequestMapping("/api")
public class AppConfigrationController {
	
	@Autowired
	AppConfigServices appConfigServiceImpl;
	
	@Autowired
	private HttpServletRequest request;
	@Autowired 
	Header header;
	
	
	//DeviceInformation  deviceInformation;// = getHeaders();
	
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	
	

 
	
	@ApiOperation(value = "Country Info Evt-2", response = Iterable.class)
	@RequestMapping(value = "/countryinfo", method = RequestMethod.POST)
	public @ResponseBody RootResponse countryinfo( ) {
	     	return   appConfigServiceImpl.countryinfo( header.getHeaders(request));
	}
	
	 
	
	@ApiOperation(value = "applicationInstallViaShare Evt-95", response = Iterable.class)
	@RequestMapping(value = "/applicationInstallViaShare", method = RequestMethod.POST)
	public @ResponseBody Object applicationInstallViaShare(@RequestBody(required=true) ApplicationInstallViaShareModel reqParam    ) {
	     	return   appConfigServiceImpl.applicationInstallViaShare(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "applicationInstallViaShareClickTracking Evt-223", response = Iterable.class)
	@RequestMapping(value = "/applicationInstallViaShareClickTracking", method = RequestMethod.POST)
	public @ResponseBody RootResponse applicationInstallViaShareClickTracking(@RequestBody(required=true) ApplicationInstallViaShareModel reqParam    ) {
	     	return   appConfigServiceImpl.applicationInstallViaShareClickTracking(reqParam,header.getHeaders(request));
	}
	// GET SERVER DATE TiME
	@ApiOperation(value = "getServerDateTime", response = Iterable.class)
	@RequestMapping(value = "/getServerDateTime", method = RequestMethod.POST)
	public @ResponseBody String getServerDateTime() {
	     	return   new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
	}
	
	// SEND NOTIFICATION
	@ApiOperation(value = "sendNotification", response = Iterable.class)
	@RequestMapping(value = "/sendNotification", method = RequestMethod.GET)
	public @ResponseBody String sendNotification(@RequestParam String notificationID,
			@RequestParam String devPin,
			@RequestParam String lang,
			@RequestParam String type,
			@RequestParam String userId
			) {
					appConfigServiceImpl.sendNotification(notificationID,devPin,lang,type,userId);
	     	return  "Notification Successfully Sent......."; 
	}
	// Change Language
//	@ApiOperation(value = "changeLanguage", response = Iterable.class)
	@RequestMapping(value = "/changeLanguage", method = RequestMethod.POST)
	public @ResponseBody RootResponse changeLanguage(@RequestBody(required=true) RequestParameter reqParam ) {
		return   appConfigServiceImpl.changeLanguage(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "PromotionShareConfig Evt-238", response = Iterable.class)
	@RequestMapping(value = "/PromotionShareConfig", method = RequestMethod.POST)
	public @ResponseBody RootResponse PromotionShareConfig(@RequestBody(required=true) Temp temp ) {
		return   appConfigServiceImpl.PromotionShareConfig(temp, header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "GetToken", response = Iterable.class)
	@RequestMapping(value = "/get-token", method = RequestMethod.POST)
	public @ResponseBody RootResponse getToken( ) {
		return   appConfigServiceImpl.getToken(header.getHeaders(request));
	}
	
	

	//get request headers
		   @SuppressWarnings("unused")
		private Map<String, String> getHeadersInfo() {
			Map<String, String> map = new HashMap<String, String>();
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				map.put(key, value);
			}
			System.out.println("Header  Key Values :: "+ map);

			return map;
		};
		
		
		
		
		   
//		   
//		 //get request headers
//		   @SuppressWarnings("unused")
//		private DeviceInformation getHeaders() {
//			
//			   DeviceInformation	 deviceInformation = new DeviceInformation();
//			Map<String, String> map = new HashMap<String, String>();
//			Enumeration headerNames = request.getHeaderNames();
//			while (headerNames.hasMoreElements()) {
//				String key = (String) headerNames.nextElement();
//				String value = request.getHeader(key);				
//				map.put(key, value);
//			}			
//			deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//			deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//			deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//			deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//			deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//			deviceInformation.setLang(map.getOrDefault("lang", "en"));
//			deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//			deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//			deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//			deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//			deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//			
//			 
//			return deviceInformation;
//		} 
}
