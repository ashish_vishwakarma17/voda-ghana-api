package com.api.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
 
    
	
    @Bean
    public Docket productApi() {
    	
  	
        return new Docket(DocumentationType.SWAGGER_2)
                .select().apis(RequestHandlerSelectors.basePackage("com.api.controller"))
                .build()
                .securitySchemes(Arrays.asList(apiKey()))
                .securityContexts(Arrays.asList(securityContext()))
                .globalOperationParameters(globalParameterList()) 
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        ApiInfo apiInfo = new ApiInfo(
                "Voda Ghana App API",
                "This is an interactive document explaining the API(s). Use the api key provided to authorize <b> x-api-key </b> and test the API(s). The output data models are also available for reference.\r\n" + 
                "\r\n" + 
                "",
                "",
                "Terms of service",
                new Contact("DigiSpice Technology Private Ltd.", "", ""),
               "",
                "");
        return apiInfo;
    }
    /*
     * 
     *  String title,
      String description,
      String version,
      String termsOfServiceUrl,
      Contact contact,
      String license,
      String licenseUrl
     * 
     */
    
    private ApiKey apiKey() {
        return new ApiKey("x-api-key", "Authorization", "header");
    }
    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.any())
                .build();
    }
    
    
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference("x-api-key", authorizationScopes));
    }
    
    
    private List<Parameter> globalParameterList() {
    	
    	 List<Parameter> listGobalParam =  new   ArrayList<Parameter>();  
      
    	 listGobalParam.add( new ParameterBuilder()
                .name("os") // name of the header
                .defaultValue("andrioid")
                .modelRef(new ModelRef("string")) // data-type of the header
                .required(true) // required/optional
                .parameterType("header") // for query-param, this value can be 'query'                
                .build());
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("osv") // name of the header
                 .defaultValue("5.1.1")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("appv") // name of the header
                 .defaultValue("1.0.5")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("devpin") // name of the header
                 .defaultValue("ehQs8S6cU6c:APA91bGSzdHaon1FAeNaColnsDZH9XW8y0p35-IlS93Hbnne8_og3Zn9BPbTqyE0za036sKpbdRGUylLMXk5b2wzdagS9_VDyjTRZXEj3Zdp08c1o_4Uj7WIOz5nhCQF-9qc4Keut9rI")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("devid") // name of the header
                 .defaultValue("869230023430369")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("lang") // name of the header
                 .defaultValue("en")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
     	 listGobalParam.add( new ParameterBuilder()
                 .name("apiversion") // name of the header
                 .defaultValue("2")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
     	 
     	 
     	 listGobalParam.add( new ParameterBuilder()
                 .name("model") // name of the header
                 .defaultValue("HUAWEI-KIW-L22")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
     	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("ccode") // name of the header
                 .defaultValue("GH")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	             
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("ocid") // name of the header
                 .defaultValue("88")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	 
    	 listGobalParam.add( new ParameterBuilder()
                 .name("opid") // name of the header
                 .defaultValue("7")
                 .modelRef(new ModelRef("string")) // data-type of the header
                 .required(true) // required/optional
                 .parameterType("header") // for query-param, this value can be 'query'                
                 .build());
    	             
    	 
            return listGobalParam;
            
            
            

        
      }
    
    
    
}
