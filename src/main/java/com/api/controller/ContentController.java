package com.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.HomeParameters;
import com.services.impl.ContentServiceImpl;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
//@Api(tags="Content" ,description="EVT (9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,84,85,86,92)")
@Api(tags="Content" ,description="Content API")
@RequestMapping("/api")
public class ContentController {
	
	@Autowired
	ContentServiceImpl contentServiceImpl;
	@Autowired
	private HttpServletRequest request;
	
	@Autowired 
	Header header;
	
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})

@ApiOperation(value = "Search Evt(9)", response = HomeParameters.class)
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public @ResponseBody Object search(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.search(reqParam, header.getHeaders(request));
	}

	@ApiOperation(value = "Get Home Item Evt(10)", response = HomeParameters.class)
	@RequestMapping(value = "/getHomeData", method = RequestMethod.POST)
	public @ResponseBody Object getHomeData(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getHomeData(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "Get Track Info Evt(11)", response = HomeParameters.class)
	@RequestMapping(value = "/getTrackInfo", method = RequestMethod.POST)
	public @ResponseBody Object getTrackInfo(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getTrackInfo(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "Get All Album Evt(12)", response = HomeParameters.class)
	@RequestMapping(value = "/getAllAlbum", method = RequestMethod.POST)
	public @ResponseBody Object getAllAlbum(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getAllAlbum(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "Get Album Tracks Evt(13)", response = HomeParameters.class)
	@RequestMapping(value = "/getAlbumTrack", method = RequestMethod.POST)
	public @ResponseBody Object getAlbumTrack(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getAlbumTrack(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "Get All Artist Evt(15)", response = HomeParameters.class)
	@RequestMapping(value = "/getAllArtist", method = RequestMethod.POST)
	public @ResponseBody Object getAllArtist(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getAllArtist(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "Get Artist Album Evt(16)", response = HomeParameters.class)
	@RequestMapping(value = "/getArtistAlbum", method = RequestMethod.POST)
	public @ResponseBody Object getArtistAlbum(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getArtistAlbum(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "Get Artist Track Evt(17)", response = HomeParameters.class)
	@RequestMapping(value = "/getArtistTrack", method = RequestMethod.POST)
	public @ResponseBody Object getArtistTrack(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getArtistTrack(reqParam,header.getHeaders(request));
	}

	@ApiOperation(value = "Get All Genre Evt(18)", response = HomeParameters.class)
	@RequestMapping(value = "/getAllGenre", method = RequestMethod.POST)
	public @ResponseBody Object getAllGenre(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getAllGenre(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "Get Genre Album Evt(19)", response = HomeParameters.class)
	@RequestMapping(value = "/getGenreAlbum", method = RequestMethod.POST)
	public @ResponseBody Object getGenreAlbum(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getGenreAlbum(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "Get Genre Artist List Evt(20)", response = HomeParameters.class)
	@RequestMapping(value = "/getGenreArtist", method = RequestMethod.POST)
	public @ResponseBody Object getGenreArtist(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getGenreArtist(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "GENRE_TRACKS_LIST Evt(21)", response = HomeParameters.class)
	@RequestMapping(value = "/getTrackList", method = RequestMethod.POST)
	public @ResponseBody Object getTrackList(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getTrackList(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "GENRE_ARTIST_ALBUM_LIST/ GENRE_ARTIST_TRACK_LIST  Evt(22)",notes="etype=(album/ tracks) for genre artist album / genre artist tracks. ", response = HomeParameters.class)
	@RequestMapping(value = "/getGenreArtistAlbumTrackList", method = RequestMethod.POST)
	public @ResponseBody Object getGenreArtistAlbumTrackList(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getGenreArtistAlbumTrackList(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "RECOMMENDED (ALBUM/ARTIST/TRACK)   Evt(23)",notes="etype=(album/artist/ track) and via=(album/artist/track/genre)", response = HomeParameters.class)
	@RequestMapping(value = "/getRecommended", method = RequestMethod.POST)
	public @ResponseBody Object getRecommended(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getRecommended(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "GET_PLAYLIST   Evt(24)", response = HomeParameters.class)
	@RequestMapping(value = "/getPlayList", method = RequestMethod.POST)
	public @ResponseBody Object getPlayList(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getPlayList(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "GET_PLAYLIST_TRACKS   Evt(25)", response = HomeParameters.class)
	@RequestMapping(value = "/getPlayListTracks", method = RequestMethod.POST)
	public @ResponseBody Object getPlayListTracks(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getPlayListTracks(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "GET_RADIO  Evt(26)", response = HomeParameters.class)
	@RequestMapping(value = "/getRadio", method = RequestMethod.POST)
	public @ResponseBody Object getRadio(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getRadio(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "GET_RADIO_TRACKS  Evt(27)", response = HomeParameters.class)
	@RequestMapping(value = "/getRadioTracks", method = RequestMethod.POST)
	public @ResponseBody Object getRadioTracks(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.getRadioTracks(reqParam, header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "NEW RELEASED/POPULAR/FEATURED - [ALBUM]   Evt(83)", response = HomeParameters.class)
	@RequestMapping(value = "/newReleasedPopularFeaturedAlbum", method = RequestMethod.POST)
	public @ResponseBody Object newReleasedPopularFeaturedAlbum(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.newReleasedPopularFeaturedAlbum(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "NEW RELEASED - [ARTIST]   Evt(84)", response = HomeParameters.class)
	@RequestMapping(value = "/newReleasedArtist", method = RequestMethod.POST)
	public @ResponseBody Object newReleasedArtist(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.newReleasedArtist(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "NEW RELEASED - [TRACK]   Evt(85)", response = HomeParameters.class)
	@RequestMapping(value = "/newReleasedTracks", method = RequestMethod.POST)
	public @ResponseBody Object newReleasedTracks(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.newReleasedTracks(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "FILTER_BY_LETTER   Evt(86)", response = HomeParameters.class)
	@RequestMapping(value = "/filterByLetter", method = RequestMethod.POST)
	public @ResponseBody Object filterByLetter(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.filterByLetter(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "RECOMMENDED/SIMILAR ARTIST   Evt(92)", response = HomeParameters.class)
	@RequestMapping(value = "/recommendedSimilarArtist", method = RequestMethod.POST)
	public @ResponseBody Object recommendedSimilarArtist(
			@RequestBody HomeParameters reqParam) {
	     	return   contentServiceImpl.recommendedSimilarArtist(reqParam,header.getHeaders(request));
	}

//	  @SuppressWarnings("unused")
//			private DeviceInformation header(request) {
//				
//				   DeviceInformation	 deviceInformation = new DeviceInformation();
//				Map<String, String> map = new HashMap<String, String>();
//				Enumeration headerNames = request.getHeaderNames();
//				while (headerNames.hasMoreElements()) {
//					String key = (String) headerNames.nextElement();
//					String value = request.getHeader(key);				
//					map.put(key, value);
//				}			
//				deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//				deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//				deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//				deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//				deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//				deviceInformation.setLang(map.getOrDefault("lang", "en"));
//				deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//				deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//				deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//				deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//				deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//				
//				
//				
//				System.out.println("Header  Key Values :: "+ map);
//
//				return deviceInformation;
//			}
//			   
	
}
