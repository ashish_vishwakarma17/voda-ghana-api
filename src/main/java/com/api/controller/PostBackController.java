package com.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.services.SubscriptionServices;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags="PostBack" ,description="API")
public class PostBackController {
	
	@Autowired
	SubscriptionServices subscriptionServices;
	
	@ApiOperation(value = "thirdpartycallbacksent", response = Iterable.class)
	@RequestMapping(value = "/thirdpartycallbacksent", method = RequestMethod.GET)
	public @ResponseBody Object campaignThirdPartyCallBacksend(@RequestParam  String  ani ,  @RequestParam String campaingName , @RequestParam String tackingId) {
	     	   subscriptionServices.campaignThirdPartyCallBackSend(ani, campaingName, tackingId);
	     	   return "Success";
	}
	

}
