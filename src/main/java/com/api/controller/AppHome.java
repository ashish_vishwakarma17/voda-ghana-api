package com.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.HomeParameters;
import com.services.HomeServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
//@Api(tags="Home Page" ,description="EVT (201,202,203,204,205)")
@Api(tags="Home Page" ,description="Home Page data")
@RequestMapping("/api")
public class AppHome {
	

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	HomeServices homeServiceImpl;
	@Autowired 
	Header header;
	
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})

@ApiOperation(value = "GET Container Evt(201)", response = HomeParameters.class)
	@RequestMapping(value = "/getContainer", method = RequestMethod.POST)
	public @ResponseBody Object getContainer(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.getContainer(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "GET Container Item Evt(202)", response = HomeParameters.class)
	@RequestMapping(value = "/getContainerItem", method = RequestMethod.POST)
	public @ResponseBody Object ContainerItem(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.ContainerItem(reqParam, header.getHeaders(request));
	}
	@ApiOperation(value = "GET Artist Info Evt(203)", response = HomeParameters.class)
	@RequestMapping(value = "/getArtistInfo", method = RequestMethod.POST)
	public @ResponseBody Object getArtistInfo(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.getArtistInfo(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "Follow Artist Evt(204)",notes="etype: follow/unfollow", response = HomeParameters.class)
	@RequestMapping(value = "/followArtist", method = RequestMethod.POST)
	public @ResponseBody Object followArtist(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.followArtist(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "DISCOVER_ALL_ITEMS Evt(205)",notes="etype: follow/unfollow", response = HomeParameters.class)
	@RequestMapping(value = "/discoverAllItems", method = RequestMethod.POST)
	public @ResponseBody Object discoverAllItems(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.discoverAllItems(reqParam, header.getHeaders(request));
	}
	//Artist See All
	@ApiOperation(value = "Artist See All Evt(206)",notes="etype: follow/unfollow", response = HomeParameters.class)
	@RequestMapping(value = "/getArtistSeeAll", method = RequestMethod.POST)
	public @ResponseBody Object getArtistSeeAll(
			@RequestBody HomeParameters reqParam) {
	     	return   homeServiceImpl.getArtistSeeAll(reqParam,header.getHeaders(request));
	}
	
	
//	   @SuppressWarnings("unused")
//		private DeviceInformation getHeaders() {
//			
//			   DeviceInformation	 deviceInformation = new DeviceInformation();
//			Map<String, String> map = new HashMap<String, String>();
//			Enumeration headerNames = request.getHeaderNames();
//			while (headerNames.hasMoreElements()) {
//				String key = (String) headerNames.nextElement();
//				String value = request.getHeader(key);				
//				map.put(key, value);
//			}			
//			deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//			deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//			deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//			deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//			deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//			deviceInformation.setLang(map.getOrDefault("lang", "en"));
//			deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//			deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//			deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//			deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//			deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//			
//			
//			
//			System.out.println("Header  Key Values :: "+ map);
//
//			return deviceInformation;
//		}
		   
}
