package com.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.beans.RequestParameter;
import com.model.UserParameters;
import com.services.CommunicationServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(tags="Communication - NOTIFICATION/FEEDBACK/SHARING " ,description="EVT (49,50,51,52,53,71,72,73)")
@RequestMapping("/api")
public class CommunicationController {
	@Autowired
	CommunicationServices communicationServices;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired	
	Header header;

	
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})

	@ApiOperation(value = "readNotification Evt-50", response = Iterable.class)
	@RequestMapping(value = "/readNotification", method = RequestMethod.POST)
	public @ResponseBody Object readNotification(
			@RequestBody UserParameters reqParam) {
		return   communicationServices.readNotification(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "userFeedback Evt-51",notes="etype =1/2/3/… (i.e Feedback Subject Id)", response = Iterable.class)
	@RequestMapping(value = "/userFeedback", method = RequestMethod.POST)
	public @ResponseBody Object userFeedback(
			@RequestBody RequestParameter reqParam) {
	     	return   communicationServices.userFeedback(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "socialSharingTrack Evt-52",notes="etype=(facebook/ twitter/sms/email/googleplus/other) for notification / favourite. ", response = Iterable.class)
	@RequestMapping(value = "/socialSharingTrack", method = RequestMethod.POST)
	public @ResponseBody Object socialSharingTrack(
			@RequestBody RequestParameter reqParam) {
	     	return   communicationServices.socialSharingTrack(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "feedbackSubjectList Evt-53", response = Iterable.class)
	@RequestMapping(value = "/feedbackSubjectList", method = RequestMethod.POST)
	public @ResponseBody Object feedbackSubjectList(
			@RequestBody RequestParameter reqParam) {
	     	return   communicationServices.feedbackSubjectList(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "getNotificationInfo Evt-57", response = Iterable.class)
	@RequestMapping(value = "/getNotificationInfo", method = RequestMethod.POST)
	public @ResponseBody Object getNotificationInfo(@RequestBody UserParameters reqParam) {
	     	return   communicationServices.getNotificationInfo(reqParam, header.getHeaders(request));
	}

//
//	   @SuppressWarnings("unused")
//	private DeviceInformation getHeaders() {
//		
//		   DeviceInformation	 deviceInformation = new DeviceInformation();
//		Map<String, String> map = new HashMap<String, String>();
//		Enumeration headerNames = request.getHeaderNames();
//		while (headerNames.hasMoreElements()) {
//			String key = (String) headerNames.nextElement();
//			String value = request.getHeader(key);				
//			map.put(key, value);
//		}			
//		deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//		deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//		deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//		deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//		deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//		deviceInformation.setLang(map.getOrDefault("lang", "en"));
//		deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//		deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//		deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//		deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//		deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//		
//		
//		
//		System.out.println("Header  Key Values :: "+ map);
//
//		return deviceInformation;
//	}
	   
	   
}
