package com.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.Otp;
import com.model.Temp;
import com.services.SubscriptionServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
//@Api(tags="Subscription" ,description="EVT (45,74,75,76,82)")
@Api(tags="Subscription" ,description="Subscription API")
@RequestMapping("/api")
public class SubscriptionController {
@Autowired
SubscriptionServices subscriptionServices;

@Autowired
private HttpServletRequest request;
	
@Autowired 
private Header header;



	@ApiOperation(value = "CRBT_REQUEST  Evt(45)", response = Iterable.class)
	@RequestMapping(value = "/crbtRequest", method = RequestMethod.POST)
	public @ResponseBody Object crbtRequest(@RequestBody Otp crbtRequest) {
	     	return   subscriptionServices.crbtRequest(crbtRequest, header.getHeaders(request) );
	}
	
	
	@ApiOperation(value = "GET SUB PACKAGE  Evt(74)", response = Iterable.class)
	@RequestMapping(value = "/getSubPackage", method = RequestMethod.POST)
	public @ResponseBody Object getSubPackage(	@RequestBody Temp tmp) {
	     	return   subscriptionServices.getSubPackage(tmp , header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "subscription  Evt(75)", response = Iterable.class)
	@RequestMapping(value = "/subscription", method = RequestMethod.POST)
	public @ResponseBody Object subscription(@RequestBody Temp subscription) {
	     	return   subscriptionServices.subscription(subscription , header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "unSubscription  Evt(76)", response = Iterable.class)
	@RequestMapping(value = "/unSubscription", method = RequestMethod.POST)
	public @ResponseBody Object unSubscription(	@RequestBody Temp unSubscription) {
	     	return   subscriptionServices.unSubscription(unSubscription, header.getHeaders(request));
	}
	
	@ApiOperation(value = "ACtive Subscription  Evt(82)", response = Iterable.class)
	@RequestMapping(value = "/activateSubscription", method = RequestMethod.POST)
	public @ResponseBody Object activateSubscription(
			@RequestBody Temp active) {
	     	return   subscriptionServices.activateSubscription(active , header.getHeaders(request));
	}
	
	@ApiOperation(value = "CampaignTracking  Evt(224)", response = Iterable.class)
	@RequestMapping(value = "/CampaignTracking", method = RequestMethod.POST)
	public @ResponseBody Object CampaignTracking(@RequestBody Temp subscription) {
	     	return   subscriptionServices.CampaignTracking(subscription , header.getHeaders(request));
	}
	
	@ApiOperation(value = "SubscriptionViaCampaign  Evt(225)", response = Iterable.class)
	@RequestMapping(value = "/SubscriptionViaCampaign", method = RequestMethod.POST)
	public @ResponseBody Object SubscriptionViaCampaign(@RequestBody Temp subscription) {
	     	return   subscriptionServices.SubscriptionViaCampaign(subscription , header.getHeaders(request));
	}
		
	@ApiOperation(value = "thirdpartycallbacksent", response = Iterable.class)
	@RequestMapping(value = "/thirdpartycallbacksent", method = RequestMethod.POST)
	public @ResponseBody Object campaignThirdPartyCallBacksend(@RequestParam  String  ani ,  @RequestParam String campaingName , @RequestParam String tackingId) {
	     	   subscriptionServices.campaignThirdPartyCallBackSend(ani, campaingName, tackingId);
	     	   return "Success";
	}
	
	
//	 //get request headers
//	   @SuppressWarnings("unused")
//	private DeviceInformation header.getHeaders(request) {
//		
//		   DeviceInformation	 deviceInformation = new DeviceInformation();
//		Map<String, String> map = new HashMap<String, String>();
//		Enumeration headerNames = request.getHeaderNames();
//		while (headerNames.hasMoreElements()) {
//			String key = (String) headerNames.nextElement();
//			String value = request.getHeader(key);				
//			map.put(key, value);
//		}			
//		deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//		deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//		deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//		deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//		deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//		deviceInformation.setLang(map.getOrDefault("lang", "en"));
//		deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//		deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//		deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//		deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//		deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//		
//		 
//		return deviceInformation;
//	} 
//	
}
