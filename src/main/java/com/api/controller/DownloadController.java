package com.api.controller;

//import javax.security.auth.message.callback.PrivateKeyCallback.Request;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.beans.RootResponse;
import com.model.UserOfflineModel;
import com.services.DownloadServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
//@Api(tags="Download - " ,description="EVT (58,59,60,77)")
@Api(tags="Download - " ,description="Download API")
@RequestMapping("/api")
public class DownloadController {
	
	@Autowired
	DownloadServices downloadServices;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired 
	Header header;
	
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})

	//58) SAVE_TRACK_OFFLINE_REQUEST
	@ApiOperation(value = "Save Track OFF LINE Request Info Evt-58", response = Iterable.class)
	@RequestMapping(value = "/saveTrackOfflineRequest", method = RequestMethod.POST)
	public @ResponseBody RootResponse saveTrackOfflineRequest(@RequestBody(required=true) UserOfflineModel reqParam    ) {
	     	return   downloadServices.saveTrackOfflineRequest(reqParam, header.getHeaders(request));
	}
	 //59) SAVE_TRACK_OFFLINE_REQUEST 
	@ApiOperation(value = "SUCCESSFUL_SAVED_OFFLINE  Evt-59", response = Iterable.class)
	@RequestMapping(value = "/successfullySaveTrackOffline", method = RequestMethod.POST)
	public @ResponseBody RootResponse successfullySaveTrackOffline(@RequestBody(required=true) UserOfflineModel reqParam    ) {
	     	return   downloadServices.successfullySaveTrackOffline(reqParam , header.getHeaders(request));
	}
	//60) GET_USER_OFFLINE_INFORMATION 
	@ApiOperation(value = "REQUEST [MOBILE_NUMBER_VERIFICATION]  Evt-60",notes="etype – 1/2/3/4/5 [Profile/Subscription/Download/CRBT/Login] \r\n" + 
			"msisdn – Mobile Number for Verification. \r\n" + 
			"", response = Iterable.class)
	@RequestMapping(value = "/getUserOfflineInformation", method = RequestMethod.POST)
	public @ResponseBody RootResponse getUserOfflineInformation(@RequestBody(required=true) UserOfflineModel reqParam    ) {
	     	return   downloadServices.getUserOfflineInformation(reqParam , header.getHeaders(request));
	}
	
	//77) OFFLINE_STREAMING LOGS 
	@ApiOperation(value = "OFFLINE_STREAMING LOGS  Evt-77", response = Iterable.class)
	@RequestMapping(value = "/offLineStreamingLogs", method = RequestMethod.POST)
	public @ResponseBody RootResponse offLineStreamingLogs(@RequestBody(required=true) UserOfflineModel reqParam    ) {
	     	return   downloadServices.offLineStreamingLogs(reqParam , header.getHeaders(request));
	}
	
	//Download Track
	@ApiOperation(value = "Download Track", response = Iterable.class)
	@RequestMapping(value = "/downloadtrack", method = {RequestMethod.GET ,RequestMethod.POST})
	public void downloadTrack(HttpServletRequest request, HttpServletResponse response) {
		 downloadServices.downloadTrack(request,response , header.getHeaders(request));
	}
	//Download Track
	@ApiOperation(value = "Play Done Evt-", response = Iterable.class)
	@RequestMapping(value = "/playDone", method = {RequestMethod.GET ,RequestMethod.POST})
	public @ResponseBody RootResponse playDone(HttpServletRequest request ) {
		return downloadServices.playDone(request , header.getHeaders(request));
	}

	 //get request headers
//	   @SuppressWarnings("unused")
//	private DeviceInformation header.getHeaders(request) {
//		
//		   DeviceInformation	 deviceInformation = new DeviceInformation();
//		Map<String, String> map = new HashMap<String, String>();
//		Enumeration headerNames = request.getHeaderNames();
//		while (headerNames.hasMoreElements()) {
//			String key = (String) headerNames.nextElement();
//			String value = request.getHeader(key);				
//			map.put(key, value);
//		}			
//		deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//		deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//		deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//		deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//		deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//		deviceInformation.setLang(map.getOrDefault("lang", "en"));
//		deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//		deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//		deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//		deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//		deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//		
//		 
//		return deviceInformation;
//	} 
	   
}
