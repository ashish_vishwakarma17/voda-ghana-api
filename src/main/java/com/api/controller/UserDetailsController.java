package com.api.controller;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.model.Otp;
import com.model.SignIn;
import com.model.UserParameters;
import com.services.UserDetailsServices;
import com.utility.Header;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(tags="Login" , description="Login & User Details API")
@RequestMapping( value =  "/api" , produces = MediaType.APPLICATION_JSON_VALUE)
public class UserDetailsController {
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	UserDetailsServices userDetailsServices;
	
	@Autowired 
	Header header;
	
	@ApiResponses(value = {
	        @ApiResponse(code = 200, message = "Successfully retrieved list"),
	        @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
	        @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
	        @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
	})
	
	
	@ApiOperation(value = "sendOtp Evt-71", notes="etype- 1/2/3/4/5 [Profile/Subscription/Download/CRBT/Login] \r\n" + 
			"msisdn – Mobile Number for Verification. \r\n" + 
			"",response = Otp.class)
	
	@RequestMapping(value = "/sendOtp", method = RequestMethod.POST)
	public @ResponseBody Object sendOtp( @RequestBody Otp reqParam) {		
		return   userDetailsServices.sendOtp(reqParam,header.getHeaders(request));
	}
	
	
	
	@ApiOperation(value = "resendOtp Evt-72", response = Otp.class)
	@RequestMapping(value = "/resendOtp", method = RequestMethod.POST)
	public @ResponseBody Object resendOtp( @RequestBody Otp reqParam) {
		return   userDetailsServices.resendOtp(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "varifyOtp Evt-73", response = Otp.class)
	@RequestMapping(value = "/varifyOtp", method = RequestMethod.POST)
	public @ResponseBody Object varifyOtp(	@RequestBody Otp reqParam) {
	     	return   userDetailsServices.varifyOtp(reqParam,header.getHeaders(request));
	}
	

	@ApiOperation(value = "SignIn User Evt-1", response =SignIn.class)
	@RequestMapping(value = "/login", method = RequestMethod.POST )
	public @ResponseBody Object login( @RequestBody SignIn reqParam   ){	
	     return   userDetailsServices.checkLogin(reqParam,header.getHeaders(request) );           
}

 

	@ApiOperation(value = "SignOut User Evt-5",notes="", response = Iterable.class)
	@RequestMapping(value = "/signout", method = RequestMethod.POST)
	public @ResponseBody Object signout(@RequestBody SignIn reqParam) {
	     	return   userDetailsServices.SignOut(reqParam,header.getHeaders(request));
	}
	@ApiOperation(value = "Get User Info Evt-6", response = SignIn.class)
	@RequestMapping(value = "/userinfo", method = RequestMethod.POST)
	public @ResponseBody Object userinfo(@RequestBody SignIn reqParam) {
	     	return   userDetailsServices.userinfo(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "Update User Info Evt-7",notes="gender=1 /2/3 (male/ female/other) \r\n" + 
			"etype =0/1/2/3( \r\n" + 
			"0- Only User Info & \r\n" + 
			"1 - User Info + Image & \r\n" + 
			"2 - User Info +Mobile Number Verification & \r\n" + 
			"3- User Info + Image + Mobile Number Verification) \r\n" + 
			"", response = UserParameters.class)
	@RequestMapping(value = "/updateuserinfo", method = RequestMethod.POST)
	public @ResponseBody Object updateuserinfo(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.updateuserinfo(reqParam, header.getHeaders(request));
	}
	

	@ApiOperation(value = "Update User Image Evt-8", response = Iterable.class)
	@RequestMapping(value = "/uploadUserImage" ,method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public @ResponseBody Object uploadImage(HttpServletRequest request,HttpServletResponse response, @RequestPart MultipartFile  uploaded_file) {
		System.out.println("Here---1"+request.getQueryString()); 
		
		Enumeration<String> params = request.getParameterNames(); 
		while(params.hasMoreElements()){
		 String paramName = params.nextElement();
		 System.out.println("Parameter Name - "+paramName+", Value - "+request.getParameter(paramName));
		}
		//return "Done";
	    return   userDetailsServices.uploadUserImage(request,response,uploaded_file);
	}

	
	  
	@ApiOperation(value = "GET_FAVOURITE_TRACK_LIST Evt-36", response = UserParameters.class)
	@RequestMapping(value = "/getFavouriteTrackList" ,method = RequestMethod.POST)
	public @ResponseBody Object getFavouriteTrackList(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.getFavouriteTrackList(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "ADD_TRACK_TO_FAVOURITE  Evt-37", response = UserParameters.class)
	@RequestMapping(value = "/addFavouriteTrack", method = RequestMethod.POST)
	public @ResponseBody Object addFavouriteTrack(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.addFavouriteTrack(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "REMOVE_TRACK_FROM_FAVOURITE  Evt-38", response = UserParameters.class)
	@RequestMapping(value = "/removeFavouriteTrack", method = RequestMethod.POST)
	public @ResponseBody Object removeFavouriteTrack(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.removeFavouriteTrack(reqParam, header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "COUNT(NOTIFICATION/FAVOURITE)  Evt-48", response = UserParameters.class)
	@RequestMapping(value = "/getNotificationFavouriteCount", method = RequestMethod.POST)
	public @ResponseBody Object getNotificationFavouriteCount(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.getNotificationFavouriteCount(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "Get User NOTIFICATION)  Evt-49",notes="etype=(1/ 2) for notification / favourite.", response = Iterable.class)
	@RequestMapping(value = "/getUserNotification", method = RequestMethod.POST)
	public @ResponseBody Object getUserNotification(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.getUserNotification(reqParam,header.getHeaders(request));
	}
	
	@ApiOperation(value = "Get User FAVOURITE NEW)  Evt-91", response = Iterable.class)
	@RequestMapping(value = "/getUserFavouriteNew", method = RequestMethod.POST)
	public @ResponseBody Object getUserFavouriteNew(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.getUserFavourite(reqParam, header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "Change User Language)  Evt-93", response = Iterable.class)
	@RequestMapping(value = "/changeUserLanguage", method = RequestMethod.POST)
	public @ResponseBody Object changeUserLanguage(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.changeUserLanguage(reqParam, header.getHeaders(request));
	}
	
	@ApiOperation(value = "getUserDeviceInformation)  Evt-98", response = Iterable.class)
	@RequestMapping(value = "/getUserDeviceInformation", method = RequestMethod.POST)
	public @ResponseBody Object getUserDeviceInformation(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.getUserDeviceInformation(reqParam, header.getHeaders(request));
	}
	
	
	@ApiOperation(value = "updateFCMToken", response = Iterable.class)
	@RequestMapping(value = "/updateFCMToken", method = RequestMethod.POST)
	public @ResponseBody Object updateFCMToken(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.updateFCMToken(reqParam, header.getHeaders(request));
	}
	
	// UserDeviceLoginInformation	
	@ApiOperation(value = "UserDeviceLoginInformation)  Evt-215", response = Iterable.class)
	@RequestMapping(value = "/userDeviceLoginInformation", method = RequestMethod.POST)
	public @ResponseBody Object userDeviceLoginInformation(@RequestBody UserParameters reqParam) {
	     	return   userDetailsServices.userDeviceLoginInformation(reqParam,header.getHeaders(request));
	}	
	

	//get request headers
		   private Map<String, String> getHeadersInfo() {
			Map<String, String> map = new HashMap<String, String>();
			Enumeration headerNames = request.getHeaderNames();
			while (headerNames.hasMoreElements()) {
				String key = (String) headerNames.nextElement();
				String value = request.getHeader(key);
				map.put(key, value);
			}
		//	System.out.println("Header  Key Values :: "+ map);

			return map;
		}
	 	

//		   @SuppressWarnings("unused")
//		private DeviceInformation header.getHeaders(request) {
//			
//			   DeviceInformation	 deviceInformation = new DeviceInformation();
//			Map<String, String> map = new HashMap<String, String>();
//			Enumeration headerNames = request.getHeaderNames();
//			while (headerNames.hasMoreElements()) {
//				String key = (String) headerNames.nextElement();
//				String value = request.getHeader(key);				
//				map.put(key, value);
//			}			
//			deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
//			deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
//			deviceInformation.setDeviceId(map.getOrDefault("deviceid", "NOt Found"));
//			deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
//			deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
//			deviceInformation.setLang(map.getOrDefault("lang", "en"));
//			deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
//			deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
//			deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
//			deviceInformation.setCountryId(Integer.parseInt(map.getOrDefault("ocid", "88")));
//			deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));
//			
//			
//			
//			System.out.println("Header  Key Values :: "+ map);
//
//			return deviceInformation;
//		}
//		   
//	
}
