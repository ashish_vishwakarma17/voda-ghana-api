package com.utility;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.beans.DeviceInformation;


@Component
public class Header implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public DeviceInformation getHeaders(HttpServletRequest request) {
		
		   DeviceInformation	 deviceInformation = new DeviceInformation();
		Map<String, String> map = new HashMap<String, String>();
		Enumeration headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String key = (String) headerNames.nextElement();
			String value = request.getHeader(key);				
			map.put(key, value);
		}			
		deviceInformation.setEvt(map.getOrDefault("evt", "-1"));
		deviceInformation.setApplicationVersion(map.getOrDefault("appv", "1.0.0"));
		deviceInformation.setCcode(map.getOrDefault("ccode", "GH"));
		deviceInformation.setDeviceId(map.getOrDefault("devid", "NOt Found"));
		deviceInformation.setDeviceModel(map.getOrDefault("model", "NOt Found"));
		deviceInformation.setDevicePin(map.getOrDefault("devpin", "NOt Found"));
		deviceInformation.setLang(map.getOrDefault("lang", "en"));
		deviceInformation.setOperatingSystem(map.getOrDefault("os", "NOt Found"));
		deviceInformation.setOperatingSystemVersion(map.getOrDefault("osv", "NOt Found"));
		deviceInformation.setSource(Integer.parseInt(map.getOrDefault("src", "1")));
		deviceInformation.setCountryId(map.getOrDefault("ocid", "88"));
		deviceInformation.setOperatorId(Integer.parseInt(map.getOrDefault("opid", "7")));		
		
		
	//	System.out.println("Header  Key Values :: "+ map);

		return deviceInformation;
	}
	   
	
	

}
