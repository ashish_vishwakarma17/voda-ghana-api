package com.utility;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;

import org.json.JSONObject;

public class UrlProcessor {
    String m_url;
    String m_response = "";

    int connTimeOut = 15000;
    int readTimeOut = 18000;
    JSONObject jsonObject;
    private static UrlProcessor theInstance = null;
    
    
    public static synchronized UrlProcessor getInstance() {
        if (theInstance == null) {
            theInstance = new UrlProcessor();
            theInstance.init();
        }
        return theInstance;
    }
    private synchronized void init() {    	
    	   	
    }
    
//    public UrlProcessor(String theurl) {
//        m_url = theurl.replace(" ", "%20");
//    }

//    public UrlProcessor(String theurl, int conntimeout, int readtimeout) {
//        m_url = theurl.replace(" ", "%20");
//        connTimeOut = conntimeout;
//        readTimeOut = readtimeout;
//    }

//    public void execute() {
//        HttpURLConnection conn = null;
//        try {
//            URL url = new URL(m_url);
//
//            conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(connTimeOut);
//            conn.setReadTimeout(readTimeOut);
//            conn.setRequestMethod("GET");
//            conn.connect();
//
//            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//
//            m_response = in.readLine();
//        } catch (Exception ex) {
//            m_response = "NOK:Network error";
//        } finally {
//            if (m_response == null)
//                m_response = "NOK:Network error";
//
//            if (conn != null) {
//                conn.disconnect();
//                conn = null;
//            }
//        }
//    }
//
//    public void executePostNoParam(JSONObject jsonParam) {
//        HttpURLConnection conn = null;
//        try {
//            URL url = new URL(m_url);
//
//            conn = (HttpURLConnection) url.openConnection();
//            conn.setConnectTimeout(connTimeOut);
//            conn.setReadTimeout(readTimeOut);
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//            conn.setUseCaches(false);
//            conn.setRequestProperty("Content-Type", "application/json");
//
//            conn.connect();
//
//            DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
//            //  printout.writeBytes(jsonParam.toString());
//            printout.flush();
//            printout.close();
//
//            int HttpResult = conn.getResponseCode();
//
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//
//                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
//
//                String line = null;
//                StringBuffer sb = new StringBuffer();
//                while ((line = in.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                in.close();
//
//                m_response = sb.toString();
//
//            } else {
//                m_response = "NOK:Network error";
//            }
//        } catch (Exception ex) {
//            m_response = "NOK:Network error";
//        } finally {
//            if (m_response == null)
//                m_response = "NOK:Network error";
//
//            if (conn != null) {
//                conn.disconnect();
//                conn = null;
//            }
//        }
//    }  
    
    public String getResponse(String theurl, JSONObject jsonParam)
    {
    	m_url = theurl.replace(" ", "%20"); 
    	jsonObject = jsonParam; 
    	
    //	System.out.println("  Enter In Method 1" + m_url);
    //	System.out.println("  Enter In Method 2" + jsonObject.toString());
    	JSONObject objJson = null;
    	
    	executePost();
    	 if (isSuccessful()) {
             try {            	 
                // JSONObject jsonObjectResponse = new JSONObject(m_response);                                
                // if (jsonObjectResponse.getInt("code") == 0 ) {
                	// objJson = jsonObjectResponse;	                	 
                // }
             } catch (Exception e) {	                   
             }
         }  
    //	 m_response = "[{\"message\":\"VENDORID_MISSING\",\"code\":\"1\"}]";
    	 return m_response;
    	
    }

    public void executePost() {
        HttpURLConnection conn = null;
        BufferedReader in = null;
       
        try {
            URL url = new URL(m_url);
           
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(connTimeOut);
            conn.setReadTimeout(readTimeOut);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestProperty("Content-Type", "application/json");

            conn.connect();
        //    System.out.println("  Enter In Method 5");
            DataOutputStream printout = new DataOutputStream(conn.getOutputStream());
            printout.writeBytes(jsonObject.toString());
            printout.flush();
            printout.close();

            int HttpResult = conn.getResponseCode();
          
        //    System.out.println("  Enter In Method Response" + HttpResult);
            
            if (HttpResult == HttpURLConnection.HTTP_OK) {
            	
                in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));

                String line = null;
                StringBuffer sb = new StringBuffer();
                while ((line = in.readLine()) != null) {
                    sb.append(line + "\n");
                }
                
                in.close();

                m_response = sb.toString();
          //      System.out.println("  Enter In Method:8  " + m_response);
            } else {
            	 m_response = "[{\"code\": -2,\"message\": \"NOK:Network error\" }]";
            	 // m_response = "NOK:Network error";
           // 	  System.out.println("  Enter In Method:9" + m_response);
                  
            }
        } catch (Exception e) {
        	System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
           // m_response = "NOK:Network error";
            m_response = "[{\"code\": -2,\"message\": \" "+ e.getMessage().toString() + "\" }]";
        } finally {
            if (m_response == null)
            	m_response = "[{\"code\": -2,\"message\": \"NOK:Network error\" }]";
               // m_response = "NOK:Network error";

            try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
    }

//    public String getResponse() {
//        return m_response;
//    }

    public boolean isSuccessful() {
        boolean bRetval = true;

        if (m_response.length() >= 4) {
            String tmp = m_response.substring(0, 4);
            bRetval = !(tmp.equals("NOK:"));
        }

        return bRetval;
    }

    public boolean isNetworkError() {
        boolean bRetval = false;

        if (m_response.equals("NOK:Network error")) {
            bRetval = true;
        }

        return bRetval;
    }
}
