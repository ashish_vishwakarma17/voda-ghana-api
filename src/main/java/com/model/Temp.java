package com.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Temp  implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ApiModelProperty(notes = "encripted Mobile Number",example="n2XBRtSEzykWFICTZlpARQ==",position=-1)
	@JsonProperty("msisdn")
	private String msisdn = "";
	@JsonProperty("userid")
	private int userId;
	private String trackCode = "";
	@JsonProperty("vendorId")
	private String vendorId = "0";
	
	@JsonProperty("clickId")
	private String clickId = "0";
	
	private int viaId;
	private String userName = "";
	private String emailAddress = "";
	private String password = "";
	private int genderId;
	private String fbProfileId = "";
	@JsonProperty(value="apiversion")
	private String apiVersion ;
	private String result;
	private String error;
	private String errorCode;
	private String subscriptionId;
	
	     
	   
	
	public String getTrackCode() {
		return trackCode;
	}
	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	public String getClickId() {
		return clickId;
	}
	public void setClickId(String clickId) {
		this.clickId = clickId;
	}

	@JsonProperty("serviceType")
	private String serviceType = "";
	
	@JsonProperty("packType")
	private String packType;
	
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	@JsonProperty("packid")
	private int packageId ;
	
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	
	
	public int getViaId() {
		return viaId;
	}
	public void setViaId(int viaId) {
		this.viaId = viaId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		 try {
	            if (userName != null) {
	                this.userName = userName.replaceAll("'", "''");
	            } else {
	                this.userName = "";
	            }
	        } catch (Exception e) {
	            this.userName = "";
	            System.out.println("Exception in Vodafone TempParameter.setUserName(String userName) - " + e.getMessage());
	        }
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
	 try {
            if (emailAddress == null) {
                this.emailAddress = "";
            } else {
                this.emailAddress = emailAddress;
            }
        } catch (Exception e) {
            this.emailAddress = "";
            System.out.println("Exception in Vodafone setEmailAddress(String emailAddress) - " + e.getMessage());
        }
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		try {
            if (password == null) {
                this.password = "";
            } else {
                this.password = password;
            }
        } catch (Exception e) {
            this.password = "";
        }
	}
	public int getGenderId() {
		return genderId;
	}
	public void setGenderId(int genderId) {
		this.genderId = genderId;
	}
	public String getFbProfileId() {
		return fbProfileId;
	}
	public void setFbProfileId(String fbProfileId) {
	 try {
            if (fbProfileId == null) {
                this.fbProfileId = "";
            } else {
                this.fbProfileId = fbProfileId;
            }
        } catch (Exception e) {
            this.fbProfileId = "";
        }
	}
	public String getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getSubscriptionId() {
		return subscriptionId;
	}
	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}	
	

	

}
