package com.model;

import com.beans.Root;

public class InstallResponse extends Root   {
	private long userId;
	private String msisdn;
	 
	
	 
	
	public InstallResponse( int rootCode, String rootMsg, long userId, String msisdn ) {
		super(rootCode,rootMsg);
		this.userId = userId;
		this.msisdn = msisdn;
	 
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
 
	

}
