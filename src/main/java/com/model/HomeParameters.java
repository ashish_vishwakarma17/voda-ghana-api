package com.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class HomeParameters {
	
  	
	@JsonProperty("id")
	private String id = "0";
	
	
	@JsonProperty("subid")
	private String subid = "0";
	
	@JsonProperty("search")
	private String searchKeyword = "";
	
	public String getSubid() {
		return subid;
	}
	public void setSubid(String subid) {
		this.subid = subid;
	}

	@JsonProperty("userid")
	private int userId;
	
		

	@ApiModelProperty(notes = "EType",example="1",position=-1)
	@JsonProperty("etype")	
	private String eventType = "0";
	
//	@JsonProperty("artistid")
//	private int artistId;
	
//	@JsonProperty("email")
//	private String emailAddress = "";	
//	
//	@JsonProperty("gender")
//	private int genderId;
	
	

	@JsonProperty("audioTechRefId")
	private int audioTechRefId;
	@JsonProperty("imageTechRefId")
	private int imageTechRefId;
	
	@JsonProperty("start")
	@ApiModelProperty(notes = "start limit",example="0",position=7)
	private int startLimit;
	@JsonProperty("limit")
	@ApiModelProperty(notes = "End Limit",example="10",position=7)
	private int endLimit;	

	
	@JsonProperty("aformat")
	private String audioFormat = "";
	@JsonProperty("quality")
	private String audioQuality = "";
	
	@JsonProperty("size")
	private String imageSize = "";
	
	@JsonProperty("network")
	private String network = "";
	
	@JsonProperty("msisdn")
	private String msisdn = "";
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		if (id != null) {
			this.id = id;
		}
	}	
	@JsonProperty(value="apiversion")
	private String apiVersion ;
	

public String getApiVersion() {
		return apiVersion;
	}
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
public HomeParameters(String id, String subid, String searchKeyword, int userId, String eventType,
			int audioTechRefId, int imageTechRefId, int startLimit, int endLimit, String audioFormat,
			String audioQuality, String imageSize, String network, String msisdn) {
		super();
		this.id = id;
		this.subid = subid;
		this.searchKeyword = searchKeyword;
		this.userId = userId;
		this.eventType = eventType;
		this.audioTechRefId = 64;
		this.imageTechRefId = 127;
		this.startLimit = startLimit;
		this.endLimit = endLimit;
		this.audioFormat = audioFormat;
		this.audioQuality = audioQuality;
		this.imageSize = imageSize;
		this.network = network;
		this.msisdn = msisdn;
	}
//	public void setArtistId(int artistId) {
//		this.artistId = artistId;
//	}
//	
//	public int getArtistId() {
//		return artistId;
//	}
//
//	public void setArtistId(String artistId) {
//		this.artistId = getIntegerValue(artistId);
//	}

		public String getSearchKeyword() {
			return searchKeyword;
		}
		
		public void setSearchKeyword(String searchKeyword) {
			try {
				if (searchKeyword != null) {
					this.searchKeyword = searchKeyword.replaceAll("'", "''");
				} else {
					this.searchKeyword = "a";
				}
			} catch (Exception e) {
				this.searchKeyword = "a";
				System.out.println(
						"Exception in Mziiki RequestParameter.setSearchKeyword(String searchKeyword) - " + e.getMessage());
			}
		}
	public static int getIntegerValue(String str) {
		int i = -1;
		try {
			if (str == null) {
				i = -1;
			} else {
				i = Integer.parseInt(str);
			}
		} catch (NumberFormatException nfe) {
			i = -1;
		} catch (Exception e) {
			i = -1;
		}
		return i;
	}
	
//	public int getNumericId() {
//		try {
//			int i = Integer.parseInt(numericId);
//			return i;
//		} catch (Exception e) {
//			return 0;
//		}
//
//	}
	
	
	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		try {
			if (network == null) {
				this.network = "gprs";
			} else {
				this.network = network;
			}
		} catch (Exception e) {
			this.network = "gprs";
			System.out.println("Exception in Mziiki RequestParameter.setNetwork(String network) - " + e.getMessage());
		}
	}

	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}

	public void setEndLimit(int endLimit) {
		this.endLimit = endLimit;		
	}
	
	public int getImageTechRefId() {
		return imageTechRefId;
	}
	
	public int getAudioTechRefId() {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
		return audioTechRefId;
	}

	public void setAudioTechRefId(int audioTechRefId) {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
	}
	public String getAudioFormat() {
        if (this.getAudioFormat().equalsIgnoreCase("m4a")) {
            this.setAudioFormat("mp4");
        }
		return audioFormat;
	}

	public void setAudioFormat(String audioFormat) {
		try {
			if (audioFormat == null) {
				this.audioFormat = "m4a";
			} else {
				this.audioFormat = audioFormat;
			}
		} catch (Exception e) {
			this.audioFormat = "m4a";
			System.out.println(
					"Exception in Mziiki RequestParameter.setAudioFormat(String audioFormat) - " + e.getMessage());
		}
	}

	public String getAudioQuality() {
		return audioQuality;
	}

	public void setAudioQuality(String audioQuality) {
		try {
			if (audioQuality == null) {
				this.audioQuality = "auto";
			} else {
				this.audioQuality = audioQuality;
			}
		} catch (Exception e) {
			this.audioQuality = "auto";
			System.out.println(
					"Exception in Vodafone RequestParameter.setAudioQuality(String audioQuality) - " + e.getMessage());
		}
	}
	
	public void setImageTechRefId(int imageTechRefId) {
		try {
			this.imageTechRefId = imageTechRefId;
		} catch (Exception e) {
			this.imageTechRefId = 0;
		}
	}


	public int getStartLimit() {
		return startLimit;
	}

	public int getEndLimit() {
		if(endLimit == 0)
			endLimit = 10;
		return endLimit;
	}
	
	public String getEventType() {
		return eventType;
	}


	public void setEventType(String eventType) {
		try {

			if (eventType == null || eventType.trim().length() <= 0) {
				this.eventType = "1";
			} else {
				this.eventType = eventType;
			}
		} catch (Exception e) {
			this.eventType = "1";
			System.out
					.println("Exception in Vodafone RequestParameter.setEventType(String eventType) - " + e.getMessage());
		}
	}
	
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}	

	
	public String getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}


	public String getImageSize() {
		return imageSize;
	}

	public void setImageSize(String imageSize) {
		try {
			this.imageSize = imageSize;
			if (imageSize == null) {
				// this.imageTechRefId = 119;
				this.imageTechRefId = 125;
			} else if (imageSize.equalsIgnoreCase("XS")) {
				// this.imageTechRefId = 119;
				this.imageTechRefId = 125;
			} else if (imageSize.equalsIgnoreCase("S")) {
				// this.imageTechRefId = 120;
				this.imageTechRefId = 126;
			} else if (imageSize.equalsIgnoreCase("M")) {
				// this.imageTechRefId = 121;
				this.imageTechRefId = 127;
			} else if (imageSize.equalsIgnoreCase("L")) {
				// this.imageTechRefId = 122;
				this.imageTechRefId = 128;
			} else if (imageSize.equalsIgnoreCase("XL")) {
				// this.imageTechRefId = 122;
				this.imageTechRefId = 128;
			} else if (imageSize.equalsIgnoreCase("XXL")) {
				// this.imageTechRefId = 122;
				this.imageTechRefId = 128;
			} else {
				// this.imageTechRefId = 120;
				this.imageTechRefId = 126;
			}
		} catch (Exception e) {
			this.imageTechRefId = 126;
			System.out
					.println("Exception in Mziiki RequestParameter.setImageSize(String imageSize) - " + e.getMessage());
		}
	}



//	public String getEmailAddress() {
//		return emailAddress;
//	}
//
//
//	public void setEmailAddress(String emailAddress) {
//		this.emailAddress = emailAddress;
//	}
//
//
//	public int getGenderId() {
//		return genderId;
//	}
//
//
//	public void setGenderId(int genderId) {
//		this.genderId = genderId;
//	}
	 
	
}
