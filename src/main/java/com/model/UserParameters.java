package com.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class UserParameters {
	
	@JsonProperty("itemtypeid")
	private int itemTypeId;
	
  	
	@JsonProperty("uname")
	private String userName = "";
	
	@ApiModelProperty(notes = "Encripted Mobile Number",example="n2XBRtSEzykWFICTZlpARQ==",position=-1)
	@JsonProperty("msisdn")
	private String msisdn = "";
	
	@ApiModelProperty(notes = "User ID",example="1",position=-6)
	@JsonProperty("userid")
	private int userId;	
		
	@ApiModelProperty(notes = "EType",example="1",position=-1)
	@JsonProperty("etype")
	private String eventType = "0";
	
	@JsonProperty("email")
	private String emailAddress = "";	
	
	@JsonProperty("gender")
	private int genderId;
	
	@JsonProperty("audioTechRefId")
	private int audioTechRefId;
	@JsonProperty("imageTechRefId")
	private int imageTechRefId;
	
	@JsonProperty("start")
	@ApiModelProperty(notes = "start limit",example="0",position=7)
	private int startLimit;
	@JsonProperty("limit")
	@ApiModelProperty(notes = "End Limit",example="10",position=7)
	private int endLimit;
	
	@JsonProperty("id")
	private String id;
	
	
	 
	 
	
	public int getItemTypeId() {
		return itemTypeId;
	}

	public void setItemTypeId(int itemTypeId) {
		this.itemTypeId = itemTypeId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty(value="apiversion")
	private String apiVersion ;
	

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}
	@JsonProperty("aformat")
	private String audioFormat = "";
	@JsonProperty("quality")
	private String audioQuality = "";
	
	@JsonProperty("network")
	private String network = "";

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		try {
			if (network == null) {
				this.network = "gprs";
			} else {
				this.network = network;
			}
		} catch (Exception e) {
			this.network = "gprs";
			System.out.println("Exception in   setNetwork(String network) - " + e.getMessage());
		}
	}
	

	public void setStartLimit(int startLimit) {
		this.startLimit = startLimit;
	}

	public void setEndLimit(int endLimit) {
		this.endLimit = endLimit;
	}
	
	public int getImageTechRefId() {
		return imageTechRefId;
	}

	
	public int getAudioTechRefId() {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
		return audioTechRefId;
	}

	public void setAudioTechRefId(int audioTechRefId) {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
	}
	public String getAudioFormat() {
        if (this.getAudioFormat().equalsIgnoreCase("m4a")) {
            this.setAudioFormat("mp4");
        }
		return audioFormat;
	}

	public void setAudioFormat(String audioFormat) {
		try {
			if (audioFormat == null) {
				this.audioFormat = "m4a";
			} else {
				this.audioFormat = audioFormat;
			}
		} catch (Exception e) {
			this.audioFormat = "m4a";
			System.out.println(
					"Exception in Mziiki RequestParameter.setAudioFormat(String audioFormat) - " + e.getMessage());
		}
	}

	public String getAudioQuality() {
		return audioQuality;
	}

	public void setAudioQuality(String audioQuality) {
		try {
			if (audioQuality == null) {
				this.audioQuality = "auto";
			} else {
				this.audioQuality = audioQuality;
			}
		} catch (Exception e) {
			this.audioQuality = "auto";
			System.out.println(
					"Exception in Mziiki RequestParameter.setAudioQuality(String audioQuality) - " + e.getMessage());
		}
	}


	
	public void setImageTechRefId(int imageTechRefId) {
		try {
			this.imageTechRefId = imageTechRefId;
		} catch (Exception e) {
			this.imageTechRefId = 0;
		}
	}
	
	
	 

	public int getStartLimit() {
		return startLimit;
	}

	public int getEndLimit() {
		return endLimit;
	}

	public String getEventType() {
		return eventType;
	}


	public void setEventType(String eventType) {
		try {

			if (eventType == null || eventType.trim().length() <= 0) {
				this.eventType = "1";
			} else {
				this.eventType = eventType;
			}
		} catch (Exception e) {
			this.eventType = "1";
			System.out
					.println("Exception in Vodafone RequestParameter.setEventType(String eventType) - " + e.getMessage());
		}
	}

	
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getEmailAddress() {
		return emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public int getGenderId() {
		return genderId;
	}


	public void setGenderId(int genderId) {
		this.genderId = genderId;
	}
	 
	
}
