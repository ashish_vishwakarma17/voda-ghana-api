package com.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class Otp {
	
	
	@ApiModelProperty(notes = "encripted Mobile Number",example="n2XBRtSEzykWFICTZlpARQ==",position=-1)
	@JsonProperty("msisdn")
	private String msisdn = "";
	@ApiModelProperty(notes = "oneTimePassword (OTP)",example="",position=-1)
	@JsonProperty("oneTimePassword")
	private String oneTimePassword = "";
	@ApiModelProperty(notes = "transactionId used for OTP verification",example="",position=-1)
	@JsonProperty("transactionId")
	private String transactionId = "0";
	
	@ApiModelProperty(notes = "trackCode is optional. its use when CRBT request will sent.",example="",position=-1)
	@JsonProperty("id")
	private String trackCode = "";
	
	
	@ApiModelProperty(notes = "Userid is optional. its use when CRBT request will sent. ",example="1",position=-6)
	@JsonProperty("userid")
	private int userId;
	
	
	
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getOneTimePassword() {
		return oneTimePassword;
	}
	public void setOneTimePassword(String oneTimePassword) {
		this.oneTimePassword = oneTimePassword;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTrackCode() {
		return trackCode;
	}
	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}
	
	
	
	
	
	 
}
