package com.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserOfflineModel  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3228286045764294491L;
	
	
	
	@JsonProperty("userid")
	private int userId;
	
	@JsonProperty("id")
	private String trackCode = "";
	
	@JsonProperty("quality")
	private String audioQuality = "";
	
	@JsonProperty("dwldquality")
	private String downloadQuality;
	
	@JsonProperty("aformat")
	private String audioFormat = "";
	
	@JsonProperty("audioTechRefId")
	private int audioTechRefId;
	
	@JsonProperty("network")
	private String network = "";
	
	@JsonProperty("imageTechRefId")
	private int imageTechRefId;
	
	 
	@JsonProperty("json")
	private String json = "";
	
	@JsonProperty("usertype")
	private String usertype = "";
	
	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getTrackCode() {
		return trackCode;
	}

	public void setTrackCode(String trackCode) {
		this.trackCode = trackCode;
	}

	public String getAudioQuality() {
		return audioQuality;
	}

	
	
	public String getDownloadQuality() {
		return downloadQuality;
	}

	public void setDownloadQuality(String downloadQuality) {
		this.downloadQuality = downloadQuality;
	}

	public void setAudioQuality(String audioQuality) {
		this.audioQuality = audioQuality;
	}

	public String getAudioFormat() {
		return audioFormat;
	}

	public void setAudioFormat(String audioFormat) {
		this.audioFormat = audioFormat;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public int getImageTechRefId() {
		return imageTechRefId;
	}

	public void setImageTechRefId(int imageTechRefId) {
		this.imageTechRefId = imageTechRefId;
	}

	public int getAudioTechRefId() {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
		return audioTechRefId;
	}

	public void setAudioTechRefId(int audioTechRefId) {
		try {
			try {
				if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					// audioTechRefId=65;
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("3g")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("wifi")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("gprs")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("auto")
						&& this.network.equalsIgnoreCase("bis")) {
					this.audioTechRefId = 111;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 63;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 64;
				} else if (this.audioFormat.equalsIgnoreCase("m4a") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 112;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 57;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 56;
				} else if (this.audioFormat.equalsIgnoreCase("mp3") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 78;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("high")) {
					this.audioTechRefId = 67;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("medium")) {
					this.audioTechRefId = 66;
				} else if (this.audioFormat.equalsIgnoreCase("3gp") && this.audioQuality.equalsIgnoreCase("low")) {
					this.audioTechRefId = 111;
				} else {
					this.audioTechRefId = 63;
				}
			} catch (Exception e) {
				this.audioTechRefId = 63;
				System.out.println("Exception in Mziiki RequestParameter.setAudioTechRefId() - " + e.getMessage());
			}
		} catch (Exception e) {
			this.audioTechRefId = 64;
		}
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	
	

}
