package com.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApplicationInstallViaShareModel  implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("etype")
	private String eventType = "0";
	
	@JsonProperty("userid")
	private int userId;
	
	@JsonProperty("promocode")
	private String promotionCode = "";
	
	@JsonProperty("action")
	private String action = "";

	@JsonProperty("utm_campaign")
	private String campaign;

	@JsonProperty("utm_compaignId")
	private String utm_compaignId;
	
	
	@JsonProperty("utm_medium")
	private String medium;
	@JsonProperty("utm_term")
	private String term;
	@JsonProperty("utm_content")
	private String content;
	
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCampaign() {
		return campaign;
	}

	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}

	public String getUtm_compaignId() {
		return utm_compaignId;
	}

	public void setUtm_compaignId(String utm_compaignId) {
		this.utm_compaignId = utm_compaignId;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@JsonProperty("sourcetype")
	private String sourcetype;


	public String getSourcetype() {
		return sourcetype;
	}

	public void setSourcetype(String sourcetype) {
		this.sourcetype = sourcetype;
	}
	

	
	

}
