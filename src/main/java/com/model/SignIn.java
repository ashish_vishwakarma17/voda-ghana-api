package com.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class SignIn {
	
  	
	@JsonProperty("uname")
	private String userName = "";
	
	@ApiModelProperty(notes = "Encripted Mobile Number",example="n2XBRtSEzykWFICTZlpARQ==",position=-1)
	@JsonProperty("msisdn")
	private String msisdn = "";
	
	@ApiModelProperty(notes = "User ID",example="1",position=-6)
	@JsonProperty("userid")
	private int userId;	
		
	@ApiModelProperty(notes = "EType",example="1",position=-1)
	@JsonProperty("etype")
	private String eventType = "0";

	@JsonProperty("email")
	private String emailAddress = "";
	
	@JsonProperty("passwd")
	private String password = "";
	
	@JsonProperty("gender")
	private int genderId;
	
	
	@JsonProperty("fbProfileId")
	private String fbProfileId = "";	
	
	
	public SignIn(String userName, String msisdn, int userId, String eventType, String emailAddress, String password,
			int genderId, String fbProfileId) {
		super();
		this.userName = userName;
		this.msisdn = msisdn;
		this.userId = userId;
		this.eventType = eventType;
		this.emailAddress = emailAddress;
		this.password = password;
		this.genderId = genderId;
		this.fbProfileId = fbProfileId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		try {

			if (eventType == null || eventType.trim().length() <= 0) {
				this.eventType = "1";
			} else {
				this.eventType = eventType;
			}
		} catch (Exception e) {
			this.eventType = "1";
			System.out
					.println("Exception in Vodafone RequestParameter.setEventType(String eventType) - " + e.getMessage());
		}
	}


	
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	

	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getMsisdn() {
		return msisdn;
	}


	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}


	public String getEmailAddress() {
		return emailAddress;
	}
	
	public void setEmailAddress(String emailAddress) {
		try {
			if (emailAddress == null) {
				this.emailAddress = "";
			} else {
				this.emailAddress = emailAddress;
			}
		} catch (Exception e) {
			this.emailAddress = "";
			System.out.println("Exception in Mziiki setEmailAddress(String emailAddress) - " + e.getMessage());
		}
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public int getGenderId() {
		return genderId;
	}


	public void setGenderId(int genderId) {
		this.genderId = genderId;
	}


	public String getFbProfileId() {
		return fbProfileId;
	}


	public void setFbProfileId(String fbProfileId) {
		try {
			if (fbProfileId == null) {
				this.fbProfileId = "";
			} else {
				this.fbProfileId = fbProfileId;
			}
		} catch (Exception e) {
			this.fbProfileId = "";
		}
	}	
	 
	
}
