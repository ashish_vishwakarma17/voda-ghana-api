package com.database;

 
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.sql.DataSource;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.beans.DownloadHistory;
import com.app.beans.DownloadPackage;
import com.app.beans.SubscribedPackage;
import com.app.beans.SubscriptionBenefits;
import com.app.beans.SubscriptionPackage;
import com.beans.AlbumData;
import com.beans.AppVersionInfoData;
import com.beans.ArtistData;
import com.beans.CciPortalResponse;
import com.beans.ContainerBean;
import com.beans.ContainerItemBean;
import com.beans.CountInfo;
import com.beans.CountInfoData;
import com.beans.CountryInfoData;
import com.beans.DashBoardData;
import com.beans.DedicateeData;
import com.beans.DedicationData;
import com.beans.DeviceInformation;
import com.beans.DownloadURL;
import com.beans.FeaturedPlaylistBean;
import com.beans.FeedbackSubjectData;
import com.beans.FollowKaraokeArtistData;
import com.beans.GenreBean;
import com.beans.HikeKeyboardItemBean;
import com.beans.HomeItemData;
import com.beans.KaraokeCommentData;
import com.beans.LeftMenuTitle;
import com.beans.MobileOperatorData;
import com.beans.NotificationData;
import com.beans.OperatorData;
import com.beans.OptScreenConfig;
import com.beans.PaymentMethod;
import com.beans.PromotionShareConfig;
import com.beans.RadioBean;
import com.beans.RequestParameter;
import com.beans.Root;
import com.beans.SignUpViaConfigurationBean;
import com.beans.Splash;
import com.beans.SplashScreenData;
import com.beans.TrackData;
import com.beans.Transaction;
import com.beans.UserInfoData;
import com.beans.UserOfflineInformationBean;
import com.beans.karaokeTrackData;
import com.model.ApplicationInstallViaShareModel;
import com.model.HomeParameters;
import com.model.Otp;
import com.model.SignIn;
import com.model.Temp;
import com.model.UserOfflineModel;
import com.model.UserParameters;
import com.utility.UrlProcessor;

import beans.config.AESEncriptionDecription;
import beans.config.AudioQuality;
import beans.config.AudioQualityConfiguration;
import beans.config.Configuration;
import beans.config.ConfigurationValue;
import beans.config.Visibility;
import beans.config.VisibilityValue;

@Component
public class DataBaseProcedures {

	@Autowired
	private DataSource dataSource;
	@Value("${api.domain.name}")
	private String apiDomainName = "";
	
	
	@Autowired
	private JdbcTemplate jdbcTemplateObject;

	@SuppressWarnings("finally")
	public AppVersionInfoData getAppVersionInfo( DeviceInformation  deviceInformation) {
		AppVersionInfoData appVersionInfoData = null;
		try {

			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("ApplicationVersionInformation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
				    .addValue("inCountryCode", deviceInformation.getCcode())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inApplicationVersion", deviceInformation.getApplicationVersion());					
			
				Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
				
				appVersionInfoData = new AppVersionInfoData(
						resultMap.get("android").toString().toString(),
						resultMap.get("android_update_msg").toString(),
						resultMap.get("android_tab").toString(),
						resultMap.get("android_tab_update_msg").toString(),
						resultMap.get("ios").toString(),
						resultMap.get("ios_update_msg").toString(),
						resultMap.get("blackberry").toString(),
						resultMap.get("blackberry_update_msg").toString(),
						resultMap.get("blackberry10").toString(),
						resultMap.get("blackberry10_update_msg").toString(),
						resultMap.get("windows").toString(),
						resultMap.get("windows_update_msg").toString(),
						Integer.parseInt(resultMap.get("update_popup_frequency").toString()),
						resultMap.get("cache").toString(), 
						Integer.parseInt(resultMap.get("update_required").toString()),
						resultMap.get("update_message").toString());
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `ApplicationVersionInformation`('" + countryCode + "','"
//					+ operatingSystem + "','" + applicationVersion + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					appVersionInfoData = new AppVersionInfoData(rs.getString("android"),
//							rs.getString("android_update_msg"), rs.getString("android_tab"),
//							rs.getString("android_tab_update_msg"), rs.getString("ios"), rs.getString("ios_update_msg"),
//							rs.getString("blackberry"), rs.getString("blackberry_update_msg"),
//							rs.getString("blackberry10"), rs.getString("blackberry10_update_msg"),
//							rs.getString("windows"), rs.getString("windows_update_msg"),
//							rs.getInt("update_popup_frequency"), rs.getString("cache"), rs.getInt("update_required"),
//							rs.getString("update_message"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception in setAppVersionInfo() - " + e.getMessage());
		} finally {
			return appVersionInfoData;
		}
	}

	// Get Total Follow Folled Artist
	@SuppressWarnings("finally")
	public String getFollowArtist(RequestParameter reqParam) {
		String response = "0#0#0";
		try {
			
//			MySQL1 mysql = new MySQL1();
//			ResultSet rs1 = mysql.prepareCall(
//					"{CALL `FOLLOWKARAOKEARTIST`(5," + reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FOLLOWKARAOKEARTIST");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("IN_FLAG",5)
					.addValue("in_USER_ID", reqParam.getUserId())
					.addValue("in_FOLLOW_USER_ID", reqParam.getArtistId());
			  
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
			 //   System.out.println("FOLLOWKARAOKEARTIST in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
				response = resultMap.get("follow_count").toString() + "#" + resultMap.get("followed_count").toString() + "#"
						+ resultMap.get("vFollowing").toString();
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FOLLOWKARAOKEARTIST`(5," + reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					response = rs.getString("follow_count") + "#" + rs.getString("followed_count") + "#"
//							+ rs.getString("vFollowing");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		 
		} finally {
			return response;
		}
	}

	public List<ContainerBean> getKaraokeContainerList(RequestParameter reqParam, DeviceInformation devInfo) {
		List<ContainerBean> lst = new ArrayList<ContainerBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetKaraokeContainerList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inDeviceId", devInfo.getDeviceId())
					.addValue("in_lang", devInfo.getLang());
					
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				    
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  int containder_id = 0;
		    	  containder_id = Integer.parseInt(resultMap.get("container_id").toString());
					if (containder_id == 4 || containder_id == 14 || containder_id == 24 || containder_id == 34
							|| containder_id == 44 || containder_id == 54 || containder_id == 64 || containder_id == 74
							|| containder_id == 84 || containder_id == 94 || containder_id == 104
							|| containder_id == 114) {
						lst.add(new ContainerBean(Integer.parseInt(resultMap.get("container_id").toString()), Integer.parseInt(resultMap.get("seq_id").toString()),
								Integer.parseInt(resultMap.get("container_type_id").toString()), rs.get("container_title").toString(), Integer.parseInt(resultMap.get("see_all").toString()),
										Integer.parseInt(resultMap.get("item_list_type_id").toString()),
								getContainerItemList(devInfo.getCountryId(), reqParam.getUserId(),
										Integer.parseInt(resultMap.get("container_id").toString()), reqParam.getImageTechRefId(), 0, 6,reqParam.getApiVersion())));
					} else {
						lst.add(new ContainerBean(Integer.parseInt(resultMap.get("container_id").toString()), Integer.parseInt(resultMap.get("seq_id").toString()),
								Integer.parseInt(resultMap.get("container_type_id").toString()), rs.get("container_title").toString(), Integer.parseInt(resultMap.get("see_all").toString()),
										Integer.parseInt(resultMap.get("item_list_type_id").toString()),
								getContainerItemList(devInfo.getCountryId(), reqParam.getUserId(),
										Integer.parseInt(resultMap.get("container_id").toString()), reqParam.getImageTechRefId(), 0, 20,reqParam.getApiVersion())));
					}		    				    			  
		    	  });
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetKaraokeContainerList`(" + reqParam.getCountryId() + ",'"
//					+ devInfo.getDeviceId() + "','" + reqParam.getLanguageCode() + "')}");
//			int containder_id = 0;
//			if (rs != null) {
//				while (rs.next()) {
//					containder_id = rs.getInt("container_id");
//					if (containder_id == 4 || containder_id == 14 || containder_id == 24 || containder_id == 34
//							|| containder_id == 44 || containder_id == 54 || containder_id == 64 || containder_id == 74
//							|| containder_id == 84 || containder_id == 94 || containder_id == 104
//							|| containder_id == 114) {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getKaraokeItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 6)));
//					} else {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getKaraokeItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 20)));
//					}
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	@SuppressWarnings("finally")
	public List<SubscriptionPackage> getSubscriptionPackageList(DeviceInformation deviceInformation) {
		List<SubscriptionPackage> lst = new ArrayList<SubscriptionPackage>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("SubscriptionPackage");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()					
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inOperatorId", deviceInformation.getOperatorId());
					
//					.addValue("in_lang", deviceInformation.getLang())
//					.addValue("inOS", deviceInformation.getOperatingSystem());
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new SubscriptionPackage(Integer.parseInt(resultMap.get("package_id").toString()), 
		    			  resultMap.get("package_name").toString(),
		    			  resultMap.get("package_description").toString(),
		    			  resultMap.get("price_info").toString(),
		    			  resultMap.get("subscription_amount").toString(),
		    			  resultMap.get("renewal_amount").toString(),
		    			  resultMap.get("validity_period").toString(),
		    			  Integer.parseInt(resultMap.get("validity_in_days").toString()),
		    			  resultMap.get("currency_sign").toString()
		    			  
		    			  ));	
		    				    			  
		    	  });
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `SubscriptionPackage`(" + reqParam.getCountryId() + "," + reqParam.getOperatorId() + ",'"
//							+ reqParam.getLanguageCode() + "','" + reqParam.getOperatingSystem() + "')}");
//			while (rs.next()) {
//				lst.add(new SubscriptionPackage(rs.getInt("package_id"), rs.getString("package_name"),
//						rs.getString("package_description"), rs.getString("price_info"),
//						rs.getString("subscription_amount"), rs.getString("renewal_amount"),
//						rs.getString("validity_period"), rs.getInt("validity_in_days"), rs.getString("currency_sign"),
//						rs.getString("redirectUrl")));
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	 
	@SuppressWarnings("finally")
	public List<DownloadPackage> getDownloadPackageList(RequestParameter reqParam) {
		List<DownloadPackage> lst = new ArrayList<DownloadPackage>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("DownloadPackage");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inOperatorId", reqParam.getOperatorId());
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new DownloadPackage(
		    			  Integer.parseInt(resultMap.get("package_id").toString()), 
		    			  resultMap.get("package_name").toString(),
		    			  resultMap.get("package_description").toString(),
		    			  resultMap.get("price_info").toString(),
		    			  resultMap.get("subscription_amount").toString(),
		    			  resultMap.get("renewal_amount").toString(),
		    			  resultMap.get("validity_period").toString(),
		    			  Integer.parseInt(resultMap.get("validity_in_days").toString()),
		    			  resultMap.get("currency_sign").toString()
		    			  ));	
		    				    			  
		    	  });
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `DownloadPackage`(" + reqParam.getCountryId() + "," + reqParam.getOperatorId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new DownloadPackage(rs.getInt("package_id"), rs.getString("package_name"),
//							rs.getString("package_description"), rs.getString("price_info"),
//							rs.getString("subscription_amount"), rs.getString("renewal_amount"),
//							rs.getString("validity_period"), rs.getInt("validity_in_days"),
//							rs.getString("currency_sign")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

//	public int requestSubscription(Temp reqParam , DeviceInformation  deviceInformation) {
//		int responseCode = 110;
//		try {
//				SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Subscribe");			  			  
//				  SqlParameterSource inParams = new MapSqlParameterSource()
//						
//						.addValue("inCountryId", deviceInformation.getCountryId())
//						.addValue("inOperatorId", deviceInformation.getOperatorId())
//						.addValue("inSourceId", deviceInformation.getSource())
//						.addValue("inBillingViaId", 1)
//						.addValue("inUserId", reqParam.getUserId())
//						.addValue("inPackageId", reqParam.getPackageId())
//						.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
//				  
//
//				  Map<String, Object> rs = jdbcCall.execute(inParams);
//	 				///////      System.out.println("sign in response :: "+ rs);					      
//	 				ArrayList<Object> ar = new ArrayList<Object>();
//	 				ar = (ArrayList) rs.get("#result-set-1");
//	 				Map resultMap = (Map) ar.get(0);				    
//	 				responseCode =Integer.parseInt(resultMap.get("code").toString());
//				 				
//				 				
////				ResultSet rs = mysql.prepareCall("{CALL `Subscribe`(" + reqParam.getCountryId() + ","
////						+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + "," + reqParam.getViaId() + ","
////						+ reqParam.getUserId() + "," + reqParam.getEventType() + ",'" + reqParam.getMsisdn() + "')}");
////				if (rs != null) {
////					while (rs.next()) {
////						responseCode = rs.getInt("code");
////					}
////					System.out.println("responseCode ::  " + responseCode);
////				}
//			 
//			//mysql.close();
//		} catch (Exception e) {
//			responseCode = 110;
//			System.out.println("Exception in Vodafone .requestSubscription(RequestParameter reqParam) - "
//					+ e.getMessage());
//		} finally {
//			return responseCode;
//		}
//	}
//
//
	@SuppressWarnings("finally")
	public int requestActivate(Temp reqParam  , DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Activate");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inOperatorId", deviceInformation.getOperatorId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inBillingViaId", 1)
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inPackageId", reqParam.getPackageId())
			  		.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());

				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `Activate`(" + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + "," + reqParam.getViaId() + ","
//					+ reqParam.getUserId() + "," + reqParam.getEventType() + ",'" + reqParam.getMsisdn() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}
	public String getFinalRedirectedUrl(String url) {
		HttpURLConnection connection;
		String finalUrl = url;
		try {
			do {
				connection = (HttpURLConnection) new URL(finalUrl).openConnection();
				connection.setInstanceFollowRedirects(false);
				connection.setUseCaches(false);
				connection.setRequestMethod("GET");
				connection.connect();
				int responseCode = connection.getResponseCode();
				if (responseCode >= 300 && responseCode < 400) {
					String redirectedUrl = connection.getHeaderField("Location");
					if (null == redirectedUrl) {
						break;
					}
					finalUrl = redirectedUrl;
				} else {
					break;
				}
			} while (connection.getResponseCode() != HttpURLConnection.HTTP_OK);
			connection.disconnect();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return finalUrl;
	}



	public int CampaignTracking(Temp reqParam, DeviceInformation deviceinformation) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("CampaignTracking");
			SqlParameterSource inParams = new MapSqlParameterSource().addValue("inResourceCode", reqParam.getTrackCode())
			//.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
			.addValue("inMsisdn", reqParam.getMsisdn())
			;

			Map<String, Object> rs = jdbcCall.execute(inParams);
			/////// System.out.println("sign in response :: "+ rs);
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);
			responseCode = Integer.parseInt(resultMap.get("code").toString());

		} catch (Exception e) {
			
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}

	}
	@SuppressWarnings("finally")
	public UserInfoData SubscriptionViaCampaign(Temp reqParam, DeviceInformation deviceinformation) {
		UserInfoData userinfodata = null;
		try {
			
			System.out.println("call SubscriptionViaCampaign ("+deviceinformation.getCountryId()+", "+deviceinformation.getSource()+", "+deviceinformation.getSource()+" ,'"+deviceinformation.getOperatingSystemVersion()+"'  + '"+ deviceinformation.getDeviceModel()+"' ,'"+deviceinformation.getDeviceId()+"' ,'"+ deviceinformation.getDevicePin()+"' ,'"+reqParam.getViaId()+"' ,'"+reqParam.getUserName().replaceAll("'", "''")+"' , '"+reqParam.getUserName().replaceAll("'", "''")+"' , '"+ AESEncriptionDecription.decrypt(reqParam.getMsisdn())+"','"+reqParam.getPassword().replaceAll("'", "''")+"' ,"+reqParam.getGenderId()+" ,'' ,  '"+reqParam.getResult()+"'  ,'"+reqParam.getError()+"' , '"+reqParam.getErrorCode()+"','"+ reqParam.getSubscriptionId()+"'  ");

			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("SubscriptionViaCampaign");
			SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", deviceinformation.getCountryId())
					.addValue("inSourceId", deviceinformation.getSource())
					.addValue("inOS", deviceinformation.getOperatingSystem())
					.addValue("inOSV", deviceinformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceinformation.getDeviceModel())
					.addValue("inDevId", deviceinformation.getDeviceId())
					.addValue("inDevPin", deviceinformation.getDevicePin())
					.addValue("inPackageId", reqParam.getViaId())
					.addValue("inCmpName", reqParam.getUserName().replaceAll("'", "''"))
					.addValue("inTrackingID", reqParam.getEmailAddress().replaceAll("'", "''"))
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()) )
					.addValue("inPassword", reqParam.getPassword().replaceAll("'", "''"))
					.addValue("inGenderId", reqParam.getGenderId())					
					.addValue("inProfilePictureUrl", "")
					
					.addValue("inResult", reqParam.getResult())
					.addValue("inError", reqParam.getError())
					.addValue("inErrorCode", reqParam.getErrorCode())
					.addValue("inSubscriptionId", reqParam.getSubscriptionId())
					
					;

			Map<String, Object> rs = jdbcCall.execute(inParams);
			/////// System.out.println("sign in response :: "+ rs);
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			// Map resultMap1 = (Map) ar.get(0);

			
			for(int i =0; i< ar.size();i++)
			{
					  

				          Map<String, Object> resultMap = (Map<String, Object>) ar.get(i);				          
				          userinfodata = new UserInfoData((resultMap.get("user_id").toString()),
						  resultMap.get("user_promo_code").toString(), resultMap.get("user_name").toString(),
						  resultMap.get("mobile_number").toString(), resultMap.get("email_address").toString(),
						  Integer.parseInt(resultMap.get("gender_id").toString()),
						  resultMap.get("password").toString(), resultMap.get("image_url").toString(),						  
						  Integer.parseInt(resultMap.get("user_type_id").toString()),
						  Integer.parseInt(resultMap.get("user_status").toString()),
						  Integer.parseInt(resultMap.get("action_id").toString()),
						  Integer.parseInt(resultMap.get("registered").toString()),
						  Integer.parseInt(resultMap.get("mobile_number_verification").toString()),
						  resultMap.get("billing_mobile_number").toString(),
						  resultMap.get("dialing_code").toString(),
						  Integer.parseInt(resultMap.get("mobile_number_length").toString()),
						  (resultMap.get("titleMsg").toString()),
						  (resultMap.get("TrialMsg").toString()),						  
						  new SubscribedPackage(Integer.parseInt(resultMap.get("package_id").toString()),
								  resultMap.get("package_description").toString(), resultMap.get("price_info").toString(),
						  (resultMap.get("package_start_date").toString()),
						  (resultMap.get("package_start_date").toString()),
						  (resultMap.get("package_end_date").toString()),
						  resultMap.get("android_token").toString(),
						  resultMap.get("ios_token").toString())
						  
						  );		   
			}
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
			userinfodata = null;
			
		} finally {
			return userinfodata;
		}
	}
	
	public Map getPackageDetails(int packageID) {
		Map resultMap = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PackageDetails");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inPackageID", packageID);			  

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				resultMap = (Map) ar.get(0);	    
				 
			
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return resultMap;
		}
	}
	
	public Map getSubscribedPackageDetails(String sService, String sPackType) {
		Map resultMap = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Subscribed_PackageDetails");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inPackType", sPackType)
					.addValue("inService", sService);

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				resultMap = (Map) ar.get(0);	    
			//	System.out.println("Subscribed details:-   ....  "+ resultMap.toString());
					 
			
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return resultMap;
		}
	}


	@SuppressWarnings("finally")
	public int requestSubscription(Temp reqParam,DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			// MySQL mysql = new MySQL();
			if (deviceInformation.getOperatorId() != -1) {

				Map resultMap = getPackageDetails(reqParam.getPackageId());
			
				String strHitURL = "";
				String strPackType = resultMap.get("package_name").toString();// String.valueOf(reqParam.getPackType());
				String strService = "APP";
				if (resultMap.get("package_description").toString().equalsIgnoreCase("Trail")) {
					strService = "APPTRIAL";
				}

				JSONObject jsonParam = new JSONObject();
				JSONObject jsonResponse = new JSONObject();
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
				jsonParam.put("lang", "e");
				jsonParam.put("service", strService);

				if (deviceInformation.getSource() == 1) {
					jsonParam.put("actSource", "APP");
				} else
					jsonParam.put("actSource", "WAP");

				jsonParam.put("packType", strPackType);
				jsonParam.put("validity", resultMap.get("validity_in_days").toString());

				jsonParam.put("transId", getTimeMillisecond());
				jsonParam.put("clickID","0");
				jsonParam.put("vendorID", "0");
				strHitURL = getURLValue("api.subscription.sub");

				String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
				jsonService = new JSONArray(jsonresponse);
				
				// JSONObject jsonObject;
				jsonResponse = jsonService.getJSONObject(0);

				
				//jsonResponse = new JSONObject(jsonresponse);

				try {
					int iCode = 110;
					if (jsonResponse != null) {
						iCode = Integer.parseInt(jsonResponse.getString("code"));
						if (iCode == 0) {
							responseCode = 248;
//							if(String.valueOf(reqParam.getPackType()).equalsIgnoreCase("BUNDLE_TNB")) {
//								responseCode = UpdateFreeTrail(reqParam.getMsisdn());
//							}	
						} else  {							
							responseCode = 511;
						}
 

					} else
						responseCode = 110;

				} catch (Exception ex) {
					responseCode = 110;
				}

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "SUB",
							jsonParam.toString(), strHitURL, jsonResponse.toString(),"");
				} catch (Exception ex) {
					ex.printStackTrace();
					System.out.println("Exception in Vodafone DatabaseProcedures - requestSubscription " + ex.getMessage());
				}
			}
		} catch (Exception e) {
			
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}
	public int requestSubscription(Temp reqParam, DeviceInformation deviceInformation, String sServiceType,String sPackType) {
		int responseCode = 110;
		try {
			// MySQL mysql = new MySQL();
			if (deviceInformation.getOperatorId() != -1) {

				String strHitURL = "";

				JSONObject jsonParam = new JSONObject();
				JSONObject jsonResponse = new JSONObject();
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
				jsonParam.put("lang", "e");
				jsonParam.put("service", sServiceType);

				if (deviceInformation.getSource() == 1) {
					jsonParam.put("actSource", "APP");
				} else
					jsonParam.put("actSource", "WAP");

				jsonParam.put("packType", sPackType);

				jsonParam.put("transId", getTimeMillisecond());
				jsonParam.put("clickID", reqParam.getClickId());
				jsonParam.put("vendorID", reqParam.getVendorId());
				strHitURL = getURLValue("api.subscription.sub");

				String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
				jsonService = new JSONArray(jsonresponse);
				
				// JSONObject jsonObject;
				jsonResponse = jsonService.getJSONObject(0);

				try {
					int iCode = 110;
					if (jsonResponse != null) {
						iCode = Integer.parseInt(jsonResponse.getString("code"));
						if (iCode == 0) {
							responseCode = 248;
//							if(String.valueOf(reqParam.getPackType()).equalsIgnoreCase("BUNDLE_TNB")) {
//								responseCode = UpdateFreeTrail(reqParam.getMsisdn());
//							}	
						} else if (iCode == 2)
							responseCode = 511;
						else if (iCode == 3)
							responseCode = 512;
						else if (iCode == 4)
							responseCode = 514;
						else if (iCode == 5)
							responseCode = 515;
						else if (iCode == -1)
							responseCode = 110;

//						if (iCode != -2) {
//							UpdateFreeTrail(reqParam.getMsisdn());
//						}

					} else
						responseCode = 110;

				} catch (Exception ex) {
					responseCode = 110;
				}

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "SUB",
							jsonParam.toString(), strHitURL, jsonResponse.toString(),reqParam.getVendorId());
				} catch (Exception ex) {
					System.out
							.println("Exception in devotionalportalMainServlet.insertURLTracking - " + ex.getMessage());
				}
			} else {
				responseCode = 110;
			}
			// mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}
	@Autowired
	private Environment env;

	JSONArray jsonService = null;
	// JSONObject jsonbundle = null, jsonkaraoke = null, jsonmusic = null;
	int iStatus = 6;
	String strBillDate = "";

	@Autowired
	private MessageSource messageSource;

	@GetMapping("/property")
	public String getURLValue(@RequestParam("key") String key) {
		String returnValue = "";

		String keyValue = env.getProperty(key);

		if (keyValue != null && !keyValue.isEmpty()) {
			returnValue = keyValue;
		}
		return returnValue;
	}

	public int insertURLTracking(String sMSISDN, String sUrltype, String sJsonParam, String sUrlHit,
			String sUrlResponse,String sCampaign) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("PRO_URL_TRACKING");
			SqlParameterSource inParams = new MapSqlParameterSource().addValue("inMSISDN", sMSISDN)
					.addValue("inURLTYPE", sUrltype).addValue("inJSONPARAM", sJsonParam).addValue("inURLHIT", sUrlHit)
					.addValue("inURLRESPONSE", sUrlResponse)
					.addValue("inCampaign", sCampaign);

			Map<String, Object> rs = jdbcCall.execute(inParams);
			/////// System.out.println("sign in response :: "+ rs);
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);
			responseCode = Integer.parseInt(resultMap.get("code").toString());

		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}
	private String getTimeMillisecond() {
		Date date = new Date();
		return String.valueOf(date.getTime()).substring(0, 9);
	}
	/// Get Mobile Number
		private String getMobileNumber(String p_mobileNumber) {
			try {

				return p_mobileNumber.substring(p_mobileNumber.length() - 9, p_mobileNumber.length());

			} catch (Exception ex) {
				return "";
			}

		}

		private String getDateFormat(String sDate) {

			String sDateFormatted = "";
			if (!sDate.trim().equalsIgnoreCase("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date convertedCurrentDate;
				try {
					convertedCurrentDate = sdf.parse(sDate);
					sDateFormatted = sdf.format(convertedCurrentDate);
				} catch (ParseException e) {
				
					System.out.print("Date got:" + sDate + "/n" + e.getMessage());
					// e.printStackTrace();
				}
			}
			return sDateFormatted;
		}
		//// Get Status
		private int getStatusID(String strStatusValue) {
			int iStatus = -1;
			try {
				iStatus = Integer.parseInt(strStatusValue);
			} catch (Exception ex) {
			}
			return iStatus;
		}

		/// Get ActionId based on UserStatus
		private int getActionID(int userStatus) {
			int actionID = 1;  // show package 

			if (userStatus == 1 || userStatus == 11) /// Subscribed
				actionID = 2;  
			else if (userStatus == 66 || userStatus == 100) /// UnSubID/DND
				actionID = 1;
			else if (userStatus == 3 || userStatus == 6 || userStatus == 13 || userStatus == 16) /// low balance
				actionID = 1;
			else if (userStatus == 10 || userStatus == 0) /// New Sub under process
				actionID = 4;
			else // userStatus == 10 means pending
				actionID = 1;
			return actionID;

		}

		/// Get TypeId based on UserStatus
		private int getTypeID(int userStatus) {
			int typeID = 1;

			if (userStatus == 1 || userStatus == 11) /// Subscribed
				typeID = 3;
			else if (userStatus == 66 || userStatus == 100) /// UnSub/DND
				typeID = 1;
			else if (userStatus == 3 || userStatus == 6 || userStatus == 13 || userStatus == 16) /// low balance
				typeID = 2;
			else if (userStatus == 10 || userStatus == 0) /// New Sub Under Process
				typeID = 3;

//				if (userStatus == 2 || userStatus == 66)
//					typeID = 3;			
//				else
//					typeID = 2;

			return typeID;
		}

	private int UpdateFreeTrail(String smsisdn) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("UpdateFreeTrailStatus");
			SqlParameterSource inParams = new MapSqlParameterSource().addValue("inMsisdn", smsisdn);

			Map<String, Object> rs = jdbcCall.execute(inParams);
			/////// System.out.println("sign in response :: "+ rs);
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);
			responseCode = Integer.parseInt(resultMap.get("code").toString());

		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}

	}
	
	@SuppressWarnings("finally")
	public int requestUnSubscription(Temp reqParam, DeviceInformation deviceinformation) {
		int responseCode = 110;
		try {

		//	Map resultMap = getPackageDetails(reqParam.getPackageId());
			
			String strHitURL = "";
			String strPackType = "NA";//resultMap.get("package_name").toString();// String.valueOf(reqParam.getPackType());
			String strService = "NA";
//			if (resultMap.get("package_description").toString().equalsIgnoreCase("Trail")) {
//				strService = "APPTRIAL";
//			}

			JSONObject jsonParam = new JSONObject();
			JSONObject jsonResponse = new JSONObject();

			jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
			jsonParam.put("lang", "e");
			jsonParam.put("service", strService);

			if (deviceinformation.getSource() == 1) {
				jsonParam.put("actSource", "APP");
			} else
				jsonParam.put("actSource", "WAP");

			jsonParam.put("packType", strPackType);
			jsonParam.put("validity","0");

			jsonParam.put("transId", getTimeMillisecond());
			jsonParam.put("clickID", "0");
			jsonParam.put("vendorID", "0");

			strHitURL = getURLValue("api.subscription.unsub");

			// jsonResponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);

			String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
			//jsonResponse = new JSONObject(jsonresponse);
			jsonService = new JSONArray(jsonresponse);
			
			// JSONObject jsonObject;
			jsonResponse = jsonService.getJSONObject(0);

			try {
				int iCode = 110;
				if (jsonResponse != null) {
					iCode = Integer.parseInt(jsonResponse.getString("code"));
					if (iCode == 0)
						responseCode = 158;
					else if (iCode == 1)
						responseCode = 513;
					else if (iCode == -1)
						responseCode = 110;

				} else
					responseCode = 110;

			} catch (Exception ex) {
				responseCode = 110;
			}

			try {
				insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "UNSUB", jsonParam.toString(),
						strHitURL, jsonResponse.toString(),"");
			} catch (Exception ex) {
				System.out.println("Exception in VodaFone DatabaseProcedures requestUnSubscription - " + ex.getMessage());
			}

		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	 
//	public int requestUnSubscription(Temp reqParam) {
//		int responseCode = 110;
//		try {
//			
//			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Unsubscribe");			  			  
//			  SqlParameterSource inParams = new MapSqlParameterSource()
//					.addValue("inUserId", reqParam.getUserId());			  
//
//			  Map<String, Object> rs = jdbcCall.execute(inParams);
//				///////      System.out.println("sign in response :: "+ rs);					      
//				ArrayList<Object> ar = new ArrayList<Object>();
//				ar = (ArrayList) rs.get("#result-set-1");
//				Map resultMap = (Map) ar.get(0);				    
//				responseCode =Integer.parseInt(resultMap.get("code").toString());  
////			MySQL mysql = new MySQL();
////			ResultSet rs = mysql.prepareCall("{CALL `Unsubscribe`(" + reqParam.getUserId() + ")}");
////			if (rs != null) {
////				while (rs.next()) {
////					responseCode = rs.getInt("code");
////				}
////			}
//			//mysql.close();
//		} catch (Exception e) {
//			responseCode = 110;
//			System.out
//					.println("Exception in Vodafone .requestUnSubscription(RequestParameter reqParam) - "
//							+ e.getMessage());
//		} finally {
//			return responseCode;
//		}
//	}

	
	@SuppressWarnings("finally")
	public int appInstallViaShareClickTraking(ApplicationInstallViaShareModel reqParam  , DeviceInformation  deviceInformation) {
		int resultCode = 317;
		try {

			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("appInstallViaShareClickTraking");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inEventType",reqParam.getEventType())
					  	.addValue("inCountryCode",deviceInformation.getCcode())
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inApplicationVersion", deviceInformation.getApplicationVersion())
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())						  
						.addValue("inUserId", reqParam.getUserId() )
						.addValue("utm_campaign", reqParam.getCampaign())
						.addValue("utm_source", reqParam.getSourcetype())
						.addValue("utm_medium", reqParam.getMedium())
						.addValue("utm_term", reqParam.getTerm())
						.addValue("utm_content", reqParam.getContent());
			 
			    Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				resultCode =Integer.parseInt(resultMap.get("code").toString());
				
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `appInstallViaShareClickTraking`('" + reqParam.getEventType()
//					+ "','" + reqParam.getCountryCode() + "'," + reqParam.getCountryId() + "," + reqParam.getSourceId()
//					+ ",'" + reqParam.getApplicationVersion() + "','" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getCampaign() + "',   '" + reqParam.getSource() + "' ,  '" + reqParam.getMedium()
//					+ "'  ,  '" + reqParam.getTerm() + "'  ,    '" + reqParam.getContent() + "'           )}");
//			if (rs != null) {
//				while (rs.next()) {
//					resultCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return resultCode;
		}
	}

	@SuppressWarnings("finally")
	public AudioQualityConfiguration getAudioQualityConfiguration( DeviceInformation deviceInformation) {
		AudioQualityConfiguration audioQualityConfiguration = null;
		AudioQuality auto = null, high = null, medium = null, low = null;
		String ssDomainName = "";
		String streamingURL = "";
		try {
			
			System.out.println("{CALL `AudioQualityConfiguration`('" + deviceInformation.getCcode() + "',"
					+ deviceInformation.getSource() + ",'" + deviceInformation.getOperatingSystem() + "','"
					+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
					+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "')}");
					
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("AudioQualityConfiguration");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryCode", deviceInformation.getCcode())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
			  		.addValue("inDevicePin", deviceInformation.getDevicePin());
			  
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);	  
				
				
				auto = new AudioQuality(resultMap.get("auto_abr").toString(),
						Integer.parseInt(resultMap.get("auto_tech_ref_id").toString()),
						resultMap.get("auto_codec").toString(),
						resultMap.get("auto_codecType").toString(),
						Integer.parseInt(resultMap.get("auto_bitRate").toString()),
						Integer.parseInt(resultMap.get("auto_noOfChannels").toString()),
						Integer.parseInt(resultMap.get("auto_samplingRate").toString()),
						resultMap.get("auto_fileExtention").toString());
				high = new AudioQuality(
						resultMap.get("high_abr").toString(),
						Integer.parseInt(resultMap.get("high_tech_ref_id").toString()),
						resultMap.get("high_codec").toString(),
						resultMap.get("high_codecType").toString(),
						Integer.parseInt(resultMap.get("high_bitRate").toString()),
						Integer.parseInt(resultMap.get("high_noOfChannels").toString()),
						Integer.parseInt(resultMap.get("high_samplingRate").toString()),
						resultMap.get("high_fileExtention").toString());
				medium = new AudioQuality(
						resultMap.get("medium_abr").toString(),
						Integer.parseInt(resultMap.get("medium_tech_ref_id").toString()),
						resultMap.get("medium_codec").toString(),
						resultMap.get("medium_codecType").toString(),
						Integer.parseInt(resultMap.get("medium_bitRate").toString()),
						Integer.parseInt(resultMap.get("medium_noOfChannels").toString()),
						Integer.parseInt(resultMap.get("medium_samplingRate").toString()),
						resultMap.get("medium_fileExtention").toString());
				low = new AudioQuality(resultMap.get("low_abr").toString(),
						Integer.parseInt(resultMap.get("low_tech_ref_id").toString()),
						resultMap.get("low_codec").toString(),
						resultMap.get("low_codecType").toString(),
						Integer.parseInt(resultMap.get("low_bitRate").toString()),
						Integer.parseInt(resultMap.get("low_noOfChannels").toString()),
						Integer.parseInt(resultMap.get("low_samplingRate").toString()),
						resultMap.get("low_fileExtention").toString());
				ssDomainName = resultMap.get("ssDomainName").toString();
				streamingURL = resultMap.get("streamingURL").toString();
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `AudioQualityConfiguration`('" + reqParam.getCountryCode() + "',"
//					+ reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					auto = new AudioQuality(rs.getString("auto_abr"), rs.getInt("auto_tech_ref_id"),
//							rs.getString("auto_codec"), rs.getString("auto_codecType"), rs.getInt("auto_bitRate"),
//							rs.getInt("auto_noOfChannels"), rs.getInt("auto_samplingRate"),
//							rs.getString("auto_fileExtention"));
//					high = new AudioQuality(rs.getString("high_abr"), rs.getInt("high_tech_ref_id"),
//							rs.getString("high_codec"), rs.getString("high_codecType"), rs.getInt("high_bitRate"),
//							rs.getInt("high_noOfChannels"), rs.getInt("high_samplingRate"),
//							rs.getString("high_fileExtention"));
//					medium = new AudioQuality(rs.getString("medium_abr"), rs.getInt("medium_tech_ref_id"),
//							rs.getString("medium_codec"), rs.getString("medium_codecType"), rs.getInt("medium_bitRate"),
//							rs.getInt("medium_noOfChannels"), rs.getInt("medium_samplingRate"),
//							rs.getString("medium_fileExtention"));
//					low = new AudioQuality(rs.getString("low_abr"), rs.getInt("low_tech_ref_id"),
//							rs.getString("low_codec"), rs.getString("low_codecType"), rs.getInt("low_bitRate"),
//							rs.getInt("low_noOfChannels"), rs.getInt("low_samplingRate"),
//							rs.getString("low_fileExtention"));
//					ssDomainName = rs.getString("ssDomainName");
//				}
//
//			}
//			mysql.close();
			audioQualityConfiguration = new AudioQualityConfiguration(auto, high, medium, low);
			audioQualityConfiguration.setSsDomainName(ssDomainName);
			audioQualityConfiguration.setStreamingURL(streamingURL);
		} catch (Exception e) {
		//	e.printStackTrace();
			audioQualityConfiguration = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return audioQualityConfiguration;
		}
	}

	@SuppressWarnings("unchecked")
	public UserInfoData signIn(SignIn reqParam , DeviceInformation  deviceInformation ) {
		UserInfoData userinfodata = null;
		
		System.out.println("  {CALL `SignIn`(" + deviceInformation.getCountryId() + "," + deviceInformation.getSource()
		+ ",'" + deviceInformation.getOperatingSystem() + "','" + deviceInformation.getOperatingSystemVersion() + "','"
		+ deviceInformation.getDeviceModel() + "','" + deviceInformation.getDeviceId() + "','" +1
		+ "'," + deviceInformation.getDevicePin() + "," + deviceInformation.getDeviceId() + ",'"
		+ reqParam.getUserName().replaceAll("'", "''") + "','"+ AESEncriptionDecription.decrypt(reqParam.getMsisdn()) + "','"
		+ reqParam.getEmailAddress().replaceAll("'", "''") + "','"
		+ reqParam.getPassword().replaceAll("'", "''") + "'," + reqParam.getGenderId() + ",'"
		+ "')}");
		
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("SignIn_Billing");
			SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId())
					.addValue("inViaId", 1)
					.addValue("inDevPin", deviceInformation.getDevicePin())
					.addValue("inUserDeviceId", 0)
					.addValue("inUserName", reqParam.getUserName().replaceAll("'", "''"))
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inEmailAddress", reqParam.getEmailAddress())
					.addValue("inPassword", reqParam.getPassword())
					.addValue("inGenderId", reqParam.getGenderId())
					.addValue("inProfilePictureUrl", "");
			
			Map<String, Object> rs = jdbcCall.execute(inParams);
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList<Object>) rs.get("#result-set-1");	
			
			
			Map<String, Object> trailMap = (Map<String, Object>) ar.get(0);

			// Hit for Subscription trail pack for first time user

			if(Integer.parseInt(trailMap.get("code").toString()) == 0 && Integer.parseInt(trailMap.get("trailenabled").toString()) == 1
			&& deviceInformation.getSource() == 1) {
				Map resultMap = getPackageDetails(Integer.parseInt(trailMap.get("trailpackid").toString()));
	
				String strHitURL = "";
				String strPackType = resultMap.get("package_name").toString();// String.valueOf(reqParam.getPackType());
				String strService = "APP";
				if (resultMap.get("package_description").toString().equalsIgnoreCase("Trail")) {
				strService = "APPTRIAL";
				}
	
				JSONObject jsonParam = new JSONObject();
				JSONObject jsonResponse = new JSONObject();
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
				jsonParam.put("lang", "e");
				jsonParam.put("service", strService);
	
				if (deviceInformation.getSource() == 1) {
				jsonParam.put("actSource", "APP");
				} else
				jsonParam.put("actSource", "WAP");
	
				jsonParam.put("packType", strPackType);
				jsonParam.put("validity", resultMap.get("validity_in_days").toString());
	
				jsonParam.put("transId", getTimeMillisecond());
				jsonParam.put("clickID","0");
				jsonParam.put("vendorID", "0");
				strHitURL = getURLValue("api.subscription.sub");
	
				String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
				jsonService = new JSONArray(jsonresponse);
				jsonResponse = jsonService.getJSONObject(0);
	
				try {
				insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "SUB",
				jsonParam.toString(), strHitURL, jsonResponse.toString(),"");
				} catch (Exception ex) {
				System.out.println("Exception in Vodafone DatabaseProcedures - signIn " + ex.getMessage());
				}
			}

			//Get User Status			
			
			
			JSONObject jsonParam = new JSONObject();
			String jsonResponse = "";
			jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
		
			jsonResponse = UrlProcessor.getInstance().getResponse(getURLValue("api.userstatus"), jsonParam);
			
			jsonService = new JSONArray(jsonResponse);
			
			JSONObject jsonObject;
			jsonObject = jsonService.getJSONObject(0);

			try {
				insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "USERSTATUS",
						jsonParam.toString(), getURLValue("api.userstatus"), jsonResponse.toString(),"");
			} catch (Exception ex) {
				System.out.println("Exception in Vodafone DatabaseProcedures.signIn - " + ex.getMessage());
			}
			
			String pack= null;			
			try {
				pack = jsonObject.getString("pack").toString();
			} catch (Exception e) {
				pack = jsonObject.getString("packType").toString();
			}

			Map packMap = getSubscribedPackageDetails(jsonObject.getString("service").toString(),pack);
			
			for(int i =0; i< ar.size();i++)
			{
				
						  int iStatus = Integer.parseInt(jsonObject.getString("status").toString());

				          Map<String, Object> resultMap = (Map<String, Object>) ar.get(i);
				          
				          userinfodata = new UserInfoData((resultMap.get("user_id").toString()),
						  resultMap.get("user_promo_code").toString(), resultMap.get("user_name").toString(),
						  resultMap.get("mobile_number").toString(), resultMap.get("email_address").toString(),
						  Integer.parseInt(resultMap.get("gender_id").toString()),
						  resultMap.get("password").toString(), resultMap.get("image_url").toString(),
						  getTypeID(iStatus),
						  iStatus,
						  getActionID(iStatus),
//						  Integer.parseInt(resultMap.get("user_type_id").toString()),
//						  Integer.parseInt(resultMap.get("user_status").toString()),
//						  Integer.parseInt(resultMap.get("action_id").toString()),
						  Integer.parseInt(resultMap.get("registered").toString()),
						  Integer.parseInt(resultMap.get("mobile_number_verification").toString()),
						  resultMap.get("billing_mobile_number").toString(),
						  resultMap.get("dialing_code").toString(),
						  Integer.parseInt(resultMap.get("mobile_number_length").toString()),
						  (resultMap.get("titleMsg").toString()),
						  (resultMap.get("TrialMsg").toString()),
						  
						  new SubscribedPackage(Integer.parseInt(packMap.get("package_id").toString()),
						  packMap.get("package_description").toString(), packMap.get("price_info").toString(),
						  getDateFormat(jsonObject.getString("nextBillingDate").toString()),
						  getDateFormat(jsonObject.getString("subDate").toString()),
						  getDateFormat(jsonObject.getString("nextBillingDate").toString()),
						  resultMap.get("android_token").toString(),
						  resultMap.get("ios_token").toString())
						  
						  );		   
			}
			 

		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
			userinfodata = null;
			
		} finally {
			return userinfodata;
		}

	}
	@SuppressWarnings("finally")
	public int sendSMS(RequestParameter reqParam) {
		int responseCode = 320;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("SendSMS");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inMessage", reqParam.getMessage())
					.addValue("inSender", reqParam.getSender());
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
				
 
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	} 

	@SuppressWarnings("finally")
	public int getSubscriptionTrialStatus(int userId) {
		int responseCode = 0;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetSubscriptionTrialStatus");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inUserId", userId);					
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("status_code").toString());
								
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetSubscriptionTrialStatus`(" + userId + ")}");
//			while (rs.next()) {
//				responseCode = rs.getInt("status_code");
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	@SuppressWarnings("finally")
	public String GetSubscriptionTrialStatus(int userId) {
		String outResponce = "";
		int responseCode = 0;
		int userStatus = 99;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetSubscriptionTrialStatus1");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inUserId", userId);					
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("status_code").toString());
				userStatus =Integer.parseInt(resultMap.get("user_status").toString());
 
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			outResponce = String.valueOf(responseCode) + "#" + String.valueOf(userStatus);
			return outResponce;
		}
	}
	@SuppressWarnings("finally")
	public UserInfoData getUserInfo(SignIn reqParam , DeviceInformation  deviceInformation) {
		UserInfoData userinfodata = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserInfo_Billing");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inUserName", reqParam.getUserName())
					.addValue("inEmail", reqParam.getEmailAddress())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inGender", reqParam.getGenderId())
					.addValue("inUpdateType", reqParam.getEventType())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inAppVer", deviceInformation.getApplicationVersion());
				
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				JSONObject jsonParam = new JSONObject();
				String jsonResponse = "";
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
			
				jsonResponse = UrlProcessor.getInstance().getResponse(getURLValue("api.userstatus"), jsonParam);
				
				jsonService = new JSONArray(jsonResponse);
				
				JSONObject jsonObject;
				jsonObject = jsonService.getJSONObject(0);

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "USERSTATUS",
							jsonParam.toString(), getURLValue("api.userstatus"), jsonResponse.toString(),"");
				} catch (Exception e) {
					System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
					e.printStackTrace();
				}
				String pack= null;			
				try {
					pack = jsonObject.getString("pack").toString();
				} catch (Exception e) {
					pack = jsonObject.getString("packType").toString();
				}

				Map packMap = getSubscribedPackageDetails(jsonObject.getString("service").toString(),pack);
				

				  for(int i =0; i< ar.size();i++)
					{
					  int iStatus = Integer.parseInt(jsonObject.getString("status").toString());

						Map resultMap = (Map) ar.get(i);
						
					//	System.out.println("user info:-   ....  "+ resultMap.toString());
						userinfodata = new UserInfoData((resultMap.get("user_id").toString()),
								resultMap.get("user_promo_code").toString(),
								resultMap.get("user_name").toString(),
								resultMap.get("mobile_number").toString(),
								resultMap.get("email_address").toString(),
								Integer.parseInt(resultMap.get("gender_id").toString()),
								resultMap.get("password").toString(),
								resultMap.get("image_url").toString(),
								getTypeID(iStatus),
								iStatus,
								getActionID(iStatus),
								Integer.parseInt(resultMap.get("registered").toString()),
								Integer.parseInt(resultMap.get("mobile_number_verification").toString()),
								resultMap.get("billing_mobile_number").toString(), 
								resultMap.get("dialing_code").toString(),
								Integer.parseInt(resultMap.get("mobile_number_length").toString()),
								(resultMap.get("titleMsg").toString()),
								  (resultMap.get("TrialMsg").toString()),
								  
								new SubscribedPackage(Integer.parseInt(packMap.get("package_id").toString()),
										packMap.get("package_description").toString(),
										packMap.get("price_info").toString(),
										getDateFormat(jsonObject.getString("nextBillingDate").toString()),
										getDateFormat(jsonObject.getString("subDate").toString()),
										getDateFormat(jsonObject.getString("nextBillingDate").toString()),
										resultMap.get("android_token").toString(),
										resultMap.get("ios_token").toString())
										
								);	
					}	
			  
				 
			return userinfodata;
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			//e.printStackTrace();

			return null;
		}
	}

	@SuppressWarnings("finally")
	public UserInfoData setUserInfo(UserParameters reqParam, DeviceInformation deviceInformation) {
		UserInfoData userinfodata = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserInfo_Billing");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inUserName", reqParam.getUserName())
					.addValue("inEmail", reqParam.getEmailAddress())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inGender", reqParam.getGenderId())
					.addValue("inUpdateType", reqParam.getEventType())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inAppVer", deviceInformation.getApplicationVersion());
//			  
			  
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				JSONObject jsonParam = new JSONObject();
				String jsonResponse = "";
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
			
				jsonResponse = UrlProcessor.getInstance().getResponse(getURLValue("api.userstatus"), jsonParam);
				
				jsonService = new JSONArray(jsonResponse);
				
				JSONObject jsonObject;
				jsonObject = jsonService.getJSONObject(0);

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "USERSTATUS",
							jsonParam.toString(), getURLValue("api.userstatus"), jsonResponse.toString(),"");
				} catch (Exception ex) {
					System.out.println("Exception in Vodafone DatabaseProcedures.signIn - " + ex.getMessage());
				}
				String pack= null;			
				try {
					pack = jsonObject.getString("pack").toString();
				} catch (Exception e) {
					pack = jsonObject.getString("packType").toString();
				}

				Map packMap = getSubscribedPackageDetails(jsonObject.getString("service").toString(),pack);

				for(int i =0; i< ar.size();i++)
				{
					int iStatus = Integer.parseInt(jsonObject.getString("status").toString());

					Map resultMap = (Map) ar.get(i);
					userinfodata = new UserInfoData((resultMap.get("user_id").toString()),
							resultMap.get("user_promo_code").toString(),
							resultMap.get("user_name").toString(),
							resultMap.get("mobile_number").toString(),
							resultMap.get("email_address").toString(),
							Integer.parseInt(resultMap.get("gender_id").toString()),
							resultMap.get("password").toString(),
							resultMap.get("image_url").toString(),
							getTypeID(iStatus),
							iStatus,
							getActionID(iStatus),
							Integer.parseInt(resultMap.get("registered").toString()),
							Integer.parseInt(resultMap.get("mobile_number_verification").toString()),
							resultMap.get("billing_mobile_number").toString(), 
							resultMap.get("dialing_code").toString(),
							Integer.parseInt(resultMap.get("mobile_number_length").toString()),
							(resultMap.get("titleMsg").toString()),
							  (resultMap.get("TrialMsg").toString()),
							  
							new SubscribedPackage(Integer.parseInt(packMap.get("package_id").toString()),
									packMap.get("package_description").toString(),
									packMap.get("price_info").toString(),
									getDateFormat(jsonObject.getString("nextBillingDate").toString()),
									getDateFormat(jsonObject.getString("subDate").toString()),
									getDateFormat(jsonObject.getString("nextBillingDate").toString()),
									resultMap.get("android_token").toString(),
									resultMap.get("ios_token").toString())
							);	
				}	
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserInfo`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getUserId() + ",'" + reqParam.getUserName() + "','" + reqParam.getEmailAddress() + "','"
//					+ reqParam.getMsisdn() + "'," + reqParam.getGenderId() + "," + reqParam.getEventType() + ",'"
//					+ reqParam.getOperatingSystem() + "','" + reqParam.getApplicationVersion() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					userinfodata = new UserInfoData(rs.getInt("user_id"), rs.getString("user_promo_code"),
//							rs.getString("user_name"), rs.getString("mobile_number"), rs.getString("email_address"),
//							rs.getInt("gender_id"), rs.getString("password"), rs.getString("image_url"),
//							rs.getInt("user_type_id"), rs.getInt("user_status"), rs.getInt("action_id"),
//							rs.getInt("registered"), rs.getInt("mobile_number_verification"),
//							rs.getString("billing_mobile_number"), rs.getString("dialing_code"),
//							rs.getInt("mobile_number_length"),
//							new SubscribedPackage(rs.getInt("package_id"), rs.getString("package_name"),
//									rs.getString("package_price"), rs.getString("package_validity_period"),
//									rs.getString("package_start_date"), rs.getString("package_end_date"),
//									rs.getString("android_token"), rs.getString("ios_token"),
//									rs.getBoolean("optScreenVisibility")));
//					// userinfodata.setUserSignUpVia(getUserSignUpVia(rs.getInt("user_id")));
//					// userinfodata.setUserSubscriptionStatus(getUserSubscriptionStatus(rs.getInt("user_id"),
//					// reqParam.getEventId()));
//
//				}
//			}
//			mysql.close();
			 
		} catch (Exception e) {
			userinfodata = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return userinfodata;
		}

	}
	@SuppressWarnings("finally")
	public List<TrackData> trackSearch(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Search");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),
		    			 false,5)
		    			  
		    			  );	
		    				    			  
		    	  });	
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql
//					.prepareCall("{CALL `Search`(1," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword()
//							+ "'," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
			// System.out.println("Search--"+lst.toString());
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> albumSearch(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Search");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);									      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
			   
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistSearch(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Search");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql
//					.prepareCall("{CALL `Search`(3," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword()
//							+ "'," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<FeaturedPlaylistBean> playlistSearch(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<FeaturedPlaylistBean> lst = new ArrayList<FeaturedPlaylistBean>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Search");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",4)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new FeaturedPlaylistBean(Integer.parseInt(resultMap.get("playlist_id").toString()), 
		    			  resultMap.get("playlist_name").toString(),
		    			  resultMap.get("playlist_desc").toString(),  
		    			  resultMap.get("image_url").toString(),
		    			  Integer.parseInt(resultMap.get("track_count").toString())
		    			  ));	
		    				    			  
		    	  });
				
//			MySQL mysql = new MySQL();
//			// ResultSet rs = mysql.prepareCall("{CALL `GetFeaturedPlaylistMetaData`(1," +
//			// reqParam.getCountryId() + "," + reqParam.getSourceId() + "," +
//			// reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','" +
//			// reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() +
//			// "','" + reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," +
//			// reqParam.getPlaylistId() + "," + reqParam.getAudioTechRefId() + "," +
//			// reqParam.getImageTechRefId() + "," + reqParam.getStartLimit() + "," +
//			// reqParam.getEndLimit() + ")}");
//			ResultSet rs = mysql
//					.prepareCall("{CALL `Search`(4," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword()
//							+ "'," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new FeaturedPlaylistBean(rs.getInt("playlist_id"), rs.getString("playlist_name"),
//							rs.getString("playlist_desc"), rs.getString("image_url"), rs.getInt("track_count")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> albumFilter(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FilterByLetter");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });			  
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FilterByLetter`(1," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword() + "',"
//							+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistFilter(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FilterByLetter");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    			
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });			  
			  
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FilterByLetter`(2," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword() + "',"
//							+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<FeaturedPlaylistBean> playlistFilter(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<FeaturedPlaylistBean> lst = new ArrayList<FeaturedPlaylistBean>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FilterByLetter");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new FeaturedPlaylistBean(Integer.parseInt(resultMap.get("playlist_id").toString()), 
		    			  resultMap.get("playlist_name").toString(),
		    			  resultMap.get("playlist_desc").toString(),	
		    			  resultMap.get("image_url").toString(),
		    			  Integer.parseInt(resultMap.get("track_count").toString())		    			 
		    			  ));	
		    				    			  
		    	  });		
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<GenreBean> genreFilter(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<GenreBean> lst = new ArrayList<GenreBean>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FilterByLetter");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",4)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new GenreBean(Integer.parseInt(resultMap.get("genre_id").toString()), 
		    			  resultMap.get("genre_name").toString(),		    			
		    			  resultMap.get("image_url").toString()		    			  	    			 
		    			  ));	
		    				    			  
		    	  });		
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FilterByLetter`(4," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword() + "',"
//							+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new GenreBean(rs.getInt("genre_id"), rs.getString("genre_name"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<TrackData> trackFilter(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {

			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FilterByLetter");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",5)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSearch", reqParam.getSearchKeyword())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),
		    			 false,5)
		    			  
		    			  );		    				    			  
		    	  });				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FilterByLetter`(5," + reqParam.getCountryId() + ",'" + reqParam.getSearchKeyword() + "',"
//							+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<HomeItemData> getHomeItem(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<HomeItemData> lst = new ArrayList<HomeItemData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetHomeItem");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new HomeItemData(Integer.parseInt(resultMap.get("id").toString()),
			    			  Integer.parseInt(resultMap.get("rid").toString()),
			    			  resultMap.get("name").toString(),
			    			  resultMap.get("type").toString(),
			    			  resultMap.get("desc").toString(),			    			
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetHomeItem`(" + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit() + ","
//					+ reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new HomeItemData(rs.getInt("id"), rs.getInt("rid"), rs.getString("name"),
//							rs.getString("type"), rs.getString("desc"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<TrackData> trackInfo(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetTrackInfo");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inResourceCode", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId());
					
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(
			    			  new TrackData(resultMap.get("resource_code").toString(), 
			    					  resultMap.get("ivrMMNumber").toString(),
			    					  resultMap.get("resource_title").toString(),
			    					  Integer.parseInt(resultMap.get("album_id").toString()),
			    					  resultMap.get("album_title").toString(),
			    					  Integer.parseInt(resultMap.get("artist_id").toString()),
			    					  resultMap.get("artist_name").toString(),
			    					  Integer.parseInt(resultMap.get("genre_id").toString()),
			    					  resultMap.get("genre_name").toString(),
			    					  Long.parseLong(resultMap.get("play_count").toString()),
			    					  Long.parseLong(resultMap.get("favourite_count").toString()),
			    					  Long.parseLong(resultMap.get("share_count").toString()),
			    					  Long.parseLong(resultMap.get("size").toString()),
			    					  Long.parseLong(resultMap.get("duration").toString()),
			    					  "-",
			    					//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    					  resultMap.get("image_url").toString(),
			    					  resultMap.get("video_id").toString(),
			    					 false,5));			    				    			  
			    	  });			
					
			
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql
//					.prepareCall("{CALL `GetTrackInfo`(" + reqParam.getCountryId() + ",'" + reqParam.getTrackCode()
//							+ "'," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<karaokeTrackData> karaokeInfo(RequestParameter reqParam) {
		List<karaokeTrackData> lst = new ArrayList<karaokeTrackData>();
		try {
			
//			MySQL1 mysql = new MySQL1();
//			ResultSet rs1 = mysql.prepareCall("{CALL `GetKaraokeInfo`(1,'" + reqParam.getUserId() + "','"
//					+ reqParam.getTrackCode() + "','" + reqParam.getEventType() + "','" + reqParam.getTabId() + "',"
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetKaraokeInfo");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("in_Flag",1)
					.addValue("in_user_id", reqParam.getUserId())
					.addValue("in_resource_code", reqParam.getTrackCode())
					.addValue("inItemType", reqParam.getEventType())
					.addValue("inTabId", reqParam.getTabId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				//  System.out.println("GetKaraokeInfo in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				for(int i =0;i<ar.size();i++) 
				{
				  Map resultMap = (Map) ar.get(i);
				  lst.add(new karaokeTrackData(Integer.parseInt(resultMap.get("karaoke_id").toString()), 
		    			  resultMap.get("resource_code").toString(),
		    			  resultMap.get("title").toString(),
		    			  resultMap.get("artist_id").toString(),
		    			  resultMap.get("artist_title").toString(),
		    			  resultMap.get("fistUserId").toString(),
		    			  resultMap.get("secondUserId").toString(),
		    			  resultMap.get("singerType").toString(),
		    			  resultMap.get("recording_mode").toString(),
		    			  resultMap.get("record_type").toString(),
		    			  getBooleanValue(resultMap.get("isJoin").toString()),
		    			  resultMap.get("streamingUrl").toString(),		    			  
		    			  resultMap.get("imageUrl").toString(),
		    			  resultMap.get("score").toString(),
		    			  getBooleanValue(resultMap.get("isCrbtAvailable").toString()),
		    			  getBooleanValue(resultMap.get("isKaraokeAvailable").toString()),
		    			  resultMap.get("userName").toString(),
		    			  Integer.parseInt(resultMap.get("firstKaraokeId").toString()),
		    			  getBooleanValue(resultMap.get("isFollowed").toString()),
		    			  getBooleanValue(resultMap.get("isReadyToPlay").toString()),
		    			  resultMap.get("lang_karaoke_available").toString().split("#")));
		    			  			    				    			  
		    	  }
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetKaraokeInfo`(1,'" + reqParam.getUserId() + "','"
//					+ reqParam.getTrackCode() + "','" + reqParam.getEventType() + "','" + reqParam.getTabId() + "',"
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new karaokeTrackData(rs.getInt("karaoke_id"), rs.getString("resource_code"),
//							rs.getString("title"), rs.getString("artist_id"), rs.getString("artist_title"),
//							rs.getString("fistUserId"), rs.getString("secondUserId"), rs.getString("singerType"),
//							rs.getString("recording_mode"), rs.getString("record_type"), rs.getBoolean("isJoin"),
//							rs.getString("streamingUrl"), rs.getString("imageUrl"), rs.getString("score"),
//							rs.getBoolean("isCrbtAvailable"), rs.getBoolean("isKaraokeAvailable"),
//							rs.getString("userName"), rs.getInt("firstKaraokeId"), rs.getBoolean("isFollowed"),
//							rs.getBoolean("isReadyToPlay"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public karaokeTrackData getkaraokeSongInfo(RequestParameter reqParam) {
		karaokeTrackData obj = null;
		try {
////			
//			MySQL1 mysql = new MySQL1();
//			ResultSet rs1 = mysql.prepareCall("{CALL `GetKaraokeInfo`(1,'" + reqParam.getUserId() + "','"
//					+ reqParam.getTrackCode() + "','" + reqParam.getEventType() + "','" + reqParam.getTabId() + "',"
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//
//			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetKaraokeInfo");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("in_Flag",1)
					.addValue("in_user_id", reqParam.getUserId())
					.addValue("in_resource_code", reqParam.getTrackCode())
					.addValue("inItemType", reqParam.getEventType())
					.addValue("inTabId", reqParam.getTabId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				for(int i =0;i<ar.size();i++) 
				{
				  Map resultMap = (Map) ar.get(i);
		    	  obj = new karaokeTrackData(Integer.parseInt(resultMap.get("karaoke_id").toString()), 
		    			  resultMap.get("resource_code").toString(),
		    			  resultMap.get("title").toString(),
		    			  resultMap.get("artist_id").toString(),
		    			  resultMap.get("artist_title").toString(),
		    			  resultMap.get("fistUserId").toString(),
		    			  resultMap.get("secondUserId").toString(),
		    			  resultMap.get("singerType").toString(),
		    			  resultMap.get("recording_mode").toString(),
		    			  resultMap.get("record_type").toString(),
		    			  getBooleanValue(resultMap.get("isJoin").toString()),
		    			  resultMap.get("streamingUrl").toString(),		    			  
		    			  resultMap.get("imageUrl").toString(),
		    			  resultMap.get("score").toString(),
		    			  getBooleanValue(resultMap.get("isCrbtAvailable").toString()),
		    			  getBooleanValue(resultMap.get("isKaraokeAvailable").toString()),
		    			  resultMap.get("userName").toString(),
		    			  Integer.parseInt(resultMap.get("firstKaraokeId").toString()),
		    			  getBooleanValue(resultMap.get("isFollowed").toString()),
		    			  getBooleanValue(resultMap.get("isReadyToPlay").toString()),
		    			  resultMap.get("lang_karaoke_available").toString().split("#"));
		    			  			    				    			  
		    	  }

//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetKaraokeInfo`(1,'" + reqParam.getUserId() + "','"
//					+ reqParam.getTrackCode() + "','" + reqParam.getEventType() + "','" + reqParam.getTabId() + "',"
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					obj = new karaokeTrackData(rs.getInt("karaoke_id"), rs.getString("resource_code"),
//							rs.getString("title"), rs.getString("artist_id"), rs.getString("artist_title"),
//							rs.getString("fistUserId"), rs.getString("secondUserId"), rs.getString("singerType"),
//							rs.getString("recording_mode"), rs.getString("record_type"), rs.getBoolean("isJoin"),
//							rs.getString("streamingUrl"), rs.getString("imageUrl"), rs.getString("score"),
//							rs.getBoolean("isCrbtAvailable"), rs.getBoolean("isKaraokeAvailable"),
//							rs.getString("userName"), rs.getInt("firstKaraokeId"), rs.getBoolean("isFollowed"),
//							rs.getBoolean("isReadyToPlay"), rs.getString("lang_karaoke_available").split("#"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			//e.printStackTrace();
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return obj;
		}
	}

	public Map<Integer, List<KaraokeCommentData>> getKaraokeCommentList(RequestParameter reqParam) {
		Map<Integer, List<KaraokeCommentData>> map = new HashMap<Integer, List<KaraokeCommentData>>();
		List<KaraokeCommentData> list = new ArrayList<KaraokeCommentData>();
		int count = 0;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("KaraokeComment");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("IN_ID", reqParam.getId())
					.addValue("IN_USER_ID", reqParam.getUserId())
					.addValue("IN_KARAOKE_ID", reqParam.getKaraokeId())
					.addValue("IN_COMMENT", reqParam.getMessage())
					.addValue("IN_COMMENT_DATETIME", reqParam.getDatetime())					
					.addValue("IN_RECORDING_TYPE", reqParam.getRecordingMode())
					.addValue("IN_EVENT", 4)					
					.addValue("IN_STARTLIMIT", reqParam.getStartLimit())
					.addValue("IN_ENDLIMIT", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					for(int i =0; i< ar.size();i++)
					{
					  Map resultMap = (Map) ar.get(i);
			    	  count = Integer.parseInt(resultMap.get("totalCount").toString());
			    	  list.add(new KaraokeCommentData(Integer.parseInt(resultMap.get("id").toString()), 
	    			  Integer.parseInt(resultMap.get("karouke_id").toString()),
	    			  Integer.parseInt(resultMap.get("user_id").toString()),
	    			  resultMap.get("userComment").toString(),
	    			  resultMap.get("commentDateTime").toString(),
	    			  resultMap.get("commentUpdateDateTime").toString(),
	    			  resultMap.get("isUpdated").toString(),
	    			  resultMap.get("user_name").toString(),			    			  
	    			  resultMap.get("user_image_url").toString()
	    			  ));
			    				    			  
					}
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `KaraokeComment`(" + reqParam.getId() + "," + reqParam.getUserId()
//					+ "," + reqParam.getKaraokeId() + ",'" + reqParam.getMessage() + "','" + reqParam.getDatetime()
//					+ "'," + reqParam.getRecordingMode() + ",4," + reqParam.getStartLimit() + ","
//					+ reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					count = rs.getInt("totalCount");
//					list.add(new KaraokeCommentData(rs.getInt("id"), rs.getInt("karouke_id"), rs.getInt("user_id"),
//							rs.getString("userComment"), rs.getString("commentDateTime"),
//							rs.getString("commentUpdateDateTime"), rs.getString("isUpdated"), rs.getString("user_name"),
//							rs.getString("user_image_url")));
//				}
//			}
			map.put(count, list);
			//mysql.close();
			return map;
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();

			return map;
		}
	}

	public int karaokeComment(RequestParameter reqParam) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("KaraokeComment");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()					
				.addValue("IN_ID", reqParam.getId())
				.addValue("IN_USER_ID", reqParam.getUserId())
				.addValue("IN_KARAOKE_ID", reqParam.getKaraokeId())
				.addValue("IN_COMMENT", reqParam.getMessage())
				.addValue("IN_COMMENT_DATETIME", reqParam.getDatetime())
				.addValue("IN_RECORDING_TYPE", reqParam.getRecordingMode())
				.addValue("IN_EVENT", reqParam.getEventType())
				.addValue("IN_STARTLIMIT", reqParam.getStartLimit())
				.addValue("IN_ENDLIMIT", reqParam.getEndLimit());
					
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
		
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `KaraokeComment`(" + reqParam.getId() + "," + reqParam.getUserId()
//					+ "," + reqParam.getKaraokeId() + ",'" + reqParam.getMessage() + "','" + reqParam.getDatetime()
//					+ "'," + reqParam.getRecordingMode() + "," + reqParam.getEventType() + ","
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			while (rs.next()) {
//				responseCode = rs.getInt("code");
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public boolean FOLLOWKARAOKEARTIST(RequestParameter reqParam) {
		boolean following = false;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FOLLOWKARAOKEARTIST");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("IN_FLAG", reqParam.getEventType())
					.addValue("in_USER_ID", reqParam.getUserId())
					.addValue("in_FOLLOW_USER_ID", reqParam.getArtistId());

			  Map<String, Object> rs = jdbcCall.execute(inParams);
 				///////      System.out.println("sign in response :: "+ rs);					      
 				ArrayList<Object> ar = new ArrayList<Object>();
 				ar = (ArrayList) rs.get("#result-set-1");
 				Map resultMap = (Map) ar.get(0);				    
 				following =getBooleanValue(resultMap.get("code").toString());
			 				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `FOLLOWKARAOKEARTIST`(" + reqParam.getEventType() + ","
//					+ reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					following = rs.getBoolean("follow");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return following;
		}
	}

	public List<FollowKaraokeArtistData> FOLLOWKARAOKEARTISTLISTING(RequestParameter reqParam) {
		List<FollowKaraokeArtistData> list = new ArrayList<FollowKaraokeArtistData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FOLLOWKARAOKEARTIST");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("IN_FLAG",reqParam.getEventType())
					.addValue("in_USER_ID", reqParam.getUserId())
					.addValue("in_FOLLOW_USER_ID", reqParam.getArtistId());
					
			Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			//Map resultMap1 = (Map) ar.get(0);
			      
			ar.forEach(item->{
	    	  Map resultMap = (Map) item;
	    	  list.add(new FollowKaraokeArtistData(
	    			  Integer.parseInt(resultMap.get("user_id").toString()), 
	    			  resultMap.get("userName").toString(),
	    			  resultMap.get("user_image_url").toString(),
	    			  getBooleanValue(resultMap.get("isArtistFollowed").toString())	    			  
	    			  ));	    				    			  
	    	  });
	 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return list;
		}
	}

	@SuppressWarnings("finally")
	public CountryInfoData getCountryInfo(  DeviceInformation  deviceInformation) {
		CountryInfoData data = null;
		try {
			
			
			
			System.out.println("{call `CountryInformation`('"
			+ deviceInformation.getLang() + "','" + deviceInformation.getCcode() + "'," + deviceInformation.getSource()
			+ ",'" + deviceInformation.getApplicationVersion() + "','" + deviceInformation.getOperatingSystem() + "','"
			+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
			+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "', 1)}");
			
			  SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CountryInformation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					    .addValue("inLang", deviceInformation.getLang())
					    .addValue("inCountryCode",deviceInformation.getCcode())
					    .addValue("inSourceId",  deviceInformation.getSource())	
						.addValue("inApplicationVersion", deviceInformation.getApplicationVersion())
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())
						.addValue("inSplashScreenType",1);						
			 
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
								      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
			
				Map resultMap = (Map) ar.get(0);
								
			
				data = new CountryInfoData();
				data.setCountryid(Integer.parseInt(resultMap.get("country_id").toString()));
				data.setCountryname(resultMap.get("country_name").toString());
				data.setDialingcode(resultMap.get("dialing_code").toString());
				data.setMobnolength(resultMap.get("mob_no_len").toString());
				data.setCode2digit(resultMap.get("code_2digit").toString());
				data.setCode3digit(resultMap.get("code_3digit").toString());
				data.setLanguageCode(resultMap.get("language_code").toString());
				data.setUserdevid(Integer.parseInt(resultMap.get("user_device_id").toString()));
				data.setAudioadversion(Integer.parseInt(resultMap.get("audio_ad_version").toString()));
				data.setAudioadurl(resultMap.get("audio ad_url").toString());
				data.setSplashversion(Integer.parseInt(resultMap.get("splash_version").toString()));
				data.setSplashurl(resultMap.get("splash_url").toString());
				data.setLogourl(resultMap.get("logo_url").toString());
				data.setMlogourl(resultMap.get("mlogo_url").toString());
				data.setNewLogoUrl(resultMap.get("new_logo_url").toString());
				data.setIosThemeColour(resultMap.get("ios_theme_colour").toString());
				data.setIsDataCacheEnable(getBooleanValue(resultMap.get("data_cache_enable").toString()));
				data.setSubscriptionAvailable(Integer.parseInt(resultMap.get("subscription_available").toString()));
				data.setDownloadAvailable(Integer.parseInt(resultMap.get("download_available").toString()));
				data.setIsDataCacheEnable(getBooleanValue(resultMap.get("data_cache_enable").toString()));
				data.setCrbtAvailable(Integer.parseInt(resultMap.get("crbt_available").toString()));
				data.setSubscriptionUrl(resultMap.get("subscription_url").toString());
				data.setSubLinkOpenAction(Integer.parseInt(resultMap.get("sub_link_open_action").toString()));
				data.setShareUrl(resultMap.get("share_url").toString());
				data.setMsisdnHeaderUrl(resultMap.get("msisdn_header_url").toString());

				data.setSongOptionsSequence(resultMap.get("song_options_sequence").toString());
				data.setSongPreviewDuration(Integer.parseInt(resultMap.get("song_preview_duration").toString()));

				data.setCli(resultMap.get("cli").toString());
				data.setOtpLength(Integer.parseInt(resultMap.get("otp_length").toString()));
				data.setOtpPosition(Integer.parseInt(resultMap.get("otpPosition").toString()));
				data.setShowStickyIcon(getBooleanValue(resultMap.get("show_sticky_icon").toString()));
				data.setShowLanguageChangePopup(getBooleanValue(resultMap.get("show_language_change_popup").toString()));

				data.setIsSocialScreenEnable(getBooleanValue(resultMap.get("social_screen_enable").toString()));

				data.setLoginRequiredAll(getBooleanValue(resultMap.get("login_required_all").toString()));
				data.setLoginRequiredShare(getBooleanValue(resultMap.get("login_required_share").toString()));
				data.setLoginRequiredCrbt(getBooleanValue(resultMap.get("login_required_crbt").toString()));
				data.setLoginRequiredPlayAudio(getBooleanValue(resultMap.get("login_required_play_audio").toString()));
				data.setLoginRequiredPlayVideo(getBooleanValue(resultMap.get("login_required_play_video").toString()));
				data.setLoginRequiredDownload(getBooleanValue(resultMap.get("login_required_download").toString()));
				data.setLoginRequiredOffline(getBooleanValue(resultMap.get("login_required_offline").toString()));
				data.setLoginRequiredFavourite(getBooleanValue(resultMap.get("login_required_favourite").toString()));

				data.setSubscriptionRequiredShare(getBooleanValue(resultMap.get("subscription_required_share").toString()));
				data.setSubscriptionRequiredCrbt(getBooleanValue(resultMap.get("subscription_required_crbt").toString()));
				data.setSubscriptionRequiredPlayAudio(getBooleanValue(resultMap.get("subscription_required_play_audio").toString()));
				data.setSubscriptionRequiredPlayVideo(getBooleanValue(resultMap.get("subscription_required_play_video").toString()));
				data.setSubscriptionRequiredDownload(getBooleanValue(resultMap.get("subscription_required_download").toString()));
				data.setSubscriptionRequiredOffline(getBooleanValue(resultMap.get("subscription_required_offline").toString()));
				data.setSubscriptionRequiredFavourite(getBooleanValue(resultMap.get("subscription_required_favourite").toString()));

				data.setLikeSongPopup(Integer.parseInt(resultMap.get("like_song_popup").toString()));
				data.setOfflineDownloadPopup(Integer.parseInt(resultMap.get("offline_download_popup").toString()));
				data.setLeftMenuButtonTitle(resultMap.get("leftMenuButtonTitle").toString());
				data.setLeftMenuButtonTitle_SW(resultMap.get("leftMenuButtonTitle_SW").toString());
				data.setUploadMusicVisibility(getBooleanValue(resultMap.get("uploadMusicVisibility").toString()));
				data.setArtistURL(resultMap.get("artistURL").toString());
			//	data.setMsisdnHeaderFinalUrl(resultMap.get("msisdnHeaderFinalUrl").toString());
			//	data.setOptScreenVisibility(getBooleanValue(resultMap.get("voptScreenVisibility").toString()));
				data.setIsPromoCodeScreen(getBooleanValue(resultMap.get("isPromoCodeScreen").toString()));
			//	data.setIsKaraokeAvailable(getBooleanValue(resultMap.get("isKaraokeAvailable").toString()));
				data.setFaqURL(resultMap.get("faq_url").toString());
				data.setPolicyURL(resultMap.get("policy_url").toString());
				data.setTermsURL(resultMap.get("terms_url").toString());
				data.setSubCallbackURL(resultMap.get("sub_callback_url").toString());				
				data.setLoginURL(resultMap.get("login_url").toString());
				
				data.setDefaultSignIn( getBooleanValue( resultMap.get("default_signin").toString()));
				
				
				
				
		} catch (Exception e) {
			
			data = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return data;

	}

	@SuppressWarnings("finally")
	public Visibility getAppFeaturesVisibility( DeviceInformation  deviceInformation ) {
		
		Visibility visibility = null;
		VisibilityValue visibilityValue[] = new VisibilityValue[3];
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FeatureVisibility");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryCode",deviceInformation.getCcode())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem());
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
			//  System.out.println("getAppFeaturesVisibility in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap = (Map) ar.get(0);				
				
				
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;	 
			    	  
			    int	  userTypeId = Integer.parseInt(resultMap.get("user_type_id").toString());
					 
						if (userTypeId == 1) {
						//	 System.out.println("userTypeId ::"+ userTypeId);
							visibilityValue[0] = new VisibilityValue(Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline").toString()),
									Integer.parseInt(resultMap.get("download").toString()));
						}
						if (userTypeId == 2) {
						//	 System.out.println("userTypeId 2222::"+ userTypeId);
							visibilityValue[1] = new VisibilityValue(
									Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline").toString()),
									Integer.parseInt(resultMap.get("download").toString()));
						}
						if (userTypeId == 3) {
							// System.out.println("userTypeId  3333 ::"+ userTypeId);
							visibilityValue[2] = new VisibilityValue(
									Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline").toString()),
									Integer.parseInt(resultMap.get("download").toString()));
						}
						
			    	  });
				
				
			
				visibility = new Visibility(visibilityValue[0], visibilityValue[1], visibilityValue[2]);

			//	System.out.println("visibility :::::::::::"+ visibility.toString());
		} catch (Exception e) {
			visibility = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return visibility;
		}

	}

	@SuppressWarnings("finally")
	public Configuration getApplicationConfiguration( DeviceInformation  deviceInformation ) {
		 
		Configuration configuration = null;
		ConfigurationValue configVal[] = new ConfigurationValue[3];
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FeatureConfiguration");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryCode",deviceInformation.getCcode())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem());
			
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap = (Map) ar.get(0);		
				
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;	 
			    	  
			    int	  userTypeId = Integer.parseInt(resultMap.get("user_type_id").toString());
						
						if (userTypeId == 1) {
							configVal[0] = new ConfigurationValue(
									Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline_limit").toString()),
									Integer.parseInt(resultMap.get("offline_daily").toString()),
									Integer.parseInt(resultMap.get("offline_total").toString()),
									Integer.parseInt(resultMap.get("download").toString()),
									Integer.parseInt(resultMap.get("stream_interrupt").toString()),
									Integer.parseInt(resultMap.get("stream_limit").toString()),
									Integer.parseInt(resultMap.get("stream_preview").toString()),
									Integer.parseInt(resultMap.get("stream_daily").toString()),
									Integer.parseInt(resultMap.get("stream_total").toString()),
									Integer.parseInt(resultMap.get("stream_track").toString()));
						}
						if (userTypeId == 2) {
							configVal[1] = new ConfigurationValue(
									Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline_limit").toString()),
									Integer.parseInt(resultMap.get("offline_daily").toString()),
									Integer.parseInt(resultMap.get("offline_total").toString()),
									Integer.parseInt(resultMap.get("download").toString()),
									Integer.parseInt(resultMap.get("stream_interrupt").toString()),
									Integer.parseInt(resultMap.get("stream_limit").toString()),
									Integer.parseInt(resultMap.get("stream_preview").toString()),
									Integer.parseInt(resultMap.get("stream_daily").toString()),
									Integer.parseInt(resultMap.get("stream_total").toString()),
									Integer.parseInt(resultMap.get("stream_track").toString()));
						}
						if (userTypeId == 3) {
							configVal[2] = new ConfigurationValue(
									Integer.parseInt(resultMap.get("banner_ads").toString()),
									Integer.parseInt(resultMap.get("audio_ads").toString()),
									Integer.parseInt(resultMap.get("favourite").toString()),
									Integer.parseInt(resultMap.get("dedication").toString()),
									Integer.parseInt(resultMap.get("share").toString()),
									Integer.parseInt(resultMap.get("crbt").toString()),
									Integer.parseInt(resultMap.get("offline_limit").toString()),
									Integer.parseInt(resultMap.get("offline_daily").toString()),
									Integer.parseInt(resultMap.get("offline_total").toString()),
									Integer.parseInt(resultMap.get("download").toString()),
									Integer.parseInt(resultMap.get("stream_interrupt").toString()),
									Integer.parseInt(resultMap.get("stream_limit").toString()),
									Integer.parseInt(resultMap.get("stream_preview").toString()),
									Integer.parseInt(resultMap.get("stream_daily").toString()),
									Integer.parseInt(resultMap.get("stream_total").toString()),
									Integer.parseInt(resultMap.get("stream_track").toString()));
						}
						
		    	  });
				
				configuration = new Configuration(configVal[0], configVal[1], configVal[2]);
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `FeatureConfiguration`('" + reqParam.getCountryCode() + "',"
//					+ reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					userTypeId = ").toString()),"user_type_id");
//					if (userTypeId == 1) {
//						configVal[0] = new ConfigurationValue(rs.getInt("banner_ads"), rs.getInt("audio_ads"),
//								rs.getInt("favourite"), rs.getInt("dedication"), rs.getInt("share"), rs.getInt("crbt"),
//								rs.getInt("offline_limit"), rs.getInt("offline_daily"), rs.getInt("offline_total"),
//								rs.getInt("download"), rs.getInt("stream_interrupt"), rs.getInt("stream_limit"),
//								rs.getInt("stream_preview"), rs.getInt("stream_daily"), rs.getInt("stream_total"),
//								rs.getInt("stream_track"));
//					}
//					if (userTypeId == 2) {
//						configVal[1] = new ConfigurationValue(rs.getInt("banner_ads"), rs.getInt("audio_ads"),
//								rs.getInt("favourite"), rs.getInt("dedication"), rs.getInt("share"), rs.getInt("crbt"),
//								rs.getInt("offline_limit"), rs.getInt("offline_daily"), rs.getInt("offline_total"),
//								rs.getInt("download"), rs.getInt("stream_interrupt"), rs.getInt("stream_limit"),
//								rs.getInt("stream_preview"), rs.getInt("stream_daily"), rs.getInt("stream_total"),
//								rs.getInt("stream_track"));
//					}
//					if (userTypeId == 3) {
//						configVal[2] = new ConfigurationValue(rs.getInt("banner_ads"), rs.getInt("audio_ads"),
//								rs.getInt("favourite"), rs.getInt("dedication"), rs.getInt("share"), rs.getInt("crbt"),
//								rs.getInt("offline_limit"), rs.getInt("offline_daily"), rs.getInt("offline_total"),
//								rs.getInt("download"), rs.getInt("stream_interrupt"), rs.getInt("stream_limit"),
//								rs.getInt("stream_preview"), rs.getInt("stream_daily"), rs.getInt("stream_total"),
//								rs.getInt("stream_track"));
//					}
//				}
//				configuration = new Configuration(configVal[0], configVal[1], configVal[2]);
//			}
//			mysql.close();

		} catch (Exception e) {
			configuration = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return configuration;
		}

	}

	@SuppressWarnings("finally")
	public SignUpViaConfigurationBean getSignUpViaConfiguration(DeviceInformation  deviceInformation) {
		int signUpViaId = 0;
		SignUpViaConfigurationBean signUpViaConfiguration = new SignUpViaConfigurationBean();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetSignUpViaConfiguration");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryCode", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem());
			
			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);	
			
			signUpViaId =Integer.parseInt(resultMap.get("signup_via_id").toString());
			
			switch (signUpViaId) {
			case 1:
				signUpViaConfiguration.setEmail(1);
				break;
			case 2:
				signUpViaConfiguration.setFacebook(1);
				break;
			case 3:
				signUpViaConfiguration.setGoogle(1);
				break;
			case 4:
				signUpViaConfiguration.setMobileNumber(1);
				break;
			default:
				break;
			}
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetSignUpViaConfiguration`('" + reqParam.getCountryCode() + "',"
//					+ reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					signUpViaId = rs.getInt("signup_via_id");
//					switch (signUpViaId) {
//					case 1:
//						signUpViaConfiguration.setEmail(1);
//						break;
//					case 2:
//						signUpViaConfiguration.setFacebook(1);
//						break;
//					case 3:
//						signUpViaConfiguration.setGoogle(1);
//						break;
//					case 4:
//						signUpViaConfiguration.setMobileNumber(1);
//						break;
//					default:
//						break;
//					}
//				}
//			}
//			mysql.close();

		} catch (Exception e) {
			signUpViaConfiguration = new SignUpViaConfigurationBean();
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return signUpViaConfiguration;
		}

	}

	 

	@SuppressWarnings("finally")
	public List<ContainerBean> getContainerList(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ContainerBean> lst = new ArrayList<ContainerBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetContainerList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inEType", reqParam.getEventType())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inApiVersion", reqParam.getApiVersion());
					//.addValue("in_lang", deviceInformation.getLang());
					
			  Map<String, Object> rs = jdbcCall.execute(inParams);			    			      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
			//	System.out.println("Container List ::"+ ar);
				
				    
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;	    	
		    					lst.add(new ContainerBean(Integer.parseInt(resultMap.get("container_id").toString()), Integer.parseInt(resultMap.get("seq_id").toString()),
								Integer.parseInt(resultMap.get("container_type_id").toString()), resultMap.get("container_title").toString(), Integer.parseInt(resultMap.get("see_all").toString()),
										Integer.parseInt(resultMap.get("item_list_type_id").toString()),
								getContainerItemList(deviceInformation.getCountryId(), reqParam.getUserId(),	Integer.parseInt(resultMap.get("container_id").toString()), reqParam.getImageTechRefId(), 0, 20, reqParam.getApiVersion())));
					    				    			  
		    	  });
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetContainerList`(" + reqParam.getCountryId() + ",'"
//					+ reqParam.getDeviceId() + "','" + reqParam.getLanguageCode() + "')}");
//			int containder_id = 0;
//			if (rs != null) {
//				while (rs.next()) {
//					containder_id = rs.getInt("container_id");
//					if (containder_id == 4 || containder_id == 14 || containder_id == 24 || containder_id == 34
//							|| containder_id == 44 || containder_id == 54 || containder_id == 64 || containder_id == 74
//							|| containder_id == 84 || containder_id == 94 || containder_id == 104
//							|| containder_id == 114) {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getContainerItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 6)));
//					} else {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getContainerItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 20)));
//					}
//
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	@SuppressWarnings("finally")
	public List<ContainerItemBean> getContainerItemList(String countryId, int userID, int containerId, int imageTechRefId,
			int startLimit, int endLimit, String apiversion) {
		List<ContainerItemBean> lst = new ArrayList<ContainerItemBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetContainerItemList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", countryId)
					.addValue("inContainerId", containerId)
					.addValue("inImageTechRefId", imageTechRefId)
					.addValue("inUserId", userID)
					.addValue("inApiVersion", apiversion)					
					.addValue("inStart", startLimit)
					.addValue("inLimit", endLimit); 
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);		      					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//
//				MySQL1 mysql = new MySQL1();
//				ResultSet rs1 = mysql.prepareCall("{call `GetContainerItemList`(" + countryId + "," + containerId + ","
//				+ imageTechRefId + "," + userID + "," + startLimit + "," + endLimit + ")}");
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ContainerItemBean(resultMap.get("item_id").toString(), 
		    			  resultMap.get("item_seq_id").toString(),
		    			  resultMap.get("item_type_id").toString(),
		    			  resultMap.get("item_resource_id").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("resource_description").toString(),
		    			  resultMap.get("resource_image_url").toString(),
		    			  resultMap.get("click_url").toString()));		    			 
		    			//  getBooleanValue(resultMap.get("isCrbtAvailable").toString()),
		    			//  getBooleanValue(resultMap.get("isKaraokeAvailable").toString()),
		    			//  resultMap.get("lang_karaoke_available").toString().split("#")));		    				    			  
		    	 
				});
		 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			//System.out.println("Data for  "+ containerId +" :: "+ lst);
			return lst;
		}

	}

	/// Karaoka List
	@SuppressWarnings("finally")
	public List<ContainerBean> getKaraokeContainerList(DeviceInformation  deviceInformation) {
		List<ContainerBean> lst = new ArrayList<ContainerBean>();
		try {
//			MySQL1 mysql = new MySQL1();
//			ResultSet rs1 = mysql.prepareCall("{call `GetKaraokeContainerList`(" + reqParam.getCountryId() + ",'"
//					+ reqParam.getDeviceId() + "','" + reqParam.getLanguageCode() + "')}");
//			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetKaraokeContainerList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("in_lang", deviceInformation.getLang());
					
			  Map<String, Object> rs = jdbcCall.execute(inParams);
		  //   System.out.println("GetKaraokeContainerList  in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				    
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;	
		    	  String container_title="";
		    	  if(rs.get("container_title")!=null) {
		    		  container_title=rs.get("container_title").toString();
		    	  }
		   
//						lst.add(new ContainerBean(Integer.parseInt(resultMap.get("container_id").toString()), Integer.parseInt(resultMap.get("seq_id").toString()),
//								Integer.parseInt(resultMap.get("container_type_id").toString()), container_title, Integer.parseInt(resultMap.get("see_all").toString()),
//										Integer.parseInt(resultMap.get("item_list_type_id").toString()),
//								getContainerItemList(deviceInformation.getCountryId(), reqParam.getUserId(),
//										Integer.parseInt(resultMap.get("container_id").toString()), reqParam.getImageTechRefId(), 0, 20)));
				 	    				    			  
		    	  });
				
				
				

//			int containder_id = 0;
//			if (rs != null) {
//				while (rs.next()) {
//					containder_id = rs.getInt("container_id");
//					if (containder_id == 4 || containder_id == 14 || containder_id == 24 || containder_id == 34
//							|| containder_id == 44 || containder_id == 54 || containder_id == 64 || containder_id == 74
//							|| containder_id == 84 || containder_id == 94 || containder_id == 104
//							|| containder_id == 114) {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getKaraokeItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 6)));
//					} else {
//						lst.add(new ContainerBean(rs.getInt("container_id"), rs.getInt("seq_id"),
//								rs.getInt("container_type_id"), rs.getString("container_title"), rs.getInt("see_all"),
//								rs.getInt("item_list_type_id"),
//								getKaraokeItemList(reqParam.getCountryId(), reqParam.getUserId(),
//										rs.getInt("container_id"), reqParam.getImageTechRefId(), 0, 20)));
//					}
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	@SuppressWarnings("finally")
	public List<ContainerItemBean> getKaraokeItemList(int countryId, int userID, int containerId, int imageTechRefId,
			int startLimit, int endLimit) {
		List<ContainerItemBean> lst = new ArrayList<ContainerItemBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetKaraokaContainerItemList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", countryId)
					.addValue("inContainerId", containerId)
					.addValue("inImageTechRefId", imageTechRefId)
					.addValue("inUserId", userID)
					.addValue("inStart", startLimit)
					.addValue("inLimit", endLimit); 
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ContainerItemBean(resultMap.get("item_id").toString(), 
		    			  resultMap.get("item_seq_id").toString(),
		    			  resultMap.get("item_type_id").toString(),
		    			  resultMap.get("item_resource_id").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("resource_description").toString(),
		    			  resultMap.get("resource_image_url").toString(),
		    			  resultMap.get("click_url").toString()));		    			 
		    			//  getBooleanValue(resultMap.get("isCrbtAvailable").toString()),
		    			//  getBooleanValue(resultMap.get("isKaraokeAvailable").toString()),
		    			 // resultMap.get("lang_karaoke_available").toString().split("#")));		    				    			  
		    	 
				});
			   
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}
	
	
	public List<TrackData> getAllItemList(int userID, String itemType, int imageTechRefId, int startLimit,
			int endLimit, String eType) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAllItemList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inUserId", userID)
					.addValue("inItemType", itemType)
					.addValue("inImageTechRefId", imageTechRefId)					
					.addValue("inStart", startLimit)
					.addValue("inLimit", endLimit); 
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
//				MySQL1 mysql = new MySQL1();
//				ResultSet rs1 = mysql.prepareCall("{call `GetAllItemList`(" + countryId + ",'" + itemType + "',"
//						+ imageTechRefId + "," + startLimit + "," + endLimit + ")}");
//				  
				
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  
		    	Boolean isFav = false;
				int itemtypeid = 5;
					
		    	  if (eType.equalsIgnoreCase("crbt"))
		    	  {
		    		//  rs.getBoolean("isFav") , rs.getInt("itemTypeId")
		    		  
		    		isFav = getBooleanValue(resultMap.get("isFav").toString());
					itemtypeid = Integer.parseInt(resultMap.get("itemTypeId").toString());
		    	  }
		    	 
		   //	  System.out.println("resultMap :: "+ resultMap);
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), resultMap.get("ivrMMNumber").toString(),
							resultMap.get("resource_title").toString(), Integer.parseInt(resultMap.get("album_id").toString()), resultMap.get("album_title").toString(),
							Integer.parseInt(resultMap.get("artist_id").toString()), resultMap.get("artist_name").toString(), Integer.parseInt(resultMap.get("genre_id").toString()),
							resultMap.get("genre_name").toString(), Long.parseLong(resultMap.get("play_count").toString()), Long.parseLong(resultMap.get("favourite_count").toString()),
							Long.parseLong(resultMap.get("share_count").toString()), Long.parseLong(resultMap.get("size").toString()), Long.parseLong(resultMap.get("duration").toString()),
							resultMap.get("filePath").toString(), resultMap.get("image_url").toString(),
							resultMap.get("video_id").toString(), isFav, itemtypeid));
						//	getBooleanValue(resultMap.get("isKaraokeAvailable").toString()), resultMap.get("lang_karaoke_available").toString().split("#"))
		    		//  );		 
		    		 				    			  
		    	 
				});
			  
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAllItemList`(" + countryId + ",'" + itemType + "',"
//					+ imageTechRefId + "," + startLimit + "," + endLimit + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ContainerItemBean(rs.getString("item_id"), rs.getString("item_seq_id"),
//							rs.getString("item_type_id"), rs.getString("item_resource_id"),
//							rs.getString("resource_title"), rs.getString("resource_description"),
//							rs.getString("resource_image_url"), rs.getString("click_url"),
//							rs.getBoolean("isCrbtAvailable"), rs.getBoolean("isKaraokeAvailable"),
//							rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	// Karaoka List
	@SuppressWarnings("finally")
	public List<ContainerItemBean> getAllItemList(HomeParameters reqParam,int countryId, String itemType, int imageTechRefId, int startLimit,
			int endLimit) {
		List<ContainerItemBean> lst = new ArrayList<ContainerItemBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAllItemList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inItemType", itemType)
					.addValue("inImageTechRefId", imageTechRefId)					
					.addValue("inStart", startLimit)
					.addValue("inLimit", endLimit); 
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
//				MySQL1 mysql = new MySQL1();
//				ResultSet rs1 = mysql.prepareCall("{call `GetAllItemList`(" + countryId + ",'" + itemType + "',"
//						+ imageTechRefId + "," + startLimit + "," + endLimit + ")}");
//				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    //	  System.out.println("resultMap :: "+ resultMap);
		    	  lst.add(new ContainerItemBean(resultMap.get("item_id").toString(), 
		    			  resultMap.get("item_seq_id").toString(),
		    			  resultMap.get("item_type_id").toString(),
		    			  resultMap.get("item_resource_id").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("resource_description").toString(),
		    			  resultMap.get("resource_image_url").toString(),
		    			  resultMap.get("click_url").toString()));		    			 
		    			//  getBooleanValue(resultMap.get("isCrbtAvailable").toString()),
		    			//  getBooleanValue(resultMap.get("isKaraokeAvailable").toString()),
		    			//  resultMap.get("lang_karaoke_available").toString().split("#")));		    				    			  
		    	 
				});
			  
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAllItemList`(" + countryId + ",'" + itemType + "',"
//					+ imageTechRefId + "," + startLimit + "," + endLimit + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ContainerItemBean(rs.getString("item_id"), rs.getString("item_seq_id"),
//							rs.getString("item_type_id"), rs.getString("item_resource_id"),
//							rs.getString("resource_title"), rs.getString("resource_description"),
//							rs.getString("resource_image_url"), rs.getString("click_url"),
//							rs.getBoolean("isCrbtAvailable"), rs.getBoolean("isKaraokeAvailable"),
//							rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}
 
	@SuppressWarnings("finally")
	public List<LeftMenuTitle> getLeftMenuTitle(DeviceInformation  deviceInformation) {
		List<LeftMenuTitle> lst = new ArrayList<LeftMenuTitle>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetLeftMenuTitle");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()					
					.addValue("lang", deviceInformation.getLang());
				

			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new LeftMenuTitle(
		    			  resultMap.get("title").toString(),
		    			  resultMap.get("url").toString(),
		    			  getBooleanValue(resultMap.get("isExternal").toString()),
		    			  resultMap.get("popupText").toString(),
		    			  resultMap.get("popupTitle").toString()));	
		    				    			  
		    	  });
				
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	@SuppressWarnings("finally")
	public List<PaymentMethod> getPaymentMethods(DeviceInformation deviceInformation) {
		List<PaymentMethod> lst = new ArrayList<PaymentMethod>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetPaymentMethod");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()					
					.addValue("inCountryCode", deviceInformation.getCcode());
				

			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				ar.forEach(item->{
					  Map resultMap = (Map) item;
					  lst.add(new PaymentMethod(Integer.parseInt(resultMap.get("id").toString()), 
								resultMap.get("payment_method").toString()));
					
				  });		
				
			 	    
				
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	 
	
	  
	@SuppressWarnings("finally")
	public List<HikeKeyboardItemBean> getHikeKeyboardItem(RequestParameter reqParam) {
		List<HikeKeyboardItemBean> lst = new ArrayList<HikeKeyboardItemBean>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetHikeKeyboardItem");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
					
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new HikeKeyboardItemBean(Integer.parseInt(resultMap.get("item_seq_id").toString()), 
		    			  Integer.parseInt(resultMap.get("item_type_id").toString()),
		    			  resultMap.get("item_resource_id").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("resource_description").toString(),
		    			  resultMap.get("resource_image_url").toString(),
		    			  resultMap.get("share_message").toString()
		    			  ));	
		    				    			  
		    	  });
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{call `GetHikeKeyboardItem`(" + reqParam.getCountryId() + "," + reqParam.getImageTechRefId() + ","
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new HikeKeyboardItemBean(rs.getInt("item_seq_id"), rs.getInt("item_type_id"),
//							rs.getString("item_resource_id"), rs.getString("resource_title"),
//							rs.getString("resource_description"), rs.getString("resource_image_url"),
//							rs.getString("share_message")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}

	}

	@SuppressWarnings("finally")
	public Splash getSplashScreenData(RequestParameter reqParam) {
		Splash data = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("SplashScreen");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inSourceId", reqParam.getSourceId())
					.addValue("inOperatingSystemId", reqParam.getOperatingSystemId())
					.addValue("inSplashScreenType", reqParam.getEventType());
					
					
			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);
			 
			data = new Splash(new SplashScreenData(resultMap.get("splash_url").toString(),
					resultMap.get("splash_version").toString(),
					resultMap.get("logo_url").toString(), resultMap.get("logo_version").toString()));			
			
 
		} catch (Exception e) {
			data = null;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return data;
		}

	}

	@SuppressWarnings("finally")
	public AlbumData album(HomeParameters reqParam, DeviceInformation deviceInformation) {
		AlbumData album = null;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");	
			
			System.out.println("{call `GetAlbumMetaData`(1," + deviceInformation.getCountryId() + ","
			+ reqParam.getId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
			+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
			
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			 
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				for(int i =0; i< ar.size();i++)
				{
					Map resultMap = (Map) ar.get(i);
					album = new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  );	
		    				    			  
		    	  }	
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					album = (new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return album;
		}
	}

	@SuppressWarnings("finally")
	public List<TrackData> albumTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			 SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");
				System.out.println("{call `GetAlbumMetaData`(2," + deviceInformation.getCountryId() + ","
				+ reqParam.getId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
				+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
			 
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), resultMap.get("ivrMMNumber").toString(),
								resultMap.get("resource_title").toString(), Integer.parseInt(resultMap.get("album_id").toString()), resultMap.get("album_title").toString(),
								Integer.parseInt(resultMap.get("artist_id").toString()), resultMap.get("artist_name").toString(), Integer.parseInt(resultMap.get("genre_id").toString()),
								resultMap.get("genre_name").toString(), Long.parseLong(resultMap.get("play_count").toString()), Long.parseLong(resultMap.get("favourite_count").toString()),
								Long.parseLong(resultMap.get("share_count").toString()), Long.parseLong(resultMap.get("size").toString()), Long.parseLong(resultMap.get("duration").toString()),
								"-",
//								getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
								resultMap.get("image_url").toString(),
								resultMap.get("video_id").toString(), false,5)
			    		  );			    			  
			    	  });			
					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> allAlbums(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			
			 SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), resultMap.get("ivrMMNumber").toString(),
			    					  resultMap.get("album_title").toString(), resultMap.get("artist_name").toString(), 
			    					  Integer.parseInt(resultMap.get("album_rating").toString()),
			    					  Integer.parseInt(resultMap.get("album_tracks_count").toString()), 
			    					  resultMap.get("image_url").toString()));			    			  
			    	  });					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> newAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> popularAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> featuredAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAlbumMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inAlbumId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_rating").toString()),
		    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAlbumMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getAlbumId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<AlbumData> albumRecommendation(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inResourceCode", reqParam.getId() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("album_title").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_rating").toString()),
			    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });
			
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetRecommendation`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public ArtistData artist(HomeParameters reqParam, DeviceInformation deviceInformation) {
		ArtistData artist = null;
		try {
			

			System.out.println("{CALL `GetArtistMetaData`(5," + deviceInformation.getCountryId() + ","
					+ reqParam.getId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",5)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				for(int i =0; i< ar.size();i++)
				{
					Map resultMap = (Map) ar.get(i);
					
					artist = new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), resultMap.get("ivrMMNumber").toString(),
							resultMap.get("artist_name").toString(),  Integer.parseInt(resultMap.get("album_count").toString()), Integer.parseInt(resultMap.get("track_count").toString()),
							resultMap.get("image_url").toString());
					artist.setTrackShareCount(Long.parseLong(resultMap.get("track_share_count").toString()));
					artist.setTrackPlayCount(Long.parseLong(resultMap.get("track_play_count").toString()));
					artist.setTrackFavouriteCount(Long.parseLong(resultMap.get("track_favourite_count").toString()));
					artist.setPageTitle(resultMap.get("artist_page_title").toString());
					artist.setMetaDescription(resultMap.get("meta_description").toString());
					artist.setMetaKeywords(resultMap.get("meta_keywords").toString());
					artist.setAboutArtist(resultMap.get("about_artist").toString());
				}
				
				
//			MySQL mysql = new MySQL();
//			// ResultSet rs = mysql.prepareCall("{call `GetArtistMetaData`(5," +
//			// reqParam.getCountryId() + "," + reqParam.getSourceId() + "," +
//			// reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','" +
//			// reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() +
//			// "','" + reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" +
//			// reqParam.getTrackCode() + "'," + reqParam.getAudioTechRefId() + "," +
//			// reqParam.getImageTechRefId() + "," + reqParam.getStartLimit() + "," +
//			// reqParam.getEndLimit() + ")}");
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(5," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					artist = new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url"));
//					artist.setTrackShareCount(rs.getLong("track_share_count"));
//					artist.setTrackPlayCount(rs.getLong("track_play_count"));
//					artist.setTrackFavouriteCount(rs.getLong("track_favourite_count"));
//					artist.setPageTitle(rs.getString("artist_page_title"));
//					artist.setMetaDescription(rs.getString("meta_description"));
//					artist.setMetaKeywords(rs.getString("meta_keywords"));
//					artist.setAboutArtist(rs.getString("about_artist"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return artist;
		}
	}

	@SuppressWarnings("finally")
	public OptScreenConfig getOptScreenConfig() {
		OptScreenConfig optObj = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetOPTPageConfig");			  			  
		  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inUserId","")
					.addValue("inMsisdn", "");					
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				for(int i =0; i< ar.size();i++)
				{
				  Map resultMap = (Map) ar.get(i);
		    	  optObj= new OptScreenConfig(
		    			  	resultMap.get("title").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							resultMap.get("buttonText").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							resultMap.get("infoText").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							getBooleanValue(resultMap.get("skipVisiblity").toString()),
							getBooleanValue(resultMap.get("infoVisiblity").toString()),
							Integer.parseInt(resultMap.get("infoType").toString()),
							resultMap.get("infoUrl").toString(),
							Integer.parseInt(resultMap.get("packageId").toString()),
							resultMap.get("imageUrl").toString(),
							getBooleanValue(resultMap.get("optScreenVisibility").toString()));    			  	
		    				    			  
		    	  }
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{call `GetOPTPageConfig`('" + reqParam.getUserId() + "','" + reqParam.getMsisdn() + "')}");
//			int containder_id = 0;
//			if (rs != null) {
//				while (rs.next()) {
//					optObj = new OptScreenConfig(
//							rs.getString("title").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//							rs.getString("buttonText").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//							rs.getString("infoText").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//							rs.getBoolean("skipVisiblity"), rs.getBoolean("infoVisiblity"), rs.getInt("infoType"),
//							rs.getString("infoUrl"), rs.getInt("packageId"), rs.getString("imageUrl"),
//							rs.getBoolean("optScreenVisibility"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return optObj;
		}
	}

	@SuppressWarnings("finally")
	public boolean followingArtist(HomeParameters reqParam) {
		boolean following = false;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FollowingArtist");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inArtistId", reqParam.getId());
			  

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);				    
			following =getBooleanValue(resultMap.get("following").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FollowingArtist`(1," + reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					following = rs.getBoolean("following");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return following;
		}
	}

	@SuppressWarnings("finally")
	public boolean followArtist(HomeParameters reqParam) {
		boolean following = false;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FollowingArtist");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inArtistId", reqParam.getId());
			  

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				following =getBooleanValue(resultMap.get("following").toString());
			 				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FollowingArtist`(2," + reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					following = rs.getBoolean("following");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return following;
		}
	}

	@SuppressWarnings("finally")
	public boolean unfollowArtist(HomeParameters reqParam) {
		boolean following = false;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("FollowingArtist");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inArtistId", reqParam.getId());
			 
			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);				    
			following =getBooleanValue(resultMap.get("following").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `FollowingArtist`(3," + reqParam.getUserId() + "," + reqParam.getArtistId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					following = rs.getBoolean("following");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return following;
		}
	}

	@SuppressWarnings("finally")
	public int insertNotification(RequestParameter reqParam) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("NotificationSend");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inMsg", reqParam.getMessage())
					.addValue("inValue", reqParam.getTrackCode())
					.addValue("inNotification_title", reqParam.getSender())
					.addValue("inXid", reqParam.getNotificationId());
			 
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `NotificationSend`(" + reqParam.getUserId() + ",'"
//					+ reqParam.getMessage() + "'," + reqParam.getTrackCode() + ",'" + reqParam.getSender() + "',"
//					+ reqParam.getNotificationId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendation(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inResourceCode", reqParam.getId() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_count").toString()),
			    			  Integer.parseInt(resultMap.get("track_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });
					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetRecommendation`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendationByAlbum(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", 0)
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inId", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    		
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
				
				
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetArtistRecommendation`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public SubscriptionBenefits SubscriptionBenifits() {
		SubscriptionBenefits obj = null;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetBenefits");			  			  
//			  SqlParameterSource inParams = new MapSqlParameterSource()
//					.addValue("inCountryId", countryID)
//					.addValue("in_lang", langCode);
					
			Map<String, Object> rs = jdbcCall.execute();
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
				
			  for(int i =0; i< ar.size();i++)
				{
					Map resultMap = (Map) ar.get(i);
					obj = new SubscriptionBenefits(resultMap.get("casualGeneralLine").toString(),
							resultMap.get("liteGeneralLine").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							resultMap.get("premiumGeneralLine").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							resultMap.get("benefitsHeading").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
							resultMap.get("benefits").toString().replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r")
									.replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
									resultMap.get("pageHeader").toString(),
									resultMap.get("packHeader").toString(), 
									resultMap.get("benefitHeader").toString());
				}
			  
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetBenefits`(" + countryID + ",'" + langCode + "')}");
//			while (rs.next()) {
//				obj = new SubscriptionBenefits(rs.getString("casualGeneralLine"),
//						rs.getString("liteGeneralLine").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//						rs.getString("premiumGeneralLine").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//						rs.getString("benefitsHeading").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//						rs.getString("benefits").replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r")
//								.replaceAll("\\\\n", "\n").replaceAll("\\\\r", "\r"),
//						rs.getString("pageHeader"), rs.getString("packHeader"), rs.getString("benefitHeader"));
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return obj;
		}
	}

	 
	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendationByArtist(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", 0)
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inId", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    			 
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistRecommendation`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendationByPlaylist(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", 1)
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inId", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    		
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetArtistRecommendation`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendationByGenre(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", 0)
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inId", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    		
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetArtistRecommendation`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	@SuppressWarnings("finally")
	public List<ArtistData> artistRecommendationByTrack(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",5)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", 1)
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inId", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),		    		
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetArtistRecommendation`(5," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<ArtistData> allArtists(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			 
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				
			  ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("album_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(
					"Exception in Vodafone .allArtists(RequestParameter reqParam) - " + e.getMessage());
		} finally {
			return lst;
		}
	}

	public List<ArtistData> newArtists(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item-> {
			    	  Map resultMap = (Map) item;
			    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_count").toString()),
			    			  Integer.parseInt(resultMap.get("track_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });	
			
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<ArtistData> popularArtists(RequestParameter reqParam) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inArtistId", reqParam.getArtistId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item-> {
			    	  Map resultMap = (Map) item;
			    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_count").toString()),
			    			  Integer.parseInt(resultMap.get("track_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });	
			
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<ArtistData> featuredArtists(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",11)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("album_count").toString()),
		    			  Integer.parseInt(resultMap.get("track_count").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(11," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<AlbumData> artistAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("album_title").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_rating").toString()),
			    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public GenreBean genre(HomeParameters reqParam,DeviceInformation deviceInformation) {
		GenreBean genre = null;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",8)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inGenreId", reqParam.getId() )
					.addValue("inArtistId", reqParam.getSubid())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					 
					for(int i =0; i< ar.size();i++)
					{
						Map resultMap = (Map) ar.get(i);
						genre = new GenreBean(Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  resultMap.get("image_url").toString()
			    			  );	
					}					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(8," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					genre = new GenreBean(rs.getInt("genre_id"), rs.getString("genre_name"), rs.getString("image_url"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return genre;
		}
	}

	public List<AlbumData> genreAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {		
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inGenreId", reqParam.getId() )
						.addValue("inArtistId", reqParam.getSubid())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("album_title").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_rating").toString()),
			    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });			
					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<AlbumData> genreArtistAlbums(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<AlbumData> lst = new ArrayList<AlbumData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",6)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inGenreId", reqParam.getId() )
						.addValue("inArtistId", reqParam.getSubid())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new AlbumData(Integer.parseInt(resultMap.get("album_id").toString()), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("album_title").toString(),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("album_rating").toString()),
			    			  Integer.parseInt(resultMap.get("album_tracks_count").toString()),
			    			  resultMap.get("image_url").toString()
			    			  ));	
			    				    			  
			    	  });			
					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(6," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new AlbumData(rs.getInt("album_id"), rs.getString("ivrMMNumber"),
//							rs.getString("album_title"), rs.getString("artist_name"), rs.getInt("album_rating"),
//							rs.getInt("album_tracks_count"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<ArtistData> genreArtists(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<ArtistData> lst = new ArrayList<ArtistData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inGenreId", reqParam.getId() )
						.addValue("inArtistId", reqParam.getSubid())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  
			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			//Map resultMap1 = (Map) ar.get(0);
			      
			ar.forEach(item->{
	    	  Map resultMap = (Map) item;
	    	  lst.add(new ArtistData(Integer.parseInt(resultMap.get("artist_id").toString()), 
	    			  resultMap.get("ivrMMNumber").toString(),
	    			  resultMap.get("artist_name").toString(),
	    			  Integer.parseInt(resultMap.get("album_count").toString()),
	    			  Integer.parseInt(resultMap.get("track_count").toString()),
	    			  resultMap.get("image_url").toString()
	    			  ));	
	    				    			  
	    	  });
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new ArtistData(rs.getInt("artist_id"), rs.getString("ivrMMNumber"),
//							rs.getString("artist_name"), rs.getInt("album_count"), rs.getInt("track_count"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> artistTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			System.out.println("{CALL `GetArtistMetaData`(3," + deviceInformation.getCountryId() + ","
			+ reqParam.getId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
			+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), resultMap.get("ivrMMNumber").toString(),
								resultMap.get("resource_title").toString(), Integer.parseInt(resultMap.get("album_id").toString()), resultMap.get("album_title").toString(),
								Integer.parseInt(resultMap.get("artist_id").toString()), resultMap.get("artist_name").toString(), Integer.parseInt(resultMap.get("genre_id").toString()),
								resultMap.get("genre_name").toString(), Long.parseLong(resultMap.get("play_count").toString()), Long.parseLong(resultMap.get("favourite_count").toString()),
								Long.parseLong(resultMap.get("share_count").toString()), Long.parseLong(resultMap.get("size").toString()), Long.parseLong(resultMap.get("duration").toString()),
								"-", 
								//getFullStreamUrl(resultMap.get("filePath").toString(), reqParam), 
								resultMap.get("image_url").toString(),
								resultMap.get("video_id").toString(),false,5)
			    			  );	
			    				    			  
			    	  });			
					
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}
	
	
	

	public List<TrackData> geAllTracks(RequestParameter reqParam, int countryId, String itemType, int imageTechRefId,
			int startLimit, int endLimit) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GteAllItemList");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", countryId)
					.addValue("inItemType", itemType)
					.addValue("inImageTechRefId", imageTechRefId)
					.addValue("inStart", startLimit)
					.addValue("inLimit", endLimit);
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false,5)
			    			  
			    			  );	
			    				    			  
			    	  });		
//			MySQL mysql = new MySQL();
//			// ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(6," +
//			// reqParam.getCountryId() + "," + reqParam.getArtistId() + "," +
//			// reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," +
//			// reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			ResultSet rs = mysql.prepareCall("{call `GetAllItemList`(" + countryId + ",'" + itemType + "',"
//					+ imageTechRefId + "," + startLimit + "," + endLimit + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getArtistTopTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",6)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
				    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false,5)
			    			  
			    			  );	
			    				    			  
			    	  });		
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(6," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getArtistSingleTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",7)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
			    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(7," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getArtistFeaturedTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",8)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
				    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(8," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getArtistVideoTracks(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",9)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
				    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(9," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> newTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",4)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
			    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> popularTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",10)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),false, 5)
		    			  
		    			  );	
		    				    			  
		    	  });		
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(10," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> featuredTracks(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetArtistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",4)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inArtistId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),false, 5)
		    			  
		    			  );	
		    				    			  
		    	  });		
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetArtistMetaData`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> trackRecommendation(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRecommendation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inResourceCode", reqParam.getId() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
			    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5));
			    				    			  
			    	  });					
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetRecommendation`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getAutoPlayFeature(RequestParameter reqParam ,  DeviceInformation  deviceInformation ) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetAutoPlayMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inSourceId", reqParam.getSourceId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inResourceCode", reqParam.getTrackCode() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5));
			    				    			  
			    	  });	
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `GetAutoPlayMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> getFavouriteTracks(UserParameters reqParam, DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			System.out.println("{call `UserFavourite`(1," + reqParam.getItemTypeId() + ","
			+ deviceInformation.getSource() + "," + reqParam.getUserId() + ",'" + deviceInformation.getOperatingSystem() + "','"
			+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
			+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "','" + reqParam.getId() + "',"
			+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
			+ "," + reqParam.getEndLimit() + ")}");
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
						.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),false,5)
		    			  
		    			  );	
		    				    			  
		    	  });	
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public int addTrackToFavourite(UserParameters reqParam, DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			
			System.out.println("{call `UserFavourite`(2," + deviceInformation.getCountryId() + ","
			+ deviceInformation.getSource() + "," + reqParam.getUserId() + ",'" + deviceInformation.getOperatingSystem() + "','"
			+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
			+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "','" + reqParam.getId() + "',"
			+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
			+ "," + reqParam.getEndLimit() + ")}");
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
				responseCode = Integer.parseInt(resultMap.get("code").toString());				

			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public int delTrackFromFavourite(UserParameters reqParam, DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId() )
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			  ArrayList<Object> ar = new ArrayList<Object>();
			  ar = (ArrayList) rs.get("#result-set-1");
			  Map resultMap = (Map) ar.get(0);
			  responseCode = Integer.parseInt(resultMap.get("code").toString());		
			  
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public List<TrackData> selectFavourite(UserParameters reqParam, DeviceInformation  deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());

			  		Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
			    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),			    			  
			    			  getBooleanValue(resultMap.get("isFav").toString()),
			    			  Integer.parseInt(resultMap.get("itemTypeId").toString()))
			    			  
			    			  );	
			    				    			  
			    	  });	
					
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public int insertFavourite(UserParameters reqParam, DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			  ArrayList<Object> ar = new ArrayList<Object>();
			  ar = (ArrayList) rs.get("#result-set-1");
			  Map resultMap = (Map) ar.get(0);
			  responseCode = Integer.parseInt(resultMap.get("code").toString());     				
			 
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	 

	public int updateFavourite(UserParameters reqParam, DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",5)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			  ArrayList<Object> ar = new ArrayList<Object>();
			  ar = (ArrayList) rs.get("#result-set-1");
			  Map resultMap = (Map) ar.get(0);
			  responseCode = Integer.parseInt(resultMap.get("code").toString());    
			  
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(5," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getOldOrderId()
//					+ "," + reqParam.getNewOrderId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public int deleteFavourite(UserParameters reqParam, DeviceInformation deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", reqParam.getId())						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			  ArrayList<Object> ar = new ArrayList<Object>();
			  ar = (ArrayList) rs.get("#result-set-1");
			  Map resultMap = (Map) ar.get(0);
			  responseCode = Integer.parseInt(resultMap.get("code").toString());    
			  
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public DownloadURL getDownloadUrl(UserOfflineModel reqParam, String messageException, DeviceInformation deviceInformation) {
		DownloadURL dwld = new DownloadURL();
			try {
				dwld.setCode(0);
				dwld.setMessage("Success");
				 String timestamp = String.valueOf(System.currentTimeMillis());
				System.out.println("{CALL `UserOffline`(2," + deviceInformation.getCountryId() + ","
						+ deviceInformation.getSource() + "," + reqParam.getUserId() + ",'" + deviceInformation.getOperatingSystem()
						+ "','" + deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
						+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "','" + reqParam.getTrackCode()
						+ "'," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ",'" + timestamp
						+ "'," + 0 + "," + 10 + ")}");			
			 		
				try { 
					  	Map<String, Object> rs =callUserOffline(2, timestamp,  reqParam, deviceInformation);									      
						ArrayList<Object> ar = new ArrayList<Object>();
						ar = (ArrayList) rs.get("#result-set-1");
						Map resultMap = (Map) ar.get(0);				    
						dwld.setCode(Integer.parseInt(resultMap.get("code").toString()));
						dwld.setMessage(resultMap.get("message").toString());
					} catch (Exception e) {
					System.out.println(
							"Exception in Glo Nigeria MainServlet.getDownloadUrl_New(RequestParameter reqParam, DeviceInformation devInfo) - "
									+ e.getMessage());
				}

				String params = "ocid=" + deviceInformation.getCountryId() + "&src=" + deviceInformation.getSource() + "&os="
						+ deviceInformation.getOperatingSystem() + "&osv=" + deviceInformation.getOperatingSystemVersion() + "&model="
						+ deviceInformation.getDeviceModel() + "&devid=" + deviceInformation.getDeviceId() + "&devpin="
						+ deviceInformation.getDevicePin() + "&evt=39&userid=" + reqParam.getUserId() + "&trackid="
						+ reqParam.getTrackCode() + "&dtype=1&dwldquality=" + reqParam.getDownloadQuality() + "&token="
						+ timestamp + "&lang=" + deviceInformation.getLang();
				dwld.setWithAd(0);
				dwld.setUrl(apiDomainName+"downloadtrack?p=" +URLEncoder.encode( AESEncriptionDecription.encrypt(params) ,"UTF-8" ));
//				dwld.setUrl(apiDomainName + "downloadtrack?p=" + DatatypeConverter.printBase64Binary(new AESEncriptionDecription().encrypt(params).getBytes("UTF-8")));
			} catch (Exception e) {
			//	e.printStackTrace();
				dwld.setCode(110);
				dwld.setMessage(messageException);	
				System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
				e.printStackTrace();
			} finally {
				return dwld;
			}
		}

		
	
	public void requestDedication(RequestParameter reqParam,   DeviceInformation  deviceInformation) {
		try {
		//	MySQL mysql = new MySQL();
			int count;
			String arr[];
			if (reqParam.getEventType().equalsIgnoreCase("1")) {
				arr = reqParam.getEmailAddress().split(";");
				count = arr.length;
				arr = null;
			} else {
				arr = reqParam.getMsisdn().split(";");
				count = arr.length;
				arr = null;
			}
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDedication");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inSourceId", reqParam.getSourceId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inDedicationTypeId", reqParam.getEventType() )
						.addValue("inResourceCode", reqParam.getTrackCode())
						.addValue("inRecipientCount", count)
						.addValue("inDedicateeName", reqParam.getUserName())
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inEmail", reqParam.getEmailAddress())
						.addValue("inDedicationMessage", reqParam.getDedicationMessage())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId());
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				
			  
//			  
//			ResultSet rs = mysql.prepareCall("{CALL `UserDedication`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getTrackCode() + "'," + count + ",'" + reqParam.getUserName() + "','"
//					+ reqParam.getMsisdn() + "','" + reqParam.getEmailAddress() + "','"
//					+ reqParam.getDedicationMessage() + "'," + reqParam.getAudioTechRefId() + ","
//					+ reqParam.getImageTechRefId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}

	public List<DedicateeData> recentlyDedicatee(RequestParameter reqParam ,    DeviceInformation  deviceInformation) {
		List<DedicateeData> lst = new ArrayList<DedicateeData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDedication");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inSourceId", reqParam.getSourceId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inDedicationTypeId", reqParam.getEventType() )
						.addValue("inResourceCode", reqParam.getTrackCode())
						.addValue("inRecipientCount", 0)
						.addValue("inDedicateeName", reqParam.getUserName())
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inEmail", reqParam.getEmailAddress())
						.addValue("inDedicationMessage", reqParam.getDedicationMessage())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId());
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new DedicateeData(Integer.parseInt(resultMap.get("id").toString()), 
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("dedicatee_name").toString(),
		    			  resultMap.get("dedicatee").toString(),
		    			  resultMap.get("datetime").toString()
		    			  ));			    				    			  
		    	  });			    
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserDedication`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getTrackCode() + "',0,'" + reqParam.getUserName() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getEmailAddress() + "','" + reqParam.getDedicationMessage() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new DedicateeData(rs.getInt("id"), rs.getString("resource_title"),
//							rs.getString("dedicatee_name"), rs.getString("dedicatee"), rs.getString("datetime")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<DedicateeData> removeRecentlyDedicatee(RequestParameter reqParam,     DeviceInformation  deviceInformation) {
		List<DedicateeData> lst = new ArrayList<DedicateeData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDedication");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inSourceId", reqParam.getSourceId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inDedicationTypeId", reqParam.getEventType() )
						.addValue("inResourceCode", reqParam.getTrackCode())
						.addValue("inRecipientCount", 0)
						.addValue("inDedicateeName", reqParam.getUserName())
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inEmail", reqParam.getEmailAddress())
						.addValue("inDedicationMessage", reqParam.getDedicationMessage())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId());
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new DedicateeData(Integer.parseInt(resultMap.get("id").toString()), 
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("dedicatee_name").toString(),
		    			  resultMap.get("dedicatee").toString(),
		    			  resultMap.get("datetime").toString()
		    			  ));			    				    			  
		    	  });			    
				
//				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserDedication`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getTrackCode() + "',0,'" + reqParam.getUserName() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getEmailAddress() + "','" + reqParam.getDedicationId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new DedicateeData(rs.getInt("id"), rs.getString("resource_title"),
//							rs.getString("dedicatee_name"), rs.getString("dedicatee"), rs.getString("date_time")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<DedicationData> getDedicationDetail(RequestParameter reqParam,      DeviceInformation  deviceInformation) {
		List<DedicationData> lst = new ArrayList<DedicationData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDedication");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inSourceId", reqParam.getSourceId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inDedicationTypeId", reqParam.getEventType() )
						.addValue("inResourceCode", reqParam.getTrackCode())
						.addValue("inRecipientCount", 0)
						.addValue("inDedicateeName", reqParam.getUserName())
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inEmail", reqParam.getEmailAddress())
						.addValue("inDedicationMessage", reqParam.getDedicationMessage())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId());
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new DedicationData(
		    			  	Integer.parseInt(resultMap.get("dedication_id").toString()), 
		    			  	resultMap.get("user_name").toString(),
							resultMap.get("request_date").toString(),
							resultMap.get("dedication_msg").toString(),
							resultMap.get("resource_code").toString(),
							resultMap.get("resource_title").toString(),
							Integer.parseInt(resultMap.get("album_id").toString()),
							resultMap.get("album_title").toString(),
							Integer.parseInt(resultMap.get("artist_id").toString()),
							resultMap.get("artist_name").toString(),
							Integer.parseInt(resultMap.get("genre_id").toString()),
							resultMap.get("genre_name").toString(),
							Long.parseLong(resultMap.get("size").toString()),
							Long.parseLong(resultMap.get("duration").toString()),
							getFullStreamUrl(resultMap.get("filePath").toString(), reqParam).toString(),
							resultMap.get("image_url").toString(),
							resultMap.get("user_image_url").toString())
		    			  );			    				    			  
		    	  });			    
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserDedication`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getTrackCode() + "',0,'" + reqParam.getUserName() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getEmailAddress() + "','" + reqParam.getDedicationId() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new DedicationData(rs.getInt("dedication_id"), ).toStri).toString(),(),"user_name"),
//							rs.getString("request_date"), rs.getString("dedication_msg"), rs.getString("resource_code"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("user_image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<MobileOperatorData> crbtOperators(RequestParameter reqParam,      DeviceInformation  deviceInformation) {
		List<MobileOperatorData> lst = new ArrayList<MobileOperatorData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CallerRingBackTone");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inOperatorId", reqParam.getOperatorId())
					.addValue("inSourceId", reqParam.getSourceId())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inDevicePin", deviceInformation.getDevicePin())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inResourceCode", reqParam.getTrackCode())
					.addValue("inTransactionId", reqParam.getTransactionId())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inPassword", reqParam.getOneTimePassword());
			  

			Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");				
				      
			ar.forEach(item->{
		    Map resultMap = (Map) item;
		    lst.add(new MobileOperatorData(Integer.parseInt(resultMap.get("id").toString()), 
		    		resultMap.get("operator").toString()
		    	));	
		    				    			  
		    });
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `CallerRingBackTone`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new MobileOperatorData(rs.getInt("id"), rs.getString("operator")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	/*
	 * public Transaction requestCRBT(RequestParameter reqParam, DeviceInformation
	 * reqParam) { Transaction x = new Transaction(reqParam.getCountryId(),
	 * reqParam.getSourceId(), reqParam.getMsisdn()); try { MySQL mysql = new
	 * MySQL(); ResultSet rs = mysql.prepareCall("{CALL `CallerRingBackTone`(2," +
	 * reqParam.getCountryId() + "," + reqParam.getOperatorId() + "," +
	 * reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem() + "','" +
	 * reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() +
	 * "','" + reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," +
	 * reqParam.getUserId() + ",'" + reqParam.getTrackCode() + "','" + x.getXid() +
	 * "','" + reqParam.getMsisdn() + "','" + reqParam.getOneTimePassword() +
	 * "')}"); if (rs != null) { while (rs.next()) { //lst.add(new
	 * MobileOperatorData(rs.getInt("id"), rs.getString("operator"))); } }
	 * mysql.close(); } catch (Exception e) { System.out.
	 * println("Exception in Vodafone .requestCRBT(RequestParameter reqParam) - "
	 * + e.getMessage()); } finally { return x; } }
	 */
	public Integer requestCRBT(Otp reqParam , DeviceInformation   deviceInformation) {
		Transaction x = new Transaction(deviceInformation.getCountryId(), deviceInformation.getSource(), AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
		Integer response = 167;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CallerRingBackTone");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inOperatorId", deviceInformation.getOperatorId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inDevicePin", deviceInformation.getDevicePin())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inResourceCode", reqParam.getTrackCode())
					.addValue("inTransactionId", x.getXid())  
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inPassword", reqParam.getOneTimePassword());
			  

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				response =Integer.parseInt(resultMap.get("code").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `CallerRingBackTone`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + x.getXid() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					response = rs.getInt("code");
//					// lst.add(new MobileOperatorData(rs.getInt("id"), rs.getString("operator")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return response;
		}
	}

	public int resendOTP(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
				
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CallerRingBackTone");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inOperatorId", reqParam.getOperatorId())
					.addValue("inSourceId", reqParam.getSourceId())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inDevicePin", deviceInformation.getDevicePin())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inResourceCode", reqParam.getTrackCode())
					.addValue("inTransactionId", reqParam.getTransactionId())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inPassword", reqParam.getOneTimePassword());
			  

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
					
//			  
//			ResultSet rs = mysql.prepareCall("{CALL `CallerRingBackTone`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public int otpVerification(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CallerRingBackTone");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inResourceCode", reqParam.getTrackCode() )
						
						.addValue("inTransactionId", reqParam.getTransactionId())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", reqParam.getOneTimePassword());

			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);				    
			responseCode =Integer.parseInt(resultMap.get("code").toString());
			 				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `CallerRingBackTone`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public List<MobileOperatorData> billingOperators(RequestParameter reqParam) {
		List<MobileOperatorData> lst = new ArrayList<MobileOperatorData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("AvailableOperatorBilling");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
				.addValue("inCountryId", reqParam.getCountryId());
								
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new MobileOperatorData(Integer.parseInt(resultMap.get("id").toString()), 
		    			  resultMap.get("operator").toString()
		    			  ));	
		    				    			  
		    	  });
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `AvailableOperatorBilling`(" + reqParam.getCountryId() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new MobileOperatorData(rs.getInt("id"), rs.getString("operator")));
//				}
//			}
		//	mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public Transaction requestPaidDownload(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		Transaction x = new Transaction(deviceInformation.getCountryId(), deviceInformation.getSource() , AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
		try {

			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PaidDownload");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inResourceCode", reqParam.getTrackCode() )
						
						.addValue("inTransactionId", reqParam.getTransactionId())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", reqParam.getOneTimePassword());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				//responseCode =Integer.parseInt(resultMap.get("code").toString());
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `PaidDownload`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + x.getXid() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new MobileOperatorData(rs.getInt("id"), rs.getString("operator")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return x;
		}
	}

	public int resendOTPPaidDownload(RequestParameter reqParam,      DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PaidDownload");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",3)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inResourceCode", reqParam.getTrackCode() )
						
						.addValue("inTransactionId", reqParam.getTransactionId())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", reqParam.getOneTimePassword());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `PaidDownload`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + AESEncriptionDecription.decrypt(reqParam.getMsisdn())
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public int otpVerificationPaidDownload(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PaidDownload");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inResourceCode", reqParam.getTrackCode() )
						
						.addValue("inTransactionId", reqParam.getTransactionId())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", reqParam.getOneTimePassword());
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
 
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `PaidDownload`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	public int chargingRequestPaidDownload(RequestParameter reqParam) {
		int responseCode = 0;
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Download");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inBillingViaId", reqParam.getViaId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inPackageId", 2)
						
						.addValue("inResourceCode", reqParam.getTrackCode())
						.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
						
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
				responseCode =Integer.parseInt(resultMap.get("code").toString());
     
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `Download`(" + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + "," + reqParam.getViaId() + ","
//					+ reqParam.getUserId() + ",2,'" + reqParam.getTrackCode() + "','" + reqParam.getMsisdn() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}

	 
	public List<DownloadHistory> getDownloadHistory(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		List<DownloadHistory> lst = new ArrayList<DownloadHistory>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PaidDownload");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",7)
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inOperatorId", reqParam.getOperatorId())
						.addValue("inSourceId", reqParam.getSourceId())
						
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inResourceCode", reqParam.getTrackCode() )
						
						.addValue("inTransactionId", reqParam.getTransactionId())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", reqParam.getOneTimePassword());
				
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new DownloadHistory(
		    			  resultMap.get("date").toString(),
		    			  resultMap.get("resource_code").toString(),
		    			  Integer.parseInt(resultMap.get("download_type_id").toString()), 
		    			  resultMap.get("resource_title").toString(),
		    			  resultMap.get("artist_name").toString(),
		    			  resultMap.get("album_title").toString(),
		    			  resultMap.get("expiry_date").toString(),
		    			  Integer.parseInt(resultMap.get("allow_download").toString()),
		    			  resultMap.get("image_url").toString()
		    			  ));	
		    				    			  
		    	  });

			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `PaidDownload`(7," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new DownloadHistory(rs.getString("date"), rs.getString("resource_code"),
//							rs.getInt("download_type_id"), rs.getString("resource_title"), rs.getString("artist_name"),
//							rs.getString("album_title"), rs.getString("expiry_date"), rs.getInt("allow_download"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public Transaction mobileNumberVerificationRequest(Otp reqParam , DeviceInformation  deviceInformation ) {
		Transaction x = new Transaction(deviceInformation.getCountryId(), deviceInformation.getSource(), AESEncriptionDecription.decrypt(reqParam.getMsisdn()));
		try {
			Random r = new Random();
			String randomNumber = String.format("%04d", Integer.valueOf(r.nextInt(1001)));
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("MobileNumberVerification_Billing");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inOperatorId", deviceInformation.getOperatorId())
						.addValue("inSourceId", deviceInformation.getSource())				
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())	
						.addValue("inUserId", 0)
						.addValue("inResourceCode", "-")						
						.addValue("inTransactionId", x.getXid())						
						.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
						.addValue("inPassword", randomNumber)
						.addValue("inappversion", deviceInformation.getApplicationVersion());
			  
			   
						
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				String strMessage = messageSource.getMessage("510", null, new Locale(deviceInformation.getLang()))
						.replace("$OTP$", randomNumber);
				String strServiceType = "";
				String strHitURL = "";

				JSONObject jsonParam = new JSONObject();
				JSONObject jsonResponse = new JSONObject();
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
				jsonParam.put("message", strMessage);
				jsonParam.put("source", "Music");
				jsonParam.put("priority", "0");
				jsonParam.put("transId", x.getXid());
				jsonParam.put("contentType", "TXT");
				//jsonParam.put("dnis", getURLValue("api.sms.dnis"));

				strHitURL = getURLValue("api.subscription.sendsms");

				String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
				
				jsonService = new JSONArray(jsonresponse);
				
				// JSONObject jsonObject;
				jsonResponse = jsonService.getJSONObject(0);

				//jsonResponse = new JSONObject(jsonresponse);

				// jsonResponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);

				if (jsonResponse != null) {
					// responseCode = 110;
					// jsonCRBT = jsonResponse.getJSONObject("bundleBean");
				} else {
					// responseCode = 110;
				}
				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "SMS", jsonParam.toString(),
							strHitURL, jsonResponse.toString(),"");
				} catch (Exception ex) {
					System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + ex.toString());
					ex.printStackTrace();
				}

//				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `MobileNumberVerification`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + x.getXid() + "','" + reqParam.getMsisdn() + "','"
//					+ reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new MobileOperatorData(rs.getInt("id"), rs.getString("operator")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return x;
		}
	}

	 

	public int mobileNumberVerificationResendOTP(Otp reqParam ,  DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {

			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("MobileNumberVerification_Billing");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inOperatorId", deviceInformation.getOperatorId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inDevicePin", deviceInformation.getDevicePin())
					.addValue("inUserId", 0)
					.addValue("inResourceCode", "-")
					.addValue("inTransactionId", reqParam.getTransactionId())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inPassword", reqParam.getOneTimePassword())
					.addValue("inappversion", deviceInformation.getApplicationVersion());
					
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				
				responseCode =Integer.parseInt(resultMap.get("code").toString());
				String sOTP = resultMap.get("otp").toString();			
				
				String strMessage = messageSource.getMessage("510", null, new Locale(deviceInformation.getLang()))
						.replace("$OTP$", sOTP);
				String strServiceType = "";
				String strHitURL = "";

				JSONObject jsonParam = new JSONObject();
				JSONObject jsonResponse = new JSONObject();
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
				jsonParam.put("message", strMessage);
				jsonParam.put("source", "Music");
				jsonParam.put("priority", "0");
				jsonParam.put("transId", reqParam.getTransactionId());
				jsonParam.put("contentType", "TXT");
				//jsonParam.put("dnis", getURLValue("api.sms.dnis"));

				strHitURL = getURLValue("api.subscription.sendsms");

				String jsonresponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);
				
				jsonService = new JSONArray(jsonresponse);
				
				// JSONObject jsonObject;
				jsonResponse = jsonService.getJSONObject(0);
				//jsonResponse = new JSONObject(jsonresponse);

				// jsonResponse = UrlProcessor.getInstance().getResponse(strHitURL, jsonParam);

				if (jsonResponse != null) {
					// responseCode = 110;
					// jsonCRBT = jsonResponse.getJSONObject("bundleBean");
				} else {
					// responseCode = 110;
				}
				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "SMS", jsonParam.toString(),
							strHitURL, jsonResponse.toString(),"");
				} catch (Exception e) {
					System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
					e.printStackTrace();
				}
							
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `MobileNumberVerification`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}


//	public int mobileNumberVerificationResendOTP(Otp reqParam ,  DeviceInformation  deviceInformation) {
//		int responseCode = 110;
//		try {
//
//			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("MobileNumberVerification");			  			  
//			  SqlParameterSource inParams = new MapSqlParameterSource()
//					.addValue("inFlag",2)
//					.addValue("inCountryId", deviceInformation.getCountryId())
//					.addValue("inOperatorId", deviceInformation.getOperatorId())
//					.addValue("inSourceId", deviceInformation.getSource())
//					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
//					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
//					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
//					.addValue("inDeviceId", deviceInformation.getDeviceId())
//					.addValue("inDevicePin", deviceInformation.getDevicePin())
//					.addValue("inUserId", 0)
//					.addValue("inResourceCode", "-")
//					.addValue("inTransactionId", reqParam.getTransactionId())
//					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
//					.addValue("inPassword", reqParam.getOneTimePassword())
//					.addValue("inappversion", deviceInformation.getApplicationVersion());
//					
//			  Map<String, Object> rs = jdbcCall.execute(inParams);
//				///////      System.out.println("sign in response :: "+ rs);					      
//				ArrayList<Object> ar = new ArrayList<Object>();
//				ar = (ArrayList) rs.get("#result-set-1");
//				Map resultMap = (Map) ar.get(0);				    
//				responseCode =Integer.parseInt(resultMap.get("code").toString());
//				
//				
////			MySQL mysql = new MySQL();
////			ResultSet rs = mysql.prepareCall("{CALL `MobileNumberVerification`(2," + reqParam.getCountryId() + ","
////					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
////					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
////					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
////					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
////					+ "','" + reqParam.getOneTimePassword() + "')}");
////			if (rs != null) {
////				while (rs.next()) {
////					responseCode = rs.getInt("code");
////				}
////			}
////			mysql.close();
//		} catch (Exception e) {
//			responseCode = 110;
//			System.out.println(
//					"Exception in Vodafone .mobileNumberVerificationResendOTP(RequestParameter reqParam) - "
//							+ e.getMessage());
//		} finally {
//			return responseCode;
//		}
//	}

	public int mobileNumberVerificationOTPVerification(Otp reqParam , DeviceInformation  deviceInformation) {
		int responseCode = 110;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("MobileNumberVerification");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inOperatorId", deviceInformation.getOperatorId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
					.addValue("inDeviceModel", deviceInformation.getDeviceModel())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inDevicePin", deviceInformation.getDevicePin())
					.addValue("inUserId", "0")
					.addValue("inResourceCode","-")
					.addValue("inTransactionId", reqParam.getTransactionId())
					.addValue("inMsisdn", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inPassword", reqParam.getOneTimePassword())
					.addValue("inappversion", deviceInformation.getApplicationVersion());
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				responseCode =Integer.parseInt(resultMap.get("code").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `MobileNumberVerification`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getOperatorId() + "," + reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem()
//					+ "','" + reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getTrackCode() + "','" + reqParam.getTransactionId() + "','" + reqParam.getMsisdn()
//					+ "','" + reqParam.getOneTimePassword() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					responseCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			responseCode = 110;
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return responseCode;
		}
	}


	public CountInfo getFavouriteCount(UserParameters reqParam, DeviceInformation  deviceInformation) {
		CountInfoData info = new CountInfoData(reqParam.getUserId(), 0, 0, 0, 0, 1, "0", 0,"");
		try {
			
			System.out.println("{call `UserFavourite`(4," + reqParam.getItemTypeId() + ","
					+ deviceInformation.getSource() + "," + reqParam.getUserId() + ",'" + deviceInformation.getOperatingSystem() + "','"
					+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
					+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "','" + reqParam.getId() + "',"
					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
					+ "," + reqParam.getEndLimit() + ")}");
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserFavourite");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
					  	.addValue("inItemTypeId", reqParam.getItemTypeId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inResourceCode", "-")
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
			  ArrayList<Object> ar = new ArrayList<Object>();
			  ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
			  
			  JSONObject jsonParam = new JSONObject();
				String jsonResponse = "";
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
			
				jsonResponse = UrlProcessor.getInstance().getResponse(getURLValue("api.userstatus"), jsonParam);
				
				jsonService = new JSONArray(jsonResponse);
				
				JSONObject jsonObject;
				jsonObject = jsonService.getJSONObject(0);

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "USERSTATUS",
							jsonParam.toString(), getURLValue("api.userstatus"), jsonResponse.toString(),"");
				} catch (Exception e) {
					System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
					e.printStackTrace();
				}
				
			  for(int i =0; i< ar.size();i++)
				{
				  int iStatus = Integer.parseInt(jsonObject.getString("status").toString());

					Map resultMap = (Map) ar.get(i);
					info = new CountInfoData(Integer.parseInt(resultMap.get("user_id").toString()),
						  Integer.parseInt(resultMap.get("total_count").toString()),
						  getTypeID(iStatus),
						  iStatus,
						  // Integer.parseInt(resultMap.get("user_type_id").toString()),
						//  Integer.parseInt(resultMap.get("user_status").toString()),
						  Integer.parseInt(resultMap.get("play_counter").toString()),
						  Integer.parseInt(resultMap.get("device_status").toString()),
		    			  resultMap.get("device_status_msg").toString(),
		    			  getTimeStampForStreaming(),
		    			  resultMap.get("packageName").toString()
		    			  );	
				}					
				
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserFavourite`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			while (rs.next()) {
//				info = new CountInfoData(rs.getInt("user_id"), rs.getInt("total_count"), rs.getInt("user_type_id"),
//						rs.getInt("user_status"), rs.getInt("play_counter"), rs.getInt("device_status"),
//						rs.getString("device_status_msg"), getTimeStampForStreaming());
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return new CountInfo(info);
		}
	}

	public CountInfo getNotificationCount(UserParameters reqParam, DeviceInformation deviceInformation) {
		CountInfoData info = new CountInfoData(reqParam.getUserId(), 0, 0, 0, 0, 1, "0", 0,"");
		
		try {
			
			  SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserNotification");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
//					.addValue("insourceId", deviceInformation.getSource())
//					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inNotificationId", reqParam.getId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
//					.addValue("lang", deviceInformation.getLang())
					.addValue("inApiVersion", reqParam.getApiVersion())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
			//	System.out.println("UserNotification in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				JSONObject jsonParam = new JSONObject();
				String jsonResponse = "";
				jsonParam.put("mobileNumber", getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))));
			
				jsonResponse = UrlProcessor.getInstance().getResponse(getURLValue("api.userstatus"), jsonParam);
				
				jsonService = new JSONArray(jsonResponse);
				
				JSONObject jsonObject;
				jsonObject = jsonService.getJSONObject(0);
				 int iStatus =100;
				 String package_description="";
				  Map packMap ;

				try {
					insertURLTracking(getMobileNumber(AESEncriptionDecription.decrypt(String.valueOf(reqParam.getMsisdn()))), "USERSTATUS",
							jsonParam.toString(), getURLValue("api.userstatus"), jsonResponse.toString(),"");
					iStatus = Integer.parseInt(jsonObject.getString("status").toString());
					packMap = getSubscribedPackageDetails(jsonObject.getString("service").toString(),jsonObject.getString("packType").toString());
					
					
					package_description=  packMap.get("package_description").toString();
				
				} catch (Exception ex) {
					System.out.println("Exception in Vodafone DatabaseProcedures.getNotificationCount - " + ex.getMessage());
				}
				 
				 
				
				        
				for(int i =0; i< ar.size();i++)
				{
					Map resultMap = (Map) ar.get(i);
					info = new CountInfoData(Integer.parseInt(resultMap.get("user_id").toString()),
						  Integer.parseInt(resultMap.get("total_count").toString()),
						  getTypeID(iStatus), iStatus,
						 // Integer.parseInt(resultMap.get("user_type_id").toString()),
						  //Integer.parseInt(resultMap.get("user_status").toString()),
						  Integer.parseInt(resultMap.get("play_counter").toString()),
						  Integer.parseInt(resultMap.get("device_status").toString()),
		    			  resultMap.get("device_status_msg").toString(),
		    			  getTimeStampForStreaming(),
		    			  package_description
		    	
		    			  );	
				}					
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{call `UserNotification`(3," + reqParam.getCountryId() + "," + reqParam.getSourceId() + ",'"
//							+ reqParam.getDeviceId() + "'," + reqParam.getUserId() + ",'" + reqParam.getNotificationId()
//							+ "'," + reqParam.getImageTechRefId() + ",'" + reqParam.getLanguageCode() + "',"
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			while (rs.next()) {
//				info = new CountInfoData(rs.getInt("user_id"), rs.getInt("total_count"), rs.getInt("user_type_id"),
//						rs.getInt("user_status"), rs.getInt("play_counter"), rs.getInt("device_status"),
//						rs.getString("device_status_msg"), getTimeStampForStreaming());
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return new CountInfo(info);
		}
	}


	public UserOfflineInformationBean getUserOfflineInformation(UserOfflineModel reqParam, String messageOffline1,
			String messageOffline2 , DeviceInformation  deviceInformation) {
		UserOfflineInformationBean info = null;
		String timestamp = String.valueOf(System.currentTimeMillis());
		try {
			
			System.out.println("{CALL `UserOffline`(1," + deviceInformation.getCountryId() + ","
			+ deviceInformation.getSource() + "," + reqParam.getUserId() + ",'" + deviceInformation.getOperatingSystem() + "','"
			+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
			+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
			+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ",'" + timestamp + "',"
			+ 0 + "," +10 + ")}");

			 
			  Map<String, Object> rs = callUserOffline(1, timestamp, reqParam, deviceInformation);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
			
			  for(int i =0; i< ar.size();i++)
			  {
				Map resultMap = (Map) ar.get(i);
				info = new UserOfflineInformationBean(Integer.parseInt(resultMap.get("code").toString()),
	    			  resultMap.get("message").toString(),
	    			  Integer.parseInt(resultMap.get("total_credits").toString()),
	    			  Integer.parseInt(resultMap.get("available_credits").toString()),
	    			  Integer.parseInt(resultMap.get("used_credits").toString()),
	    			  messageOffline1, messageOffline2
	    			  );	
			 }	
			  
//			if (rs != null) {
//				while (rs.next()) {
//					info = new UserOfflineInformationBean(rs.getInt("code"), rs.getString("message"),
//							rs.getInt("total_credits"), rs.getInt("available_credits"), rs.getInt("used_credits"),
//							messageOffline1, messageOffline2);
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return info;
		}
	}

	public String getFullStreamUrl(String rcode, RequestParameter reqParam) {
		return "-";
	}

 
	public String getSignature(String filename, String timestamp, String secret_code) {
		String hashcheck = filename + timestamp + secret_code;
		String hashtext = "";
		try {
			MessageDigest m;
			m = MessageDigest.getInstance("MD5");
			m.reset();
			m.update(hashcheck.getBytes());
			byte[] digest = m.digest();
			BigInteger bigInt = new BigInteger(1, digest);
			hashtext = bigInt.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return hashtext;
	}

	public List<GenreBean> allGenre(HomeParameters reqParam, DeviceInformation deviceInformation) {
		List<GenreBean> lst = new ArrayList<GenreBean>();
		try {

			  SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inGenreId", reqParam.getId() )
					.addValue("inArtistId", reqParam.getSubid())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//		
	  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);
					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new GenreBean(Integer.parseInt(resultMap.get("genre_id").toString()), resultMap.get("genre_name").toString(),
			    			  resultMap.get("image_url").toString()));					    	   
					});
					      
//					      for (int i=0; i< ar.size();i++)
//					      {
//					    	  Map resultMap = (Map) ar.get(i);
//					    	  lst.add(new GenreBean(Integer.parseInt(resultMap.get("genre_id").toString()), resultMap.get("genre_name").toString(),
//					    			  resultMap.get("image_url").toString()));
//					      }
					    ///////  System.out.println(Integer.parseInt(resultMap1.get("genre_id").toString()));
					      
//					      rs.forEach((k,v) -> System.out.println("Key = "
//					                + k + ", Value = " + v)); 
					      
//					      ArrayList<Object> ar = new ArrayList<Object>();
//					      ar = (ArrayList) rs.get("#result-set-1");
//					      Map resultMap = (Map) ar.get(0);
//					      System.out.println(Integer.parseInt(resultMap.get("user_id").toString()));
//					     
//					   

//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new GenreBean(rs.getInt("genre_id"), rs.getString("genre_name"),
//							rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> genreTrackList(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",4)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inGenreId", reqParam.getId() )
						.addValue("inArtistId", reqParam.getSubid())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
		    			  resultMap.get("ivrMMNumber").toString(),
		    			  resultMap.get("resource_title").toString(),
		    			  Integer.parseInt(resultMap.get("album_id").toString()),
		    			  resultMap.get("album_title").toString(),
		    			  Integer.parseInt(resultMap.get("artist_id").toString()),
		    			  resultMap.get("artist_name").toString(),
		    			  Integer.parseInt(resultMap.get("genre_id").toString()),
		    			  resultMap.get("genre_name").toString(),
		    			  Long.parseLong(resultMap.get("play_count").toString()),
		    			  Long.parseLong(resultMap.get("favourite_count").toString()),
		    			  Long.parseLong(resultMap.get("share_count").toString()),
		    			  Long.parseLong(resultMap.get("size").toString()),
		    			  Long.parseLong(resultMap.get("duration").toString()),
		    			  "-",
		    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("video_id").toString(),false,5)
		    			  
		    			  );	
		    				    			  
		    	  });		
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(4," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> genreArtistTrackList(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetGenreMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",7)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inGenreId", reqParam.getId() )
						.addValue("inArtistId", reqParam.getSubid())
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			 "-",
			    			  // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetGenreMetaData`(7," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getGenreId() + ","
//					+ reqParam.getArtistId() + "," + reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId()
//					+ "," + reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<NotificationData> getUserNotification(UserParameters reqParam, DeviceInformation  deviceInformation) {
		List<NotificationData> lst = new ArrayList<NotificationData>();
		try { // ," + reqParam.getSourceId() +
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserNotification");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",1)
					  	.addValue("inCountryId", deviceInformation.getCountryId())
//						.addValue("insourceId", deviceInformation.getSource())
//						.addValue("inDeviceId", deviceInformation.getDeviceId())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inNotificationId", reqParam.getId())
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
//						.addValue("lang", deviceInformation.getLang())
						.addValue("inApiVersion", reqParam.getApiVersion())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());
			  
					  	Map<String, Object> rs = jdbcCall.execute(inParams);
						///////      System.out.println("sign in response :: "+ rs);					      
						ArrayList<Object> ar = new ArrayList<Object>();
						ar = (ArrayList) rs.get("#result-set-1");
						//Map resultMap1 = (Map) ar.get(0);
						      
						ar.forEach(item->{
				    	  Map resultMap = (Map) item;
				    	  lst.add(new NotificationData(Integer.parseInt(resultMap.get("notification_id").toString()), 
				    			  resultMap.get("sender_name").toString(),
				    			  resultMap.get("notification_title").toString(),
				    			  resultMap.get("notification_subtitle").toString(),
				    			  resultMap.get("notification_msg").toString(),
				    			  resultMap.get("value").toString(),
				    			  resultMap.get("notification_type").toString(),
				    			  Integer.parseInt(resultMap.get("read_status").toString()),
				    			  resultMap.get("date_time").toString(),
				    			  resultMap.get("image_url").toString(),
				    			  resultMap.get("packageName").toString()
				    			  ));	
				    				    			  
				    	  });
				
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `UserNotification`(1," + reqParam.getCountryId() + "," + reqParam.getSourceId() + ",'"
//							+ reqParam.getDeviceId() + "'," + reqParam.getUserId() + "," + reqParam.getNotificationId()
//							+ "," + reqParam.getImageTechRefId() + ",'" + reqParam.getLanguageCode() + "',"
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new NotificationData(rs.getInt("notification_id"), rs.getString("sender_name"),
//							rs.getString("notification_title"), rs.getString("notification_subtitle"),
//							rs.getString("notification_msg"), rs.getString("value"), rs.getString("notification_type"),
//							rs.getInt("read_status"), rs.getString("date_time"), rs.getString("image_url")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<NotificationData> getNotificationInfo(UserParameters reqParam, DeviceInformation deviceInformation ) {
		List<NotificationData> lst = new ArrayList<NotificationData>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserNotification");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",4)
					.addValue("inCountryId", deviceInformation.getCountryId())
//					.addValue("insourceId", deviceInformation.getSource())
//					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inNotificationId", reqParam.getId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
//					.addValue("lang", deviceInformation.getLang())
					.addValue("inApiVersion", reqParam.getApiVersion())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			
			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new NotificationData(Integer.parseInt(resultMap.get("notification_id").toString()), 
		    			  resultMap.get("sender_name").toString(),
		    			  resultMap.get("notification_title").toString(),
		    			  resultMap.get("notification_subtitle").toString(),
		    			  resultMap.get("notification_msg").toString(),
		    			  resultMap.get("value").toString(),
		    			  resultMap.get("notification_type").toString(),
		    			  Integer.parseInt(resultMap.get("read_status").toString()),
		    			  resultMap.get("date_time").toString(),
		    			  resultMap.get("image_url").toString(),
		    			  resultMap.get("packageName").toString()
		    			  ));	
		    				    			  
		    	  });
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall(
//					"{CALL `UserNotification`(4," + reqParam.getCountryId() + "," + reqParam.getSourceId() + ",'"
//							+ reqParam.getDeviceId() + "'," + reqParam.getUserId() + "," + reqParam.getNotificationId()
//							+ "," + reqParam.getImageTechRefId() + ",'" + reqParam.getLanguageCode() + "',"
//							+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			while (rs.next()) {
//				lst.add(new NotificationData(rs.getInt("notification_id"), rs.getString("sender_name"),
//						rs.getString("notification_title"), rs.getString("notification_subtitle"),
//						rs.getString("notification_msg"), rs.getString("value"), rs.getString("notification_type"),
//						rs.getInt("read_status"), rs.getString("date_time"), rs.getString("image_url")));
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public void readNotification(UserParameters reqParam, DeviceInformation deviceInformation) {
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserNotification");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
//					.addValue("insourceId", deviceInformation.getSource())
//					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inNotificationId", reqParam.getId())
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
//					.addValue("lang", deviceInformation.getLang())
					.addValue("inApiVersion", reqParam.getApiVersion())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  

			
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserNotification`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getUserId() + "," + reqParam.getNotificationId() + "," + reqParam.getImageTechRefId()
//					+ ",'" + reqParam.getLanguageCode() + "'," + reqParam.getStartLimit() + "," + reqParam.getEndLimit()
//					+ ")}");
//			while (rs.next()) {
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}

	public void userFeedback(RequestParameter reqParam) {
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Feedback");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inArtistId", reqParam.getSourceId())
					.addValue("inAudioTechRefId", reqParam.getUserId())
					.addValue("inImageTechRefId", "127")
					.addValue("inStart", reqParam.getEmailAddress())
					.addValue("inLimit", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inLimit", reqParam.getEventType())
					.addValue("inLimit", reqParam.getFeedbackData());
		
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap = (Map) ar.get(0);				    
				//responseCode =Integer.parseInt(resultMap.get("code").toString());
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `Feedback`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getDeviceId() + "','"
//					+ reqParam.getEmailAddress() + "','" + AESEncriptionDecription.decrypt(reqParam.getMsisdn()) + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getFeedbackData() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}

	public void audioQualityFeedback(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("AudioQualityFeedback");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	
						.addValue("inCountryId", reqParam.getCountryId())
						.addValue("inSourceId", reqParam.getSourceId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inApplicationVersion", deviceInformation.getApplicationVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inUserId", reqParam.getUserId() )
						.addValue("inUserDeviceId", deviceInformation.getDeviceId())
						.addValue("inResourceCode", reqParam.getTrackCode())						
						.addValue("inRating", reqParam.getRating())
						.addValue("inAction", reqParam.getAction())
						.addValue("inQuality", reqParam.getAudioQuality())
						.addValue("inNetwork", reqParam.getAction());

			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `AudioQualityFeedback`(" + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getApplicationVersion() + "','"
//					+ reqParam.getDeviceModel() + "','" + reqParam.getDeviceId() + "','" + reqParam.getDevicePin()
//					+ "'," + reqParam.getUserId() + "," + reqParam.getUserDeviceId() + ",'" + reqParam.getTrackCode()
//					+ "'," + reqParam.getRating() + ",'" + reqParam.getAction() + "','" + reqParam.getAudioQuality()
//					+ "','" + reqParam.getNetwork() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}

	public void socialSharingTrack(RequestParameter reqParam ,    DeviceInformation  deviceInformation) {
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("SocialSharing");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("insourceId", reqParam.getSourceId())
					.addValue("inUserId", reqParam.getUserId()) 
					.addValue("inUserDeviceId", 0)
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId())
					.addValue("inDevPin", deviceInformation.getDevicePin())
					.addValue("inItemId", reqParam.getTrackCode())
					.addValue("inShareVia", reqParam.getEventType());
			  
			   
			 

			Map<String, Object> rs = jdbcCall.execute(inParams);
			 				///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			//Map resultMap = (Map) ar.get(0);				    
			 				
			 				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `SocialSharing`(" + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + "," + reqParam.getUserDeviceId() + ",'"
//					+ reqParam.getOperatingSystem() + "','" + reqParam.getOperatingSystemVersion() + "','"
//					+ reqParam.getDeviceModel() + "','" + reqParam.getDeviceId() + "','" + reqParam.getDevicePin()
//					+ "','" + reqParam.getTrackCode() + "','" + reqParam.getEventType() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}

	public void successfulOfflineDownload(UserOfflineModel reqParam , DeviceInformation  deviceInformation) {
		String timestamp = String.valueOf(System.currentTimeMillis());
		try {
			
		      	Map<String, Object> rs =  callUserOffline(3, timestamp, reqParam, deviceInformation);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				

				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `UserOffline`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "','" + reqParam.getTrackCode() + "',"
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + ",'" + timestamp + "',"
//					+ reqParam.getStartLimit() + "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					// dwld.setCode(rs.getInt("code"));
//					// dwld.setMessage(rs.getString("message"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
	}
 
	
	
	public List<FeedbackSubjectData> feedbackSubjectList(RequestParameter reqParam ,      DeviceInformation  deviceInformation) {
		List<FeedbackSubjectData> lst = new ArrayList<FeedbackSubjectData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Feedback");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", reqParam.getCountryId())
					.addValue("inSourceId", reqParam.getSourceId())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inEmailAddress", reqParam.getEmailAddress())
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inFeedbackSubjectId", reqParam.getEventType())
					.addValue("inFeedbackData", reqParam.getFeedbackData());					
			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
				      
				ar.forEach(item->{
		    	  Map resultMap = (Map) item;
		    	  lst.add(new FeedbackSubjectData(Integer.parseInt(resultMap.get("fdbk_subject_id").toString()), 
		    			  resultMap.get("fdbk_subject").toString()));	
		    	  });
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `Feedback`(1," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getDeviceId() + "','"
//					+ reqParam.getEmailAddress() + "','" + reqParam.getMsisdn() + "'," + reqParam.getEventType() + ",'"
//					+ reqParam.getFeedbackData() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new FeedbackSubjectData(rs.getInt("fdbk_subject_id"), rs.getString("fdbk_subject")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public String toXml(Object object) {
		final StringWriter stringWriter = new StringWriter();
		try {
			JAXBContext jc = JAXBContext.newInstance(object.getClass());
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.marshal(object, stringWriter);
			return stringWriter.toString();

		} catch (Exception e) {
			System.out.println("Exception in Vodafone .toXml(Object object) - " + e.getMessage());
		}
		return null;
	}

	public String toJson(Object object) {
		String result = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			result = mapper.writeValueAsString(object);
			return result;
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return null;
	}

 

	public FeaturedPlaylistBean getFeaturedPlaylistInfo(HomeParameters reqParam,DeviceInformation deviceInformation) {
		FeaturedPlaylistBean playlist = null;
		try {
			System.out.println("in getFeaturedPlaylistInfo methods ");
					
//			MySQL1 mysql = new MySQL1();
//			ResultSet rs1 = mysql.prepareCall("{CALL `GetFeaturedPlaylistMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getPlaylistId() + ","
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetFeaturedPlaylistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())	
					
					.addValue("inPlaylistId", reqParam.getId() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
			  
			  
			
			
			
					Map<String, Object> rs = jdbcCall.execute(inParams);
					      System.out.println("GetFeaturedPlaylistMetaData  in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					for(int i =0; i< ar.size();i++)
					{
						Map resultMap = (Map) ar.get(i);
						playlist = new FeaturedPlaylistBean(Integer.parseInt(resultMap.get("playlist_id").toString()),
			    			  resultMap.get("playlist_name").toString(),
			    			  resultMap.get("playlist_desc").toString(),
			    			  resultMap.get("image_url").toString(),
			    			  Integer.parseInt(resultMap.get("track_count").toString())
			    			  );	
					}					
					

//			if (rs != null) {
//				while (rs.next()) {
//					playlist = new FeaturedPlaylistBean(rs.getInt("playlist_id"), rs.getString("playlist_name"),
//							rs.getString("playlist_desc"), rs.getString("image_url"), rs.getInt("track_count"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
			playlist = null;
		 

		} finally {
			return playlist;
		}
	}

	public RadioBean getRadioInfo(HomeParameters reqParam,DeviceInformation deviceInformation) {
		RadioBean radio = new RadioBean();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRadioMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",3)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inRadioId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					     
					for(int i =0; i< ar.size();i++)
					{
						Map resultMap = (Map) ar.get(i);
						radio = new RadioBean(Integer.parseInt(resultMap.get("radio_id").toString()),
			    			  resultMap.get("radio_name").toString(),
			    			  resultMap.get("radio_desc").toString(),
			    			  resultMap.get("image_url").toString(),
			    			  Integer.parseInt(resultMap.get("track_count").toString())
			    			  );	
					}					
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetRadioMetaData`(3," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getRadioId() + ","
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					radio = new RadioBean(rs.getInt("radio_id"), rs.getString("radio_name"), rs.getString("radio_desc"),
//							rs.getString("image_url"), rs.getInt("track_count"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return radio;
		}
	}

	public List<FeaturedPlaylistBean> getFeaturedPlaylist(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<FeaturedPlaylistBean> lst = new ArrayList<FeaturedPlaylistBean>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetFeaturedPlaylistMetaData");			  			  
			SqlParameterSource inParams = new MapSqlParameterSource()
			.addValue("inFlag",1)
			.addValue("inCountryId", deviceInformation.getCountryId())
			.addValue("inSourceId", deviceInformation.getSource())
			.addValue("inUserId", reqParam.getUserId())
			.addValue("inOS", deviceInformation.getOperatingSystem())
			.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
			.addValue("inDevModel", deviceInformation.getDeviceModel())
			.addValue("inDevId", deviceInformation.getDeviceId()) 
			.addValue("inDevPin", deviceInformation.getDevicePin())						  
			.addValue("inPlaylistId", reqParam.getId())
			.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
			.addValue("inImageTechRefId", reqParam.getImageTechRefId())
			.addValue("inStart", reqParam.getStartLimit())
			.addValue("inLimit", reqParam.getEndLimit());
			  
			 Map<String, Object> rs = jdbcCall.execute(inParams);			      
			 ArrayList<Object> ar = new ArrayList<Object>();
			 ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new FeaturedPlaylistBean(Integer.parseInt(resultMap.get("playlist_id").toString()),
			    			  resultMap.get("playlist_name").toString(),
			    			  resultMap.get("playlist_desc").toString(),
			    			  resultMap.get("image_url").toString(),
			    			  Integer.parseInt(resultMap.get("track_count").toString())
			    			  ));	
			    				    			  
			    	  });
			
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public boolean validateMobileNumber(RequestParameter reqParam) {
		boolean isValid = false;
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("ValidateMobileNumber");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()));				
			  
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				
				isValid = getBooleanValue(resultMap.get("is_valid").toString());
			  
//		MySQL mysql = new MySQL();
//		ResultSet rs = mysql.prepareCall("{CALL `ValidateMobileNumber`('" + reqParam.getMsisdn() + "')}");
//		while (rs.next()){
//		isValid = rs.getBoolean("is_valid");
//		}
//		mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return isValid;
		}
	}

	public List<RadioBean> getRadio(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<RadioBean> lst = new ArrayList<RadioBean>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRadioMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",1)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inRadioId", reqParam.getId() )
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					      
					ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new RadioBean(Integer.parseInt(resultMap.get("radio_id").toString()),
			    			  resultMap.get("radio_name").toString(),
			    			  resultMap.get("radio_desc").toString(),
			    			  resultMap.get("image_url").toString(),
			    			  Integer.parseInt(resultMap.get("track_count").toString())
			    			  ));	
			    				    			  
			    	  });
			
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> featuredPlaylistTrackList(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			
 		
	
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetFeaturedPlaylistMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inFlag",2)
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inUserId", reqParam.getUserId())
						.addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())
						.addValue("inDevId", deviceInformation.getDeviceId()) 
						.addValue("inDevPin", deviceInformation.getDevicePin())						  
						.addValue("inPlaylistId", reqParam.getId() )						
						.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
						.addValue("inImageTechRefId", reqParam.getImageTechRefId())
						.addValue("inStart", reqParam.getStartLimit())
						.addValue("inLimit", reqParam.getEndLimit());

			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				
				ar.forEach(item->{
			    	  Map resultMap = (Map) item;
			    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
			    			  resultMap.get("ivrMMNumber").toString(),
			    			  resultMap.get("resource_title").toString(),
			    			  Integer.parseInt(resultMap.get("album_id").toString()),
			    			  resultMap.get("album_title").toString(),
			    			  Integer.parseInt(resultMap.get("artist_id").toString()),
			    			  resultMap.get("artist_name").toString(),
			    			  Integer.parseInt(resultMap.get("genre_id").toString()),
			    			  resultMap.get("genre_name").toString(),
			    			  Long.parseLong(resultMap.get("play_count").toString()),
			    			  Long.parseLong(resultMap.get("favourite_count").toString()),
			    			  Long.parseLong(resultMap.get("share_count").toString()),
			    			  Long.parseLong(resultMap.get("size").toString()),
			    			  Long.parseLong(resultMap.get("duration").toString()),
			    			  "-",
			    			 // getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
			    			  resultMap.get("image_url").toString(),
			    			  resultMap.get("video_id").toString(),false, 5)
			    			  
			    			  );	
			    				    			  
			    	  });		
				
			  
		
//			if (rs != null) {
//				while (rs.next()) {
//					// lst.add(new TrackData(rs.getString("resource_code"),
//					// rs.getString("resource_title"), rs.getInt("album_id"),
//					// rs.getString("album_title"), rs.getInt("artist_id"),
//					// rs.getString("artist_name"), rs.getInt("genre_id"),
//					// rs.getString("genre_name"), rs.getLong("size"), rs.getLong("duration"),
//					// getFullStreamUrl(rs.getString("filePath"), reqParam),
//					// rs.getString("image_url"), rs.getString("video_id")));
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public List<TrackData> radioTrackList(HomeParameters reqParam,DeviceInformation deviceInformation) {
		List<TrackData> lst = new ArrayList<TrackData>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("GetRadioMetaData");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inFlag",2)
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inSourceId", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inOS", deviceInformation.getOperatingSystem())
					.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
					.addValue("inDevModel", deviceInformation.getDeviceModel())
					.addValue("inDevId", deviceInformation.getDeviceId()) 
					.addValue("inDevPin", deviceInformation.getDevicePin())						  
					.addValue("inRadioId", reqParam.getId())
					.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
					.addValue("inImageTechRefId", reqParam.getImageTechRefId())
					.addValue("inStart", reqParam.getStartLimit())
					.addValue("inLimit", reqParam.getEndLimit());
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					
					ar.forEach(item->{
				    	  Map resultMap = (Map) item;
				    	  lst.add(new TrackData(resultMap.get("resource_code").toString(), 
				    			  resultMap.get("ivrMMNumber").toString(),
				    			  resultMap.get("resource_title").toString(),
				    			  Integer.parseInt(resultMap.get("album_id").toString()),
				    			  resultMap.get("album_title").toString(),
				    			  Integer.parseInt(resultMap.get("artist_id").toString()),
				    			  resultMap.get("artist_name").toString(),
				    			  Integer.parseInt(resultMap.get("genre_id").toString()),
				    			  resultMap.get("genre_name").toString(),
				    			  Long.parseLong(resultMap.get("play_count").toString()),
				    			  Long.parseLong(resultMap.get("favourite_count").toString()),
				    			  Long.parseLong(resultMap.get("share_count").toString()),
				    			  Long.parseLong(resultMap.get("size").toString()),
				    			  Long.parseLong(resultMap.get("duration").toString()),
				    			  "-",
				    			//  getFullStreamUrl(resultMap.get("filePath").toString(), reqParam),
				    			  resultMap.get("image_url").toString(),
				    			  resultMap.get("video_id").toString(),false, 5)
				    			  
				    			  );	
				    				    			  
				    	  });		
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `GetRadioMetaData`(2," + reqParam.getCountryId() + ","
//					+ reqParam.getSourceId() + "," + reqParam.getUserId() + ",'" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getRadioId() + ","
//					+ reqParam.getAudioTechRefId() + "," + reqParam.getImageTechRefId() + "," + reqParam.getStartLimit()
//					+ "," + reqParam.getEndLimit() + ")}");
//			if (rs != null) {
//				while (rs.next()) {
//					lst.add(new TrackData(rs.getString("resource_code"), rs.getString("ivrMMNumber"),
//							rs.getString("resource_title"), rs.getInt("album_id"), rs.getString("album_title"),
//							rs.getInt("artist_id"), rs.getString("artist_name"), rs.getInt("genre_id"),
//							rs.getString("genre_name"), rs.getLong("play_count"), rs.getLong("favourite_count"),
//							rs.getLong("share_count"), rs.getLong("size"), rs.getLong("duration"),
//							getFullStreamUrl(rs.getString("filePath"), reqParam), rs.getString("image_url"),
//							rs.getString("video_id"), rs.getBoolean("isCrbtAvailable"),
//							rs.getBoolean("isKaraokeAvailable"), rs.getString("lang_karaoke_available").split("#")));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return lst;
		}
	}

	public Root getUserDeviceLoginInformation(UserParameters reqParam,DeviceInformation deviceInformation) {
		Root obj1 = new Root();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDeviceLoginInformation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					
					.addValue("inCountryId", deviceInformation.getCountryId())
					.addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					.addValue("inDeviceId", deviceInformation.getDeviceId())
					.addValue("inVia", "")
					.addValue("inEmail", reqParam.getEmailAddress())
					.addValue("inAppVer", deviceInformation.getApplicationVersion())
			  		.addValue("inOperatingSystem", deviceInformation.getOperatingSystem());
//			  
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				//Map resultMap1 = (Map) ar.get(0);
			
				for(int i =0; i< ar.size();i++)
				{
					Map resultMap = (Map) ar.get(i);
					obj1 = new Root(Integer.parseInt(resultMap.get("code").toString()), resultMap.get("message").toString());
				}
				
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {

			return obj1;
		}
	}

	public boolean validatePhoneNumber(String phoneNo) {
		// validate phone numbers of format "1234567890"
		if (phoneNo.matches("\\d{10}")) {
			return true;
		} else {
			return false;
		}
	}

	public int applicationInstallViaShare(ApplicationInstallViaShareModel reqParam  , DeviceInformation  deviceInformation) {
		int resultCode = 317;
		try {
			System.out.println("Vodafone Ghana --   call `ApplicationInstallViaShare`('" + reqParam.getEventType() + "','"
					+ deviceInformation.getCcode() + "','" + deviceInformation.getCountryId() + "'," + deviceInformation.getSource() + ",'"
					+ deviceInformation.getApplicationVersion() + "','" + deviceInformation.getOperatingSystem() + "','"
					+ deviceInformation.getOperatingSystemVersion() + "','" + deviceInformation.getDeviceModel() + "','"
					+ deviceInformation.getDeviceId() + "','" + deviceInformation.getDevicePin() + "'," + reqParam.getUserId() + ",'"
					+ reqParam.getPromotionCode() + "','" + reqParam.getAction() + "')}");
					
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("ApplicationInstallViaShare");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  	.addValue("inEventType",reqParam.getEventType())
					  	.addValue("inCountryCode",deviceInformation.getCcode())
						.addValue("inCountryId", deviceInformation.getCountryId())
						.addValue("inSourceId", deviceInformation.getSource())
						.addValue("inApplicationVersion", deviceInformation.getApplicationVersion())
						.addValue("inOperatingSystem", deviceInformation.getOperatingSystem())
						.addValue("inOperatingSystemVersion", deviceInformation.getOperatingSystemVersion())
						.addValue("inDeviceModel", deviceInformation.getDeviceModel())
						.addValue("inDeviceId", deviceInformation.getDeviceId()) 
						.addValue("inDevicePin", deviceInformation.getDevicePin())						  
						.addValue("inUserId", reqParam.getUserId() )
						.addValue("inPromotionCode", reqParam.getPromotionCode())
						.addValue("inAction", reqParam.getAction());					
			 
			    Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);				    
				resultCode =Integer.parseInt(resultMap.get("code").toString());			
			  
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `ApplicationInstallViaShare`('" + reqParam.getEventType() + "','"
//					+ reqParam.getCountryCode() + "'," + reqParam.getCountryId() + "," + reqParam.getSourceId() + ",'"
//					+ reqParam.getApplicationVersion() + "','" + reqParam.getOperatingSystem() + "','"
//					+ reqParam.getOperatingSystemVersion() + "','" + reqParam.getDeviceModel() + "','"
//					+ reqParam.getDeviceId() + "','" + reqParam.getDevicePin() + "'," + reqParam.getUserId() + ",'"
//					+ reqParam.getPromotionCode() + "','" + reqParam.getAction() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					resultCode = rs.getInt("code");
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return resultCode;
		}
	}


	public DeviceInformation getUserDeviceInformation(String emailAddress) {
		DeviceInformation devInfo = new DeviceInformation();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserDeviceInformation");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inEmailAddress", emailAddress);					
//			  
			  	Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
				
				devInfo.setDeviceId(resultMap.get("dev_id").toString());
				devInfo.setOperatingSystem(resultMap.get("os").toString());
				devInfo.setDeviceModel(resultMap.get("dev_model").toString());
				devInfo.setOperatingSystemVersion(resultMap.get("osv").toString());
				devInfo.setDevicePin(resultMap.get("dev_pin").toString());
				
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `UserDeviceInformation`('" + emailAddress + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					devInfo.setDeviceId(rs.getString("dev_id"));
//					devInfo.setOperatingSystem(rs.getString("os"));
//					devInfo.setDeviceModel(rs.getString("dev_model"));
//					devInfo.setOperatingSystemVersion(rs.getString("osv"));
//					devInfo.setDevicePin(rs.getString("dev_pin"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return devInfo;
	}

	// Send Notification
	public void sendNotification(String notificationID, String devPin, String lang, String type, String userId) {

		try {
			//MySQL sql = new MySQL();
			String sql="";
			if (type.equalsIgnoreCase("IN")) {
				sql="INSERT INTO tblNotification "
						+ " (user_id,sender_name,notification_title,notification_title_idLang,notification_subtitle,notification_msg,notification_msg_idLang,notification_image,value,notification_type,image_code,country_id)"
						+ " SELECT " + userId
						+ ",sender_name,notification_title,notification_title_idLang,notification_subtitle,notification_msg,notification_msg_idLang,notification_image,value,notification_type,image_code,country_id FROM `tblNotification` WHERE notification_id='"
						+ notificationID + "'";
				jdbcTemplateObject.update(sql);
				
				
			} else {
				if (lang.equalsIgnoreCase("en")) {
					sql= "INSERT INTO tblPushNotification "
							+ " (notification_id,notification_message,notification_image,os,device_id,device_pin,STATUS,remarks,request_date_time)"
							+ " SELECT notification_id,notification_msg,'NA','android','','" + devPin
							+ "',0,'NA',NOW() FROM `tblNotification` WHERE notification_id='" + notificationID + "'";
				jdbcTemplateObject.update(sql);
				}
				else {
					sql = "INSERT INTO tblPushNotification "
							+ " (notification_id,notification_message,notification_image,os,device_id,device_pin,STATUS,remarks,request_date_time)"
							+ " SELECT notification_id,notification_msg_idLang,'NA','android','','" + devPin
							+ "',0,'NA',NOW() FROM `tblNotification` WHERE notification_id='" + notificationID + "'";
				jdbcTemplateObject.update(sql);
				}
			}
			//sql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}

	}

	// CCI Portal
	public CciPortalResponse getUserSubDetails(String msisdn) {
		CciPortalResponse obj = new CciPortalResponse();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("HUTCHTRICCIPORTAL");			  			  
			SqlParameterSource inParams = new MapSqlParameterSource()
			.addValue("IN_FLAG",2)
			.addValue("IN_MOBILE", msisdn);
					

			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
			Map resultMap = (Map) ar.get(0);				    
				
			obj.setDatetime(resultMap.get("datetime").toString());
			obj.setMobile_number(resultMap.get("mobile_number").toString());
			obj.setStatus(resultMap.get("status").toString());
			obj.setPackName(resultMap.get("package_description").toString());
			obj.setPrice(resultMap.get("price_info").toString());

			obj.setPackageStartDate(resultMap.get("package_start_date").toString());
			obj.setPackageEndtDate(resultMap.get("package_end_date").toString());
			obj.setPackageRenewDate(resultMap.get("package_renewal_date").toString());
			obj.setLastSuccessBilling(resultMap.get("last_successfull_billing").toString());
							
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `HUTCHTRICCIPORTAL`(2,'" + msisdn + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					obj.setDatetime(rs.getString("datetime"));
//					obj.setMobile_number(rs.getString("mobile_number"));
//					obj.setStatus(rs.getString("status"));
//					obj.setPackName(rs.getString("package_description"));
//					obj.setPrice(rs.getString("price_info"));
//
//					obj.setPackageStartDate(rs.getString("package_start_date"));
//					obj.setPackageEndtDate(rs.getString("package_end_date"));
//					obj.setPackageRenewDate(rs.getString("package_renewal_date"));
//					obj.setLastSuccessBilling(rs.getString("last_successfull_billing"));
//				}
//				mysql.close();
//			}

		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return obj;
	}

	public List<CciPortalResponse> getUserSuccessDetails(String msisdn) {

		List<CciPortalResponse> list = new ArrayList<CciPortalResponse>();
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("HUTCHTRICCIPORTAL");			  			  
			SqlParameterSource inParams = new MapSqlParameterSource()
			.addValue("IN_FLAG",1)
			.addValue("IN_MOBILE", msisdn);
					

			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
		//	Map resultMap = (Map) ar.get(0);				    
				
			ar.forEach(item->{
	    	  Map resultMap = (Map) item;
		    	 
	    	  CciPortalResponse obj = new CciPortalResponse();
				obj.setDatetime(resultMap.get("datetime").toString());
				obj.setMobile_number(resultMap.get("mobile_number").toString());
				obj.setRemarks(resultMap.get("remarks").toString());
				obj.setPackName(resultMap.get("package_description").toString());
				obj.setPrice(resultMap.get("price_info").toString());
				list.add(obj);	
	    				    			  
	    	  });			
			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `HUTCHTRICCIPORTAL`(1,'" + msisdn + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					CciPortalResponse obj = new CciPortalResponse();
//					obj.setDatetime(rs.getString("datetime"));
//					obj.setMobile_number(rs.getString("mobile_number"));
//					obj.setRemarks(rs.getString("remarks"));
//					obj.setPackName(rs.getString("package_description"));
//					obj.setPrice(rs.getString("price_info"));
//					list.add(obj);
//				}
//				mysql.close();
		//	}
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return list;
	}

	public List<CciPortalResponse> getUserUnsubDetails(String msisdn) {

		List<CciPortalResponse> list = new ArrayList<CciPortalResponse>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("HUTCHTRICCIPORTAL");			  			  
			SqlParameterSource inParams = new MapSqlParameterSource()
			.addValue("IN_FLAG",3)
			.addValue("IN_MOBILE", msisdn);
					

			Map<String, Object> rs = jdbcCall.execute(inParams);
			///////      System.out.println("sign in response :: "+ rs);					      
			ArrayList<Object> ar = new ArrayList<Object>();
			ar = (ArrayList) rs.get("#result-set-1");
		//	Map resultMap = (Map) ar.get(0);				    
				
			ar.forEach(item->{
	    	  Map resultMap = (Map) item;
		    	 
	    	  CciPortalResponse obj = new CciPortalResponse();
				obj.setDatetime(resultMap.get("datetime").toString());
				obj.setMobile_number(resultMap.get("mobile_number").toString());
				obj.setRemarks(resultMap.get("status").toString());
				obj.setPackName(resultMap.get("package_description").toString());
				obj.setPrice(resultMap.get("price_info").toString());
				
				obj.setPackageStartDate(resultMap.get("package_start_date").toString());
				obj.setPackageEndtDate(resultMap.get("package_end_date").toString());
				obj.setPackageRenewDate(resultMap.get("package_renewal_date").toString());
				obj.setLastSuccessBilling(resultMap.get("last_successfull_billing").toString());
				obj.setUnsubDate(resultMap.get("unsub_date").toString());
				
				list.add(obj);	
	    				    			  
	    	  });			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `HUTCHTRICCIPORTAL`(3,'" + msisdn + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					CciPortalResponse obj = new CciPortalResponse();
//
//					obj.setDatetime(rs.getString("datetime"));
//					obj.setMobile_number(rs.getString("mobile_number"));
//					obj.setStatus(rs.getString("status"));
//					obj.setPackName(rs.getString("package_description"));
//					obj.setPrice(rs.getString("price_info"));
//
//					obj.setPackageStartDate(rs.getString("package_start_date"));
//					obj.setPackageEndtDate(rs.getString("package_end_date"));
//					obj.setPackageRenewDate(rs.getString("package_renewal_date"));
//					obj.setLastSuccessBilling(rs.getString("last_successfull_billing"));
//					obj.setUnsubDate(rs.getString("unsub_date"));
//					list.add(obj);
//				}
//				mysql.close();
			//}
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}
		return list;
	}

	public PromotionShareConfig getPromotionShareConfig(long userid, String apiversion) {
		PromotionShareConfig obj1 = null;
		try {
			
			   SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PromotionShareConfig");			  			  
			   SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("in_Userid", userid)
					.addValue("inApiVersion", apiversion);
					
//			  
					Map<String, Object> rs = jdbcCall.execute(inParams);
					///////      System.out.println("sign in response :: "+ rs);					      
					ArrayList<Object> ar = new ArrayList<Object>();
					ar = (ArrayList) rs.get("#result-set-1");
					//Map resultMap1 = (Map) ar.get(0);
					
					for(int i =0; i< ar.size();i++)
					{
						Map resultMap = (Map) ar.get(i);
						obj1 = new PromotionShareConfig(Integer.parseInt(resultMap.get("id").toString()),
								resultMap.get("banner_url").toString(),
								resultMap.get("title").toString(),
								resultMap.get("msg").toString().toString(),
								resultMap.get("tnc_title").toString(), 
								resultMap.get("tnc_msg").toString(),
								resultMap.get("share_msg").toString(),
								resultMap.get("user_point_msg").toString(),
								resultMap.get("refpoint_msg").toString());
								
								
					}
					
			
					
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{CALL `PromotionShareConfig`('" + reqParam.getUserId() + "')}");
//			if (rs != null) {
//				while (rs.next()) {
//					obj1 = new PromotionShareConfig(rs.getInt("id"), rs.getString("banner_url"), rs.getString("title"),
//							rs.getString("msg"), rs.getString("tnc_title"), rs.getString("tnc_msg"),
//							rs.getString("share_msg"));
//				}
//			}
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
			return obj1;
		}
	}

	public List<OperatorData> getOperatorData(String fromDate, String todate) {

		List<OperatorData> list = new ArrayList<OperatorData>();
//		try {
//			
//			MySQL mysql = new MySQL();
//			ResultSet rs = mysql.prepareCall("{call `PRO_OPERATOR_DATA`(0)}");
//			rs = mysql.executeQuery(
//					"SELECT DATE_FORMAT(date_time,'%Y-%m-%d %H:%i') as date_time , pack_name as pack_name ,  sub_type as sub_type ,price as price , days as days , total_count as total_count , revenue as revenue    FROM tbl_operator_data WHERE  DATE(date_time) BETWEEN  '"
//							+ fromDate + "' and '" + todate + "' ORDER  BY date_time DESC ");
//
//			if (rs != null) {
//				while (rs.next()) {
//					OperatorData obj = new OperatorData();
//					obj.setDateTime(rs.getString("date_time"));
//					obj.setPackName(rs.getString("pack_name"));
//					obj.setSubType(rs.getString("sub_type"));
//					obj.setPrice(rs.getString("price"));
//					obj.setDays(rs.getInt("days"));
//					obj.setTotalCount(rs.getInt("total_count"));
//					obj.setRevenue(rs.getString("revenue"));
//					list.add(obj);
//				}
//				mysql.close();
//			}
//		} catch (Exception e) {
//			System.out.println("Exception in send ing Notification---" + e);
//		}
		return list;
	}
	
	

public List<DashBoardData> getDashBoardData() {	
	List<DashBoardData> list = new ArrayList<DashBoardData>();
	// MySQL mysql = new MySQL();	
	// ResultSet rs =null;
	        try {		        	
	        	SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("PRO_MIS_GET_DASHBOARD_DATA")   ;
	        	Map<String, Object> rs = jdbcCall.execute();						      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				for(int i =0; i< ar.size();i++)
				{
					DashBoardData obj=new DashBoardData();
					Map resultMap = (Map) ar.get(i);
					
                	obj.setDateTime(resultMap.get("date_time").toString());	  
                	obj.setTotalCount(resultMap.get("total_count").toString());
                	obj.setGrossRevenue(resultMap.get("gross_revenue").toString());
                	obj.setNetRevenue(resultMap.get("net_revenue").toString());
                	obj.setPack1(resultMap.get("pack_1").toString());
                	obj.setPack1Revnue(resultMap.get("pack_1_revnue").toString());	                	
                	obj.setPack2(resultMap.get("pack_2").toString());
                	obj.setPack2Revnue(resultMap.get("pack_2_revenue").toString());	                	
                	obj.setPack3(resultMap.get("pack_3").toString());
                	obj.setPack3Revnue(resultMap.get("pack_3_revnue").toString());                	                	
                	obj.setPack4(resultMap.get("pack_4").toString());
                	obj.setPack4Revnue(resultMap.get("pack_4_revnue").toString());
                	
                	obj.setBEATS7SC(resultMap.get("BEATS7SC").toString());
                	obj.setBEATS7SC_revenue(resultMap.get("BEATS7SC_revenue").toString());
                	
                	obj.setBEATS30SC8(resultMap.get("BEATS30SC8").toString());
                	obj.setBEATS30SC8_revenue(resultMap.get("BEATS30SC8_revenue").toString());
                	
                	obj.setBEATS30SC18(resultMap.get("BEATS30SC18").toString());
                	obj.setBEATS30SC18_revenue(resultMap.get("BEATS30SC18_revenue").toString());
                	
                	
                	
                	list.add(obj);   
                	
                	
					
				 
				}
				
				/*
	            // rs = mysql.prepareCall("{call `PRO_MIS_GET_DASHBOARD_DATA`()}");	            
	            if (rs != null) {
	                while (rs.next()) {
	                	DashBoardData obj=new DashBoardData();
	                	obj.setDateTime(rs.getString("date_time"));	  
	                	obj.setTotalCount(rs.getString("total_count"));
	                	obj.setGrossRevenue(rs.getString("gross_revenue"));
	                	obj.setNetRevenue(rs.getString("net_revenue"));
	                	obj.setPack1(rs.getString("pack_1"));
	                	obj.setPack1Revnue(rs.getString("pack_1_revnue"));	                	
	                	obj.setPack2(rs.getString("pack_2"));
	                	obj.setPack2Revnue(rs.getString("pack_2_revenue"));	                	
	                	obj.setPack3(rs.getString("pack_3"));
	                	obj.setPack3Revnue(rs.getString("pack_3_revnue"));	                	
	                	obj.setPack4(rs.getString("pack_4"));
	                	obj.setPack4Revnue(rs.getString("pack_4_revnue"));	                	
	                	list.add(obj);
	                }
	                }
	           */     
	               
	            
	 }catch(Exception e){
		 System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
	 } 
		

	return list;
}




	public long getTimeStampForStreaming() {

		long currenttime = System.currentTimeMillis();

		//System.out.println(			"----> UTC DATE     -> " + (currenttime + new java.util.Date(currenttime).getTimezoneOffset()));

		return (currenttime + new java.util.Date(currenttime).getTimezoneOffset());
	}

	/*
	 * public static void main(String arr[]) { new
	 * DataBaseProcedures().getTimeStampForStreaming(); }
	 */
	private Boolean getBooleanValue(String p_Value) {
		Boolean bValue = false;

		if (p_Value.equals("1") || p_Value.toLowerCase().equals("true")) {
			bValue = true;
		} 

		return bValue;
	}


    public String decryptMobileNumber(String encryptedMobileNumber) {
// 	   String query =null;
// 	try {
// 		System.out.println(" **********************  Going for decrypt Mobile Number  :: "+  encryptedMobileNumber);
//// 		byte[] valueDecodedBase64 = Base64.getDecoder().decode(encryptedMobileNumber);            
////      	byte[] valueDecoded = new AESEncriptionDecription().decrypt(new String(valueDecodedBase64));
//         query = AESEncriptionDecription.decrypt(encryptedMobileNumber);
//			
//		} catch (Exception e) {			 
//			//e.printStackTrace();
//			
//			System.out.println(" **********************  decryption of MobileNumber is failed :: "+  encryptedMobileNumber);
//		}  
// 	System.out.println("Out put ::::"+ query);
 	return AESEncriptionDecription.decrypt(encryptedMobileNumber);
 }
    
    
    private Map<String, Object>  callUserOffline( int inFlag , String timestamp ,   UserOfflineModel reqParam , DeviceInformation   deviceInformation)
    {
    	 Map<String, Object> rs = null;
    
			try {
				SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("UserOffline");			  			  
				  SqlParameterSource inParams = new MapSqlParameterSource()
						  	.addValue("inFlag",inFlag)
							.addValue("inCountryId", deviceInformation.getCountryId())
							.addValue("inSourceId", deviceInformation.getSource())
							.addValue("inUserId", reqParam.getUserId())
							.addValue("inUserType", reqParam.getUsertype())							
							.addValue("inOS", deviceInformation.getOperatingSystem())
							.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
							.addValue("inDevModel", deviceInformation.getDeviceModel())
							.addValue("inDevId", deviceInformation.getDeviceId()) 
							.addValue("inDevPin", deviceInformation.getDevicePin())						  
							.addValue("inResourceCode", reqParam.getTrackCode() )							
							.addValue("inAudioTechRefId", reqParam.getAudioTechRefId())						
							.addValue("inImageTechRefId", reqParam.getImageTechRefId())
							.addValue("inValidTime", timestamp)
							.addValue("inApplicationVersion", deviceInformation.getApplicationVersion())							
							.addValue("inStart", 0)
							.addValue("inLimit", 10);
				  	         rs = jdbcCall.execute(inParams); 
    	
    }catch (Exception e) {
    	e.printStackTrace();
		
	}
			
			 return rs;
    }   
    

    
    @SuppressWarnings("finally")
	public void signOut(SignIn reqParam , DeviceInformation deviceInformation) {
		try {
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("SignOut");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					.addValue("inSource", deviceInformation.getSource())
					.addValue("inUserId", reqParam.getUserId())
					.addValue("inDev_id", deviceInformation.getDeviceId())
					.addValue("inAppVer", deviceInformation.getApplicationVersion())
					.addValue("inOperatingSystem", deviceInformation.getOperatingSystem());
					
			    Map<String, Object> rs = jdbcCall.execute(inParams);
				///////      System.out.println("sign in response :: "+ rs);					      
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");
				Map resultMap = (Map) ar.get(0);
			  
			//	System.out.println(Integer.parseInt(resultMap.get("code").toString()));
				
//			MySQL mysql = new MySQL();
//			mysql.prepareCall("{CALL `SignOut`(" + reqParam.getSourceId() + "," + reqParam.getUserId() + ",'"
//					+ reqParam.getDeviceId() + "','" + reqParam.getApplicationVersion() + "','"
//					+ reqParam.getOperatingSystem() + "')}");
//			mysql.close();
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		} finally {
		}
	}


    
	@SuppressWarnings("finally")
	public void  campaignThirdPartyCallBackSend(String ani , String campaingName , String tackingId) {
		List<SubscriptionPackage> lst = new ArrayList<SubscriptionPackage>();
		try {
			
			SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("CampaignThirdPartyCallBackSend");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()					
					.addValue("inMobile_number", ani)					
					.addValue("inCampaign_name", campaingName)
					.addValue("inTacking_id", tackingId);
			  Map<String, Object> rs = jdbcCall.execute(inParams);
				ArrayList<Object> ar = new ArrayList<Object>();
				ar = (ArrayList) rs.get("#result-set-1");	
				      
//				ar.forEach(item->{
//		    	  Map resultMap = (Map) item;		    	   
//		    		  resultMap.get("package_name").toString();	 
//		    				    			  
//		    	  });
			  
 
		} catch (Exception e) {
			System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
		}  
	}
    
	
    public void updateDevicePin( UserParameters reqParam,	DeviceInformation deviceInformation ) {
        try {
            
          System.out.println("{Vodafone GHana :  call `Update_FCM_Token`(" + reqParam.getUserId() + ",'" + reqParam.getMsisdn() + "',  '" + deviceInformation.getDeviceId() + "' , "+deviceInformation.getSource()+" , '"+ deviceInformation.getOperatingSystem()+"'  , '"+deviceInformation.getOperatingSystemVersion()+"' , '"+deviceInformation.getDeviceModel() +"' , '"+deviceInformation.getDevicePin()+"' )}");
            
        	SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("Update_FCM_Token");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
					  .addValue("inUserId", reqParam.getUserId())
					  .addValue("inMobileNumber", AESEncriptionDecription.decrypt(reqParam.getMsisdn()))
					  .addValue("inDevId", deviceInformation.getDeviceId())
					  .addValue("inSourceId", deviceInformation.getSource())
					  .addValue("inOS", deviceInformation.getOperatingSystem())
						.addValue("inOSV", deviceInformation.getOperatingSystemVersion())
						.addValue("inDevModel", deviceInformation.getDeviceModel())						 
						.addValue("inToken", deviceInformation.getDevicePin())	;
			  	        jdbcCall.execute(inParams); 

			  	         
            
           
        } catch (Exception e) {
        	System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
            
        }
    }
    
    

    public void tokenTracking(DeviceInformation deviceInformation,  StringBuffer  tokenResponse ) {
        try {
            
          //System.out.println("{Vodafone GHana Token  Tracking :  call `Update_FCM_Token`(" + reqParam.getUserId() + ",'" + reqParam.getMsisdn() + "',  '" + deviceInformation.getDeviceId() + "' , "+deviceInformation.getSource()+" , '"+ deviceInformation.getOperatingSystem()+"'  , '"+deviceInformation.getOperatingSystemVersion()+"' , '"+deviceInformation.getDeviceModel() +"' , '"+deviceInformation.getDevicePin()+"' )}");
            
        	SimpleJdbcCall jdbcCall = new   SimpleJdbcCall(dataSource).withProcedureName("token_tracking");			  			  
			  SqlParameterSource inParams = new MapSqlParameterSource()
			 
			  
			  .addValue("devid", deviceInformation.getDeviceId())
			  .addValue("model", deviceInformation.getDeviceModel())
			  .addValue("devpin", deviceInformation.getDevicePin())			  
			  .addValue("src", deviceInformation.getSource())
			  .addValue("os", deviceInformation.getOperatingSystem())
			  .addValue("osv", deviceInformation.getOperatingSystemVersion())
			  .addValue("token_response", tokenResponse)
			  
			  
			  ;	   						 
			   
	        jdbcCall.execute(inParams); 

		  
        } catch (Exception e) {
        	System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date())+ " Voda Fone -- Exception : " + e.toString());
			e.printStackTrace();
            
        }
    }

    
     
}
