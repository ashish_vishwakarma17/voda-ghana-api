/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.filter;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import beans.config.AESEncriptionDecription;

@Component
@Order(-1)
public class RequestFilter implements Filter {

	@Value("${request-logs-path}")
	private String requestLogsPath;

	private static final boolean debug = false;

	// this method will be called by container while deployment
	public void init(FilterConfig config) throws ServletException {

		System.out.println("init() method has been get invoked");
		System.out.println("Filter name is " + config.getFilterName());
		System.out.println("ServletContext name is" + config.getServletContext());
		System.out.println("init() method is ended");
		System.out.println("requestLogsPath :: " + requestLogsPath);
	}

	private void doBeforeProcessing(RequestWrapper request, ResponseWrapper response)
			throws IOException, ServletException {
		if (debug) {
			log("LoggingFilter:DoBeforeProcessing");
		}

	//	System.out.println("Vodafone -- Ghana--  ***********************" + request.getBody());

		final String finalParamList;
		String paramList = "";
		int n;
		StringBuffer buf;
		String name;

		Enumeration<String> headerNames = request.getHeaderNames();

		if (headerNames != null) {
			while (headerNames.hasMoreElements()) {

				String key = headerNames.nextElement();

				String headerKey = "";
				switch (key) {

				case "evt":
					headerKey = key;
					break;

				case "src":
					headerKey = key;
					break;

				case "appv":
					headerKey = key;
					break;

				case "ocid":
					headerKey = key;
					break;
				case "lang":
					headerKey = key;
					break;
				case "ccode":
					headerKey = key;
					break;
				case "x-api-key":
					headerKey = key;
					break;
				case "os":
					headerKey = key;
					break;
				case "deviceid":
					headerKey = "devid";
					break;
				case "apiversion":
					headerKey = key;
					break;
				case "model":
					headerKey = "model";
					break;
				case "osv":
					headerKey = key;
					break;
				case "devpin":
					headerKey = key;
					break;
				case "devid":
					headerKey = key;
					break;
				case "opid":
					headerKey = key;
					break;
				default:
					continue;

				}
				String para = headerKey + "=" + (request.getHeader(key) + "").replaceAll("\"", "");
				if (paramList.isEmpty()) {
					paramList += "dt=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "$local="
							+ request.getLocalAddr() + "$remote=" + request.getRemoteAddr() + "$" + para.toString();
				} else {
					if (!para.contains("L-USER-MSISDN:") && !para.contains("json=")) {
						paramList += "$" + para.toString();
					}
				}

			}
		}

		// System.out.println("parameter after header :::" + paramList);

		try {
			if ("POST".equalsIgnoreCase(request.getMethod()) && !request.getBody().isEmpty()) {
				Object obj = new JSONParser().parse(new String(request.getBody()));
				JSONObject jo = (JSONObject) obj;
				// System.out.println("VodaFone_Reqest Body :: "+ jo.toString() + " **** HEADER
				// : " + paramList);
				Set<String> set = jo.keySet();
				Iterator<String> it = set.iterator();
				while (it.hasNext()) {
					String key = (String) it.next();
					String value = null;
					try {
						value = jo.get(key).toString();

					} catch (Exception e) {
						value = "NA";
						// TODO: handle exception
					}

					String para;
					String paraKey;
					if (key.equals("msisdn")) {
						paraKey = key;
						para = key + "=" + AESEncriptionDecription.decrypt(value);
					} else if (key.equals("id")) {
						paraKey = "trackid";
						para = "trackid=" + value.replaceAll("\"", "");
					} else {
						paraKey = key;
						para = key + "=" + value.replaceAll("\"", "");
					}
					// System.out.println(" Key value vodafone is ::" + para);
					if (paramList.isEmpty()) {
						paramList += "dt=" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "$local="
								+ request.getLocalAddr() + "$remote=" + request.getRemoteAddr() + "$" + para.toString();
					} else {
						if (!para.contains("L-USER-MSISDN:") && !para.contains("json=")) {
							if (!paramList.contains(paraKey))
								paramList += "$" + para.toString();
						}
					}

				}

			}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		finalParamList = paramList;
	//	System.out.println(" Voda_Ghana Filter : " + finalParamList);

		new Thread() {
			public void run() {
				String fileName = UUID.randomUUID().toString();
				// Log File
				try {
					FileWriter fw = new FileWriter(requestLogsPath + fileName + ".log");
					fw.write(finalParamList);
					fw.flush();
					fw.close();
				} catch (Exception e) {
					System.out.println("Exception in  LoggingFilter.LOG_FILE - " + e.getMessage());

				}
				// Lock File
				try {
					FileWriter fw = new FileWriter(requestLogsPath + fileName + ".lck");
					fw.write("LOCK");
					fw.flush();
					fw.close();
				} catch (Exception e) {
					System.out.println("Exception in  LoggingFilter.LOCK_FILE - " + e.getMessage());
				}
			}
		}.start();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		RequestWrapper wrappedRequest = new RequestWrapper((HttpServletRequest) request);
		ResponseWrapper wrappedResponse = new ResponseWrapper((HttpServletResponse) response);

		doBeforeProcessing(wrappedRequest, wrappedResponse);

		Throwable problem = null;

		try {
			chain.doFilter(wrappedRequest, wrappedResponse);
		} catch (Throwable t) {
			// If an exception is thrown somewhere down the filter chain,
			// we still want to execute our after processing, and then
			// rethrow the problem after that.
			problem = t;
			// t.printStackTrace();
			System.out.println(t.getMessage());
		}

		// If there was a problem, we want to rethrow it if it is
		// a known type, otherwise log it.
		if (problem != null) {
			if (problem instanceof ServletException) {
				throw (ServletException) problem;
			}
			if (problem instanceof IOException) {
				throw (IOException) problem;
			}
			sendProcessingError(problem, response);
		}
	}

	/**
	 * Return the filter configuration object for this filter.
	 * 
	 * @return
	 */

	/**
	 * Set the filter configuration object for this filter.
	 *
	 * @param filterConfig The filter configuration object
	 */

	/**
	 * Destroy method for this filter
	 */
	@Override
	public void destroy() {
	}

	private void sendProcessingError(Throwable t, ServletResponse response) {
		String stackTrace = getStackTrace(t);

		if (stackTrace != null && !stackTrace.equals("")) {
			try {
				response.setContentType("text/html");
				PrintStream ps = new PrintStream(response.getOutputStream());
				PrintWriter pw = new PrintWriter(ps);
				pw.print("<html>\n<head>\n<title>Error</title>\n</head>\n<body>\n"); // NOI18N

				// PENDING! Localize this for next official release
				pw.print("<h1>The resource did not process correctly</h1>\n<pre>\n");
				pw.print(stackTrace);
				pw.print("</pre></body>\n</html>"); // NOI18N
				pw.close();
				ps.close();
				response.getOutputStream().close();
			} catch (Exception ex) {
			}
		} else {
			try {
				PrintStream ps = new PrintStream(response.getOutputStream());
				// t.printStackTrace(ps);
				// System.out.println(t.getMessage());
				ps.close();
				response.getOutputStream().close();
			} catch (Exception ex) {
			}
		}
	}

	public static String getStackTrace(Throwable t) {
		String stackTrace = null;
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			// t.printStackTrace(pw);
			// System.out.println(t.getMessage());
			pw.close();
			sw.close();
			stackTrace = sw.getBuffer().toString();
		} catch (Exception ex) {
		}
		return stackTrace;
	}

	public void log(String msg) {
		System.out.println(msg);
	}

	/**
	 * This request wrapper class extends the support class
	 * HttpServletRequestWrapper, which implements all the methods in the
	 * HttpServletRequest interface, as delegations to the wrapped request. You only
	 * need to override the methods that you need to change. You can get access to
	 * the wrapped request using the method getRequest()
	 */

	class ResponseWrapper extends HttpServletResponseWrapper {

		public ResponseWrapper(HttpServletResponse response) {
			super(response);
		}

		// You might, for example, wish to know what cookies were set on the response
		// as it went throught the filter chain. Since HttpServletRequest doesn't
		// have a get cookies method, we will need to store them locally as they
		// are being set.
		/*
		 * protected Vector cookies = null;
		 * 
		 * // Create a new method that doesn't exist in HttpServletResponse public
		 * Enumeration getCookies() { if (cookies == null) cookies = new Vector();
		 * return cookies.elements(); }
		 * 
		 * // Override this method from HttpServletResponse to keep track // of cookies
		 * locally as well as in the wrapped response. public void addCookie (Cookie
		 * cookie) { if (cookies == null) cookies = new Vector(); cookies.add(cookie);
		 * ((HttpServletResponse)getResponse()).addCookie(cookie); }
		 */
	}

}
