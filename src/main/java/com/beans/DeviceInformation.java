/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.beans;

/**
 *
 * @author Vijay.kumar
 */
public class DeviceInformation {

	private String applicationVersion = "";
	private String operatingSystem = "";
	private String operatingSystemVersion = "";	
	private String deviceModel = "";
	private String deviceId = "";
	private String devicePin = "";
	private int source;
	private String lang;
	private String ccode;	
	
	private String evt;
	
	private String countryId;
	private int operatorId;	

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public int getOperatorId() {
		return operatorId;
	}



	public void setOperatorId(int operatorId) {
		this.operatorId = operatorId;
	}



	public DeviceInformation() {
	}



	public String getApplicationVersion() {
		return applicationVersion;
	}



	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}



	public String getOperatingSystem() {
		return operatingSystem;
	}



	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}



	public String getOperatingSystemVersion() {
		return operatingSystemVersion;
	}



	public void setOperatingSystemVersion(String operatingSystemVersion) {
		this.operatingSystemVersion = operatingSystemVersion;
	}



	public String getDeviceModel() {
		return deviceModel;
	}



	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}



	public String getDeviceId() {
		return deviceId;
	}



	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}



	public String getDevicePin() {
		return devicePin;
	}



	public void setDevicePin(String devicePin) {
		this.devicePin = devicePin;
	}



	public int getSource() {
		return source;
	}



	public void setSource(int source) {
		this.source = source;
	}



	public String getLang() {
		return lang;
	}



	public void setLang(String lang) {
		this.lang = lang;
	}



	public String getCcode() {
		return ccode;
	}



	public void setCcode(String ccode) {
		this.ccode = ccode;
	}

	public String getEvt() {
		return evt;
	}

	public void setEvt(String evt) {
		this.evt = evt;
	}

	
	
	
}
