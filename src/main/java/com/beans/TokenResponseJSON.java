package com.beans;

import org.springframework.stereotype.Component;

@Component
public class TokenResponseJSON  extends Root {
	
	private TokenResponse tokenResponse;
	
	

	public TokenResponseJSON() {
		  super(0, "Success");
	}

	public TokenResponseJSON(TokenResponse tokenResponse) {
		  super(0, "Success");
		this.tokenResponse = tokenResponse;
	}

	public TokenResponse getTokenResponse() {
		return tokenResponse;
	}

	public void setTokenResponse(TokenResponse tokenResponse) {
		this.tokenResponse = tokenResponse;
	}

	@Override
	public String toString() {
		return "TokenResponseJSON [tokenResponse=" + tokenResponse + "]";
	}
	
	
	

}
