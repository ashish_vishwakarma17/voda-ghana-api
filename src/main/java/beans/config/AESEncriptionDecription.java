package beans.config;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
public class AESEncriptionDecription {	
	//private static final String secret_code = "SPICEKEYGENERATOR";
    private static final String encryptionKey = "SAFARICOMBEATSKE";
    private static final String characterEncoding = "UTF-8";
    private static final String cipherTransformation = "AES/CBC/PKCS5PADDING";
    private static final String cipherTransformationECB = "AES/ECB/PKCS5PADDING";
    private static final String aesEncryptionAlgorithem = "AES";
    private static final String IV = "vodafoneMusicBea";    

    public static String encrypt(String plainText) {
        String encryptedText = "";
        try {
        	
       /*     IvParameterSpec iv = new IvParameterSpec("encryptionIntVec".getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(plainText.getBytes());
            return Base64.encodeBase64String(encrypted);
*/
        	
            Cipher cipher = Cipher.getInstance(cipherTransformation);
            byte[] key = encryptionKey.getBytes("UTF-8");
            SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
            AlgorithmParameterSpec cipherIV = new IvParameterSpec(IV.getBytes());


            cipher.init(1, secretKey,cipherIV);
            byte[] cipherText = cipher.doFinal(plainText.getBytes(characterEncoding));
            encryptedText = Base64.encodeBase64String(cipherText); 
        } catch (Exception E) {
            System.err.println("Encrypt Exception : " + E.getMessage());
            encryptedText = encryptECB(plainText);
        }
        
        return encryptedText;
    }
    public static String encryptECB(String plainText) {
        String encryptedText = "";
        try {
        	
            Cipher cipher = Cipher.getInstance(cipherTransformationECB);
            byte[] key = encryptionKey.getBytes("UTF-8");
            SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);

            cipher.init(1, secretKey);
            byte[] cipherText = cipher.doFinal(plainText.getBytes(characterEncoding));
            encryptedText = Base64.encodeBase64String(cipherText);
        } catch (Exception E) {
            System.err.println("Encrypt Exception : " + E.getMessage());
        }
        return encryptedText;
    }


    public static String decrypt(String encryptedText) {
        String decryptedText = null;
        
        try {
        	
       /* 	IvParameterSpec iv = new IvParameterSpec("encryptionIntVec".getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            byte[] original = cipher.doFinal(Base64.decodeBase64(encryptedText));

            return new String(original);

            */
        //    String	name = new String(Base64.decodeBase64(encryptedText.getBytes()), "UTF-8");
            Cipher cipher = Cipher.getInstance(cipherTransformation);
            byte[] key = encryptionKey.getBytes(characterEncoding);
            SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
            AlgorithmParameterSpec cipherIV = new IvParameterSpec(IV.getBytes());

            cipher.init(2, secretKey,cipherIV);
            byte[] cipherText = Base64.decodeBase64(encryptedText);
            decryptedText = new String(cipher.doFinal(cipherText));  
       //     System.out.println(" ********************** Going to decryption for  :: "+  encryptedText    + "   And  decryptedText is :: "+ decryptedText);
        } catch (Exception E) {
        	decryptedText=decryptECB(encryptedText);
        //	System.out.println(" **********************  decryption failed  for  :: "+  encryptedText);
         //   E.printStackTrace();
        }

        return decryptedText;
    }
    public static String decryptECB(String encryptedText) {
        String decryptedText = null;
        
        try {
        //    String	name = new String(Base64.decodeBase64(encryptedText.getBytes()), "UTF-8");
            Cipher cipher = Cipher.getInstance(cipherTransformationECB);
            byte[] key = encryptionKey.getBytes(characterEncoding);
            SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);

            cipher.init(2, secretKey);
            byte[] cipherText = Base64.decodeBase64(encryptedText);
            decryptedText = new String(cipher.doFinal(cipherText));
       //     System.out.println(" ********************** Going to decryption for  :: "+  encryptedText    + "   And  decryptedText is :: "+ decryptedText);
        } catch (Exception E) {
        	decryptedText=null;
        	System.out.println(" **********************  decryption failed  for  :: "+  encryptedText);
         //   E.printStackTrace();
        }

        return decryptedText;
    }

	 
    
    public static void main(String []args) {
    	//String plainText = "233200423528";
    	String plainText = "233571138007";
    	String en=encrypt(plainText);
    	System.out.println("encrypt ::"+ encrypt(plainText) );
    	try {
			System.out.println(URLEncoder.encode( en, "UTF-8" ));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       	System.out.println("URL DECODE decrypt ::"+    decrypt(URLDecoder.decode(en)));
	}
	
	
}
